package com.tongtong.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.common.util.LogUtil;
import com.tencent.mm.sdk.openapi.BaseReq;
import com.tencent.mm.sdk.openapi.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tongtong.android.activity.MainActivity;

public class WXEntryActivity extends Activity implements IWXAPIEventHandler {

    private IWXAPI mWxApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mWxApi = WXAPIFactory.createWXAPI(this, getResources().getString(R.string.app_id), false);
        mWxApi.registerApp(getResources().getString(R.string.app_id));
        mWxApi.handleIntent(getIntent(), this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        mWxApi.handleIntent(intent, this);
    }


    @Override
    public void onResp(BaseResp baseResp) {

        setTitle("分享失败");
        int result = 0;
        switch (baseResp.errCode) {
            case BaseResp.ErrCode.ERR_OK:
                setTitle("分享成功");
                break;
            case BaseResp.ErrCode.ERR_USER_CANCEL:
                break;
            case BaseResp.ErrCode.ERR_AUTH_DENIED:
                break;
            default:
                break;
        }

        LogUtil.e(App.TAG,"微信分享结果:"+baseResp.errCode);
        LogUtil.e(App.TAG, "分享结果==================================================================");
    }


    @Override
    public void onReq(BaseReq baseResp) {

        try {
            Intent intent = new Intent(App.getApp(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } catch (Exception e) {
        }

        LogUtil.e(App.TAG, "响应结果==================================================================");
    }
}
