package com.tongtong.android.json;

import com.common.net.NetResult;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.tongtong.android.App;
import com.tongtong.android.domain.LiveType;
import com.tongtong.android.domain.Profile;
import com.tongtong.android.domain.Room;
import com.tongtong.android.domain.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by wangzy on 16/8/8.
 */
public class JsonHelper {


    public static final String STATUS_OK = NetResult.CODE_OK;
    public static final String STATUS_ERROR = NetResult.CODE_ERROR;


    public static final String TAG_STATUS = "status_code";
    public static final String TAG_MESSAGE = "message";
    public static final String TAG_CONTENT = "content";
    public static final String TAG_TOKEN = "token";
    public static final String TAG_ERROR = "error";
    public static final String TAG_DATA = "data";


    public static NetResult parseRegister(InputStream inputStream) throws Exception {
        String result = getJsonFromInputStream(inputStream);

        LogUtil.i(App.TAG, result);

        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();

        netResult.setMessage(getStringFroJso(TAG_MESSAGE, jsonObject));
        netResult.setCode(getStringFroJso(TAG_STATUS, jsonObject));

        if (netResult.isOk()) {

            JSONObject dataObject = jsonObject.getJSONObject(TAG_DATA);

            netResult.setData(parseTokenAddUser(dataObject));

        }

        return netResult;
    }

    public static NetResult parseLogin(InputStream inputStream) throws Exception {
        String result = getJsonFromInputStream(inputStream);

        LogUtil.i(App.TAG, result);

        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();
        netResult.setMessage(getStringFroJso(TAG_MESSAGE, jsonObject));
        netResult.setCode(getStringFroJso(TAG_STATUS, jsonObject));

        if (netResult.isOk()) {

            JSONObject dataObject = jsonObject.getJSONObject(TAG_DATA);

            JSONObject userObject = dataObject.getJSONObject("user");
            User user = parseUser(userObject);

            JSONObject profileObj = userObject.getJSONObject("profile");

            Profile profile = new Profile();

            profile.setRole(getStringFroJso("role", userObject));
            profile.setId(getStringFroJso("user_id", profileObj));
            profile.setImgUrl(getStringFroJso("avatar", profileObj));
            profile.setNickname(getStringFroJso("nickname", userObject));
            profile.setEmail(getStringFroJso("email", userObject));
            profile.setCity(getStringFroJso("city", profileObj));

            if (getStringFroJso("role", userObject).equals("anchor")) {

                JSONObject roomObject = userObject.getJSONObject("room");

                profile.setLiveType(getStringFroJso("type_id", roomObject));
                profile.setLivTopic(getStringFroJso("subject", roomObject));

//              profile.setLiveAddr(getStringFroJso("wowza_android", roomObject).substring(getStringFroJso("wowza_android", roomObject).lastIndexOf("/") + 1));

                profile.setLiveAddr(getStringFroJso("wowza_android", roomObject));

                profile.setDescription(getStringFroJso("contents", roomObject));
            }

            profile.setDisable(userObject.getInt("disable"));
            profile.setApply(userObject.getInt("apply"));


            if (userObject.has("room") && !StringUtils.isEmpty(userObject.getString("room")) && !"null".equals(userObject.getString("room"))) {

                Room room = JsonHelper.parseRoomObject(userObject.getJSONObject("room"));
                user.setRoom(room);
            }

            String apiToken = getStringFroJso(TAG_TOKEN, dataObject);

            App.getApp().setProfile(profile);
            netResult.setData(new Object[]{user, apiToken, profile});
        }

        return netResult;
    }

    public static NetResult parseProfile(InputStream inputStream) throws Exception {
        String result = getJsonFromInputStream(inputStream);

        LogUtil.i(App.TAG, result);

        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();
        netResult.setMessage(getStringFroJso(TAG_MESSAGE, jsonObject));
        netResult.setCode(getStringFroJso(TAG_STATUS, jsonObject));

        if (netResult.isOk()) {

            JSONObject dataObject = jsonObject.getJSONObject(TAG_DATA);
            JSONObject profileObj = dataObject.getJSONObject("profile");

            Profile profile = new Profile();

            profile.setRole(getStringFroJso("role", dataObject));

            profile.setId(getStringFroJso("user_id", profileObj));
            profile.setImgUrl(getStringFroJso("avatar", profileObj));
            profile.setNickname(getStringFroJso("nickname", dataObject));
            profile.setEmail(getStringFroJso("email", dataObject));
            profile.setCity(getStringFroJso("city", profileObj));
            if (getStringFroJso("role", dataObject).equals("anchor")) {
                JSONObject roomObject = dataObject.getJSONObject("room");
                profile.setLiveType(getStringFroJso("type_id", roomObject));
                profile.setLivTopic(getStringFroJso("subject", roomObject));
                profile.setLiveAddr(getStringFroJso("wowza_android", roomObject).substring(getStringFroJso("wowza_android", roomObject).lastIndexOf("/") + 1));
                profile.setDescription(getStringFroJso("contents", roomObject));
            }

            profile.setDisable(dataObject.getInt("disable"));
            profile.setApply(dataObject.getInt("apply"));

            netResult.setData(new Object[]{profile});

        }

        return netResult;
    }

    public static NetResult parseLiveTypes(InputStream inputStream) throws Exception {
        String result = getJsonFromInputStream(inputStream);

        LogUtil.i(App.TAG, result);

        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();
        netResult.setMessage(getStringFroJso(TAG_MESSAGE, jsonObject));
        netResult.setCode(getStringFroJso(TAG_STATUS, jsonObject));

        if (netResult.isOk()) {

            ArrayList<LiveType> liveTypes = new ArrayList<>();


            JSONArray dataObjectarray = jsonObject.getJSONArray(TAG_DATA);

            if (null != dataObjectarray && dataObjectarray.length() != 0) {

                for (int i = 0, isize = dataObjectarray.length(); i < isize; i++) {

                    JSONObject typeJson = dataObjectarray.getJSONObject(i);

                    LiveType liveType = new LiveType();

                    liveType.setId(getStringFroJso("id", typeJson));
                    liveType.setType(getStringFroJso("title", typeJson));

                    liveTypes.add(liveType);
                }
            }
            App.getApp().setLiveTypes(liveTypes);
            netResult.setData(new Object[]{liveTypes});
        }

        return netResult;

    }


    public static NetResult parseRooms(InputStream inputStream) throws Exception {
        String result = getJsonFromInputStream(inputStream);

        LogUtil.i(App.TAG, result);

        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();
        netResult.setMessage(getStringFroJso(TAG_MESSAGE, jsonObject));
        netResult.setCode(getStringFroJso(TAG_STATUS, jsonObject));

        if (netResult.isOk()) {

            ArrayList<Room> rooms = new ArrayList<>();

            JSONObject dataObject = jsonObject.getJSONObject(TAG_DATA);

            int total = dataObject.getInt("total");

            JSONArray dataObjectarray = dataObject.getJSONArray(TAG_DATA);


            if (null != dataObjectarray && dataObjectarray.length() != 0) {

                for (int i = 0, isize = dataObjectarray.length(); i < isize; i++) {

                    JSONObject roomObj = dataObjectarray.getJSONObject(i);

                    Room room = parseRoomAndAncher3(roomObj);

                    rooms.add(room);
                }
            }

            netResult.setData(new Object[]{rooms, total});

        }

        return netResult;

    }


    public static NetResult parseLiveRoom(InputStream inputStream) throws Exception {
        String result = getJsonFromInputStream(inputStream);

        LogUtil.i(App.TAG, result);

        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();
        netResult.setMessage(getStringFroJso(TAG_MESSAGE, jsonObject));
        netResult.setCode(getStringFroJso(TAG_STATUS, jsonObject));

        if (netResult.isOk()) {

            ArrayList<Room> rooms = new ArrayList<>();

            JSONObject dataObject = jsonObject.getJSONObject(TAG_DATA);

            int total = dataObject.getInt("total");

            JSONArray dataObjectarray = dataObject.getJSONArray(TAG_DATA);

            if (null != dataObjectarray && dataObjectarray.length() != 0) {

                for (int i = 0, isize = dataObjectarray.length(); i < isize; i++) {
                    Room room = parseRoomAndAncher3(dataObjectarray.getJSONObject(i));
                    rooms.add(room);
                }
            }

            netResult.setData(new Object[]{rooms, total});

        }

        return netResult;

    }


    public static NetResult parseLiveUsersPwd(InputStream inputStream) throws Exception {
        String result = getJsonFromInputStream(inputStream);

        LogUtil.i(App.TAG, result);

        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();
        netResult.setMessage(getStringFroJso(TAG_MESSAGE, jsonObject));
        netResult.setCode(getStringFroJso(TAG_STATUS, jsonObject));

        if (netResult.isOk()) {

            netResult.setData(new Object[]{jsonObject});

        }

        return netResult;

    }


    public static NetResult parseHomeIndex(InputStream inputStream) throws Exception {
        String result = getJsonFromInputStream(inputStream);

        LogUtil.i(App.TAG, result);

        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();
        netResult.setMessage(getStringFroJso(TAG_MESSAGE, jsonObject));
        netResult.setCode(getStringFroJso(TAG_STATUS, jsonObject));

        if (netResult.isOk()) {

            ArrayList<Room> rooms = new ArrayList<>();

            JSONObject dataObject = jsonObject.getJSONObject(TAG_DATA);
            JSONArray dataObjectarray = dataObject.getJSONArray("new");


            if (null != dataObjectarray && dataObjectarray.length() != 0) {

                for (int i = 0, isize = dataObjectarray.length(); i < isize; i++) {

                    JSONObject roomObj = dataObjectarray.getJSONObject(i);

                    Room room = new Room();

                    room.setId(getStringFroJso("id", roomObj));
                    room.setName(getStringFroJso("name", roomObj));
                    room.setUserId(getStringFroJso("user_id", roomObj));


                    room.setTitle(getStringFroJso("title", roomObj));
                    room.setImgUrl(getStringFroJso("img_url", roomObj));
                    room.setScores(getStringFroJso("scores", roomObj));
                    room.setUrl(getStringFroJso("url", roomObj));
                    room.setContents(getStringFroJso("contents", roomObj));
                    room.setTags(getStringFroJso("tags", roomObj));
                    room.setWowzaLink(getStringFroJso("tags", roomObj));
                    room.setIsLive(getStringFroJso("is_live", roomObj));
                    room.setDisable(getStringFroJso("disable", roomObj));
                    room.setTypeId(getStringFroJso("type_id", roomObj));

                    rooms.add(room);
                }
            }

            netResult.setData(new Object[]{rooms});

        }

        return netResult;

    }


    public static NetResult parseHomeIndex2(InputStream inputStream) throws Exception {
        String result = getJsonFromInputStream(inputStream);

        LogUtil.i(App.TAG, result);

        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();
        netResult.setMessage(getStringFroJso(TAG_MESSAGE, jsonObject));
        netResult.setCode(getStringFroJso(TAG_STATUS, jsonObject));

        if (netResult.isOk()) {


            ArrayList<Room> rooms = new ArrayList<>();
            ArrayList<Room> suggestRoms = new ArrayList<>();


            JSONObject dataObject = jsonObject.getJSONObject(TAG_DATA);

            JSONArray suggesDatas = dataObject.getJSONArray("suggest");

            if (null != suggesDatas && suggesDatas.length() != 0) {
                for (int i = 0, isize = suggesDatas.length(); i < isize; i++) {
                    Room roomgust = parseRoomAndAncher(suggesDatas.getJSONObject(i));
                    suggestRoms.add(roomgust);
                }
            }

            JSONArray newObjectDatas = dataObject.getJSONArray("new");

            if (null != newObjectDatas && newObjectDatas.length() != 0) {
                for (int i = 0, isize = newObjectDatas.length(); i < isize; i++) {
                    JSONObject roomObj = newObjectDatas.getJSONObject(i);
                    Room room = parseRoomAndAncher(roomObj);
                    rooms.add(room);
                }
            }

            netResult.setData(new Object[]{rooms, suggestRoms});

        }

        return netResult;

    }


    public static NetResult parseHomeIndex3(InputStream inputStream) throws Exception {
        String result = getJsonFromInputStream(inputStream);

        LogUtil.i(App.TAG, result);

        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();
        netResult.setMessage(getStringFroJso(TAG_MESSAGE, jsonObject));
        netResult.setCode(getStringFroJso(TAG_STATUS, jsonObject));

        if (netResult.isOk()) {

            ArrayList<Room> rooms = new ArrayList<>();
            ArrayList<Room> suggestRoms = new ArrayList<>();

            JSONObject dataObject = jsonObject.getJSONObject(TAG_DATA);

            JSONArray suggesDatas = dataObject.getJSONArray("suggest");

            if (null != suggesDatas && suggesDatas.length() != 0) {

                for (int i = 0, isize = suggesDatas.length(); i < isize; i++) {
                    Room roomgust = parseRoomAndAncher3(suggesDatas.getJSONObject(i));
                    suggestRoms.add(roomgust);
                }
            }

            JSONArray newObjectDatas = dataObject.getJSONArray("new");

            if (null != newObjectDatas && newObjectDatas.length() != 0) {
                for (int i = 0, isize = newObjectDatas.length(); i < isize; i++) {
                    JSONObject roomObj = newObjectDatas.getJSONObject(i);
                    Room room = parseRoomAndAncher3(roomObj);
                    rooms.add(room);
                }
            }

            netResult.setData(new Object[]{rooms, suggestRoms});

        }

        return netResult;

    }


    private static Room parseRoomAndAncher(JSONObject roomDataObject) throws JSONException {

        Room room = new Room();

        User user = new User();
        room.setUser(user);

        user.setUserId(getStringFroJso("id", roomDataObject));
        user.setNickName(getStringFroJso("nickname", roomDataObject));
        user.setEmail(getStringFroJso("email", roomDataObject));
        user.setRole(getStringFroJso("role", roomDataObject));
        user.setDisable(getStringFroJso("disable", roomDataObject));
        user.setApply(getStringFroJso("apply", roomDataObject));


        user.setBlack(getStringFroJso("black", roomDataObject));

        if (roomDataObject.has("profile") &&
                !"null".equals(roomDataObject.getString("profile")) &&
                !StringUtils.isEmpty(roomDataObject.getString("profile"))) {
            JSONObject profileObject = roomDataObject.getJSONObject("profile");
            user.setCity(getStringFroJso("city", profileObject));
            user.setAvatar(getStringFroJso("avatar", profileObject));
        }


        JSONObject roomObject = null;

        if (roomDataObject.has("room") && (null != (roomObject = roomDataObject.getJSONObject("room")))) {

            room.setId(getStringFroJso("id", roomObject));
            room.setName(getStringFroJso("name", roomObject));
            room.setTitle(getStringFroJso("title", roomObject));
            room.setImgUrl(getStringFroJso("img_url", roomObject));
            room.setScores(getStringFroJso("scores", roomObject));

            room.setUrl(getStringFroJso("url", roomObject));
            room.setContents(getStringFroJso("contents", roomObject));

            room.setTags(getStringFroJso("tags", roomObject));
            room.setWowzaLink(getStringFroJso("wowza_link", roomObject));
            room.setIsLive(getStringFroJso("is_live", roomObject));
            room.setTypeId(getStringFroJso("type_id", roomObject));
            room.setTotalPeople(getStringFroJso("totalpeople", roomObject));
            room.setStartLiveTime(getStringFroJso("startlivetime", roomObject));
            room.setAppUrl(getStringFroJso("app_url", roomObject));

        }

        return room;

    }


    private static Room parseRoomAndAncher3(JSONObject roomDataObject) throws JSONException {

        Room room = null;
        User user = null;

        JSONObject userObject = roomDataObject.getJSONObject("user");


        if (null != userObject) {
            user = parseUser(userObject);

            JSONObject roomObject = userObject.getJSONObject("room");
            if (null != roomObject) {
                room = parseRoomObject(roomObject);
            }
        }

        room.setUser(user);

        return room;

    }

    public static NetResult parseStartEndLive(InputStream inputStream) throws Exception {
        String result = getJsonFromInputStream(inputStream);

        LogUtil.i(App.TAG, result);

        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();
        netResult.setMessage(getStringFroJso(TAG_MESSAGE, jsonObject));
        netResult.setCode(getStringFroJso(TAG_STATUS, jsonObject));

        if (netResult.isOk()) {
            netResult.setData(new Object[]{jsonObject});

        }

        return netResult;

    }

    public static NetResult parseUpload(String result) throws Exception {

        LogUtil.i(App.TAG, result);

        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();
        netResult.setMessage(getStringFroJso(TAG_MESSAGE, jsonObject));
        netResult.setCode(getStringFroJso(TAG_STATUS, jsonObject));

        if (netResult.isOk()) {

            JSONObject dataObject = jsonObject.getJSONObject(TAG_DATA);

            String avatar = getStringFroJso("avatar", dataObject);

            netResult.setData(new Object[]{avatar});

        }

        return netResult;

    }

    public static NetResult parseUploadScreen(String result) throws Exception {

        LogUtil.i(App.TAG, result);

        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();
        netResult.setMessage(getStringFroJso(TAG_MESSAGE, jsonObject));
        netResult.setCode(getStringFroJso(TAG_STATUS, jsonObject));

        if (netResult.isOk()) {
            String url = jsonObject.getString(TAG_DATA);
            netResult.setData(new Object[]{url});

        }

        return netResult;

    }

    public static NetResult parseUpdateProfile(InputStream inputStream) throws Exception {

        String result = getJsonFromInputStream(inputStream);

        LogUtil.i(App.TAG, result);

        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();
        netResult.setMessage(getStringFroJso(TAG_MESSAGE, jsonObject));
        netResult.setCode(getStringFroJso(TAG_STATUS, jsonObject));

        if (netResult.isOk()) {
            if (null != App.getApp().getProfile()) {
                JSONObject dataObject = jsonObject.getJSONObject(TAG_DATA);
                App.getApp().getProfile().setNickname(getStringFroJso("nickname", dataObject));
                JSONObject profileObject = dataObject.getJSONObject("profile");
                App.getApp().getProfile().setCity(getStringFroJso("city", profileObject));

            }
        }
        return netResult;

    }

    public static NetResult parseUpdateRoom(InputStream inputStream) throws Exception {

        String result = getJsonFromInputStream(inputStream);

        LogUtil.i(App.TAG, result);

        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();
        netResult.setMessage(getStringFroJso(TAG_MESSAGE, jsonObject));
        netResult.setCode(getStringFroJso(TAG_STATUS, jsonObject));

        if (netResult.isOk()) {

            if (null != App.getApp().getProfile()) {

                JSONObject dataObject = jsonObject.getJSONObject(TAG_DATA);

                User user = App.getApp().getUser();

                user.setNickName(getStringFroJso("nickname", dataObject));

                Profile profile = App.getApp().getProfile();
                profile.setNickname(getStringFroJso("nickname", dataObject));

                JSONObject profileObject = dataObject.getJSONObject("profile");
                profile.setCity(getStringFroJso("city", profileObject));

                App.getApp().setUser(user);
                App.getApp().setProfile(profile);

            }


        }
        return netResult;

    }

    public static NetResult parseReport(InputStream inputStream) throws Exception {

        String result = getJsonFromInputStream(inputStream);

        LogUtil.i(App.TAG, result);

        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();
        netResult.setMessage(getStringFroJso(TAG_MESSAGE, jsonObject));
        netResult.setCode(getStringFroJso(TAG_STATUS, jsonObject));

        return netResult;

    }

    public static NetResult parseForgetPassword(InputStream inputStream) throws Exception {

        String result = getJsonFromInputStream(inputStream);

        LogUtil.i(App.TAG, result);

        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();
        netResult.setMessage(getStringFroJso(TAG_MESSAGE, jsonObject));
        netResult.setCode(getStringFroJso(TAG_STATUS, jsonObject));

        return netResult;

    }


    public static NetResult parseBlack(InputStream inputStream) throws Exception {

        String result = getJsonFromInputStream(inputStream);

        LogUtil.i(App.TAG, result);

        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();
        netResult.setMessage(getStringFroJso(TAG_MESSAGE, jsonObject));
        netResult.setCode(getStringFroJso(TAG_STATUS, jsonObject));

        if (netResult.isOk()) {

            String black = jsonObject.getJSONObject("data").getString("id");

            netResult.setData(new Object[]{black});
        }

        return netResult;

    }

    public static NetResult parseTrailAll(InputStream inputStream) throws Exception {

        String result = getJsonFromInputStream(inputStream);

        LogUtil.i(App.TAG, result);
        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();
        netResult.setMessage(getStringFroJso(TAG_MESSAGE, jsonObject));
        netResult.setCode(getStringFroJso(TAG_STATUS, jsonObject));
        if (netResult.isOk()) {
            JSONObject dataObject = jsonObject.getJSONObject(TAG_DATA);
            JSONArray roomsArray = dataObject.getJSONArray("data");
            Object objects[] = new Object[roomsArray.length()];
            for (int i = 0; i < objects.length; i++) {
                objects[i] = parseTimelineRoomObject(roomsArray.getJSONObject(i));
            }
            netResult.setData(objects);
        }

        return netResult;

    }

    //======================


    public static Room parseRoomObject(JSONObject dataObject) throws JSONException {


        Room room = new Room();

        room.setId(getStringFroJso("id", dataObject));
        room.setName(getStringFroJso("name", dataObject));
        room.setTitle(getStringFroJso("title", dataObject));
        room.setImgUrl(getStringFroJso("img_url", dataObject));

        room.setScores(getStringFroJso("scores", dataObject));
        room.setUrl(getStringFroJso("url", dataObject));
        room.setContents(getStringFroJso("contents", dataObject));
        room.setTags(getStringFroJso("tags", dataObject));
        room.setWowzaLink(getStringFroJso("wowza_link", dataObject));
        room.setWowzaAndroid(getStringFroJso("wowza_android", dataObject));
        room.setIsLive(getStringFroJso("is_live", dataObject));
        room.setSubject(getStringFroJso("subject", dataObject));
        room.setTypeId(getStringFroJso("type_id", dataObject));
        room.setTotalPeople(getStringFroJso("onlinepeople", dataObject));
        room.setStartLiveTime(getStringFroJso("startlivetime", dataObject));
        room.setAppUrl(getStringFroJso("app_url", dataObject));
        room.setWowzaAndroid(getStringFroJso("wowza_android", dataObject));

        return room;


    }

    public static Room parseTimelineRoomObject(JSONObject dataObject) throws JSONException {


        Room room = new Room();
        User user = null;
        JSONObject userObject = dataObject.getJSONObject("user");
        room.setName(getStringFroJso("nickname", userObject));


        if (null != userObject) {
            user = parseUser(userObject);

            if(!userObject.isNull("room")){
                JSONObject roomObject = userObject.getJSONObject("room");
                if (null != roomObject) {
                    room = parseRoomObject(roomObject);
                }
            }else{
                room = new Room();
            }
        }

        room.setId(getStringFroJso("id", dataObject));
        room.setTitle(getStringFroJso("title", dataObject));
        room.setImgUrl(getStringFroJso("img_url", dataObject));
        room.setStartLiveTime(getStringFroJso("startdate", dataObject));
        room.setEndLiveTime(getStringFroJso("enddate", dataObject));
        room.setUser(user);

        return room;


    }


    private static Object[] parseTokenAddUser(JSONObject dataObject) throws JSONException {

        String apiToken = getStringFroJso(TAG_TOKEN, dataObject);

        JSONObject userObject = dataObject.getJSONObject("user");

        return new Object[]{parseUser(userObject), apiToken};
    }


    public static User parseUser(JSONObject userJsonObject) throws JSONException {

        User user = new User();

        user.setId(getStringFroJso("id", userJsonObject));
        user.setEmail(getStringFroJso("email", userJsonObject));
        user.setNickName(getStringFroJso("nickname", userJsonObject));
        user.setRole(getStringFroJso("role", userJsonObject));

        user.setDisable(getStringFroJso("disable", userJsonObject));
        user.setApply(getStringFroJso("apply", userJsonObject));

        user.setBlack(getStringFroJso("black", userJsonObject));


        if (userJsonObject.has("profile") && !"null".equals(userJsonObject.getString("profile")) && !StringUtils.isEmpty(userJsonObject.getString("profile"))) {

            JSONObject profileObject = userJsonObject.getJSONObject("profile");

            user.setCity(getStringFroJso("city", profileObject));
            user.setAvatar(getStringFroJso("avatar", profileObject));

        }


        return user;
    }


    //==============================================================================================

    public static String getJsonFromInputStream(InputStream ins) {
        if (null != ins) {
            StringBuffer sbf = new StringBuffer();
            BufferedInputStream bfris = new BufferedInputStream(ins);
            byte[] buffer = new byte[512];
            int ret = -1;
            try {
                while ((ret = bfris.read(buffer)) != -1) {
                    sbf.append(new String(buffer, 0, ret));
                }
                bfris.close();
                ins.close();
                return sbf.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static String getStringFroJso(String key, JSONObject jobj) throws JSONException {
        if (jobj.has(key)) {

            String result = jobj.getString(key);

            if ("null".equals(result)) {
                return "";
            } else {
                return result;
            }

        } else {
            return "";
        }
    }

}
