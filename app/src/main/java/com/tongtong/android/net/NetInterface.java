package com.tongtong.android.net;

import android.content.Context;

import com.common.net.FormFile;
import com.common.net.HttpRequester;
import com.common.net.NetResult;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.tongtong.android.App;
import com.tongtong.android.json.JsonHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by clia on 2016/5/23.
 */
public class NetInterface {

    private final static String base_path = "http://api.himate.tv/api/";// server
    public final static String live_path = "52.63.94.182:1935";
//    private final static String base_path = "http://106.185.55.185:8768/api/"; // test server
//    public final static String live_path = "106.185.55.185:8768:1935";
//    private final static String base_path = "http://192.168.1.121:8000/api/"; //baozi test server


    public static NetResult register(Context context, JSONObject customizedObject) throws Exception {
        String path = "register";
        InputStream jsonInputStream = createCallBackInputStream(context, customizedObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseRegister(jsonInputStream);
            return netResult;
        }
        return null;
    }


    public static NetResult login(Context context, JSONObject jsonObject) throws Exception {
        String path = "login";
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseLogin(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult getProfile(Context context, String userId) throws Exception {
        String path = "profile/" + userId;
        InputStream jsonInputStream = createCallBackInputStream(context, new JSONObject(), base_path + path, HttpRequester.REQUEST_GET);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseProfile(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult updateProfile(Context context, JSONObject jsonObject) throws Exception {
        String path = "profile";
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseUpdateProfile(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult updateRoom(Context context, JSONObject jsonObject) throws Exception {
        String path = "room";
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, HttpRequester.REQUEST_PUT);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseUpdateRoom(jsonInputStream);
            return netResult;
        }
        return null;
    }


    public static NetResult getLiveType(Context context, JSONObject jsonObject) throws Exception {
        String path = "live_type";
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, HttpRequester.REQUEST_GET);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseLiveTypes(jsonInputStream);
            return netResult;
        }
        return null;
    }


    public static NetResult getRooms(Context context, int page, String typeId) throws Exception {
        String path = "room?typeid=" + typeId + "&page=" + page;
        InputStream jsonInputStream = createCallBackInputStream(context, new JSONObject(), base_path + path, HttpRequester.REQUEST_GET);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseRooms(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult getRoomsByKind(Context context, int page, String kind) throws Exception {
        String path = "room?kind=" + kind + "&page=" + page;

        InputStream jsonInputStream = createCallBackInputStream(context, new JSONObject(), base_path + path, HttpRequester.REQUEST_GET);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseRooms(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult getRoomsByTypeText(Context context, int page, String type) throws Exception {
        String path = "index/" + type + "?page=" + page;
        InputStream jsonInputStream = createCallBackInputStream(context, new JSONObject(), base_path + path, HttpRequester.REQUEST_GET);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseLiveRoom(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult getHomeRooms(Context context) throws Exception {
        String path = "index";
        InputStream jsonInputStream = createCallBackInputStream(context, new JSONObject(), base_path + path, HttpRequester.REQUEST_GET);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseHomeIndex(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult getHomeRooms2(Context context) throws Exception {
        String path = "index";
        InputStream jsonInputStream = createCallBackInputStream(context, new JSONObject(), base_path + path, HttpRequester.REQUEST_GET);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseHomeIndex2(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult getHomeRooms3(Context context) throws Exception {
        String path = "index";
        InputStream jsonInputStream = createCallBackInputStream(context, new JSONObject(), base_path + path, HttpRequester.REQUEST_GET);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseHomeIndex3(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult uploadAvatar(FormFile[] formFiles, HashMap<String, String> map) throws Exception {
        String path = "updateAvatar";//result
        String json = HttpRequester.postUpload(base_path + path, map, formFiles, App.getApp().getApiToken());
        if (null != json) {
            NetResult netResult = JsonHelper.parseUpload(json);
            return netResult;
        }
        return null;
    }


    public static NetResult uploadScreen(FormFile[] formFiles, HashMap<String, String> map) throws Exception {
        String path = "uploadscreen";//result
        String json = HttpRequester.postUpload(base_path + path, map, formFiles, App.getApp().getApiToken());
        if (null != json) {
            NetResult netResult = JsonHelper.parseUploadScreen(json);
            return netResult;
        }
        return null;
    }


    public static NetResult startEndLive(Context context, JSONObject jsonObject) throws Exception {
        String path = "startendlive";//result//GET /api/startendlive
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseStartEndLive(jsonInputStream);
            return netResult;
        }
        return null;
    }


    public static NetResult searchRoom(Context context, JSONObject jsonObject) throws Exception {
        String path = "search";//result//GET /api/startendlive
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseRooms(jsonInputStream);
            return netResult;
        }
        return null;
    }


    public static NetResult getliveUserPassword(Context context, JSONObject jsonObject) throws Exception {
        String path = "getlivekey";//result//GET /api/startendlive
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, HttpRequester.REQUEST_GET);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseLiveUsersPwd(jsonInputStream);

            return netResult;
        }
        return null;
    }

    public static NetResult uploadreport(Context context, JSONObject jsonObject) throws Exception {
        String path = "report";
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseReport(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult forgetPassword(Context context, JSONObject jsonObject) throws Exception {
        String path = "password/sendemail";
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseForgetPassword(jsonInputStream);
            return netResult;
        }
        return null;
    }


    public static NetResult blackList(Context context, JSONObject jsonObject) throws Exception {
        String path = "blacklist";
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseBlack(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult deleteblackList(Context context, String userid) throws Exception {
        String path = "blacklist/" + userid;
        InputStream jsonInputStream = createCallBackInputStream(context, new JSONObject(), base_path + path, HttpRequester.REQUEST_DELETE);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseForgetPassword(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult getTrailerAll(Context context, String pageId) throws Exception {
        String path = base_path+ "trailerall?page=" + pageId;

        InputStream jsonInputStream = createCallBackInputStream(context, new JSONObject(), path, HttpRequester.REQUEST_GET);
        if (null != jsonInputStream) {
            NetResult netResult = JsonHelper.parseTrailAll(jsonInputStream);
            return netResult;
        }
        return null;
    }


    //=====================common method=========================


    private static InputStream createCallBackInputStream(Context context, JSONObject customizedObject, String path, String requestType) throws Exception {


        JSONObject commonObject = new JSONObject();

        commonObject.put("app_version", Tool.getVersionName(context));
        commonObject.put("client_type", "0");
        commonObject.put("deviceId", Tool.getImei(context));

        customizedObject.put("common", commonObject);


        String jsonRequest = customizedObject.toString();


        //=========================

        String token = null;
        if (null != App.getApp().getUser() && !StringUtils.isEmpty(App.getApp().getApiToken())) {
            token = App.getApp().getApiToken();
        }


        LogUtil.i(App.TAG, "path:" + path);
        LogUtil.i(App.TAG, "" + jsonRequest);
        LogUtil.i(App.TAG, "======token=======");
        LogUtil.e(App.TAG, "" + token);
        LogUtil.i(App.TAG, "======token end=======");
        InputStream jsonInputStream = HttpRequester.doHttpRequestText(context, path, jsonRequest, token, requestType);
        return jsonInputStream;
    }

    /**
     * create request Object from map to jsonobject
     *
     * @param paramMap
     * @return
     * @throws JSONException
     */
    public static JSONObject buildCustomizedObject(HashMap<String, String> paramMap) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        for (Map.Entry<String, String> entry : paramMap.entrySet()) {
            jsonObject.put(entry.getKey(), entry.getValue());
        }

        return jsonObject;
    }
}
