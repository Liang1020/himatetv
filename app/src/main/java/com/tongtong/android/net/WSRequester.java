package com.tongtong.android.net;

import com.common.util.LogUtil;
import com.tongtong.android.App;
import com.tongtong.android.domain.Message;
import com.tongtong.android.domain.Subscribe;
import com.tongtong.android.domain.User;
import com.tongtong.android.json.JsonHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import de.tavendo.autobahn.WebSocket;
import de.tavendo.autobahn.WebSocketConnection;
import de.tavendo.autobahn.WebSocketConnectionHandler;

public class WSRequester {

    //    public static final String host = "192.168.1.121";
    public static final String host = "message.himate.tv";
    //    public static final String host = "106.185.55.185";
    public static final int port = 80;//·   5· ·

    public static WSRequester wsRequester;

    public static WSRequester getInstance() throws Exception {
        if (null == wsRequester || !wsRequester.isConencted()) {
            wsRequester = new WSRequester();
        }
        return wsRequester;
    }

    public static WSRequester getInstance(ConnectionListener connectionListener) throws Exception {
        wsRequester = getInstance();
        wsRequester.setConnectionListener(connectionListener);
        return wsRequester;
    }

    // ================

    private WebSocket webSocket;
    private ConnectionListener connectionListener;

    private WSRequester() throws Exception {

        webSocket = new WebSocketConnection();


        final String wsuri = "ws://" + host + ":" + port;

        webSocket.connect(wsuri, new WebSocketConnectionHandler() {
            @Override
            public void onOpen() {
                startSendPing();
                if (null != connectionListener) {
                    connectionListener.onOpen();
                }
            }

            @Override
            public void onTextMessage(String payload) {

                LogUtil.i(App.TAG, "receive msg:" + payload);

                LogUtil.i("leo", "receive msg:" + payload);
                try {


                    JSONObject jsonObject = new JSONObject(payload);
                    String command = jsonObject.getString("command");

                    if ("message".equals(command)) {
                        if (null != connectionListener) {
                            Message message = Message.jsonObject2Message(payload);
                            LogUtil.i(App.TAG, "receive msg:" + Message.jsonObject2Message(payload).getNickname());
                            connectionListener.onReceiverMessage(Message.jsonObject2Message(payload));
                        }

                    } else if ("subscribe".equals(command)) {
//                        {"command":"subscribe","channel":"1","userid":"1"}
                        LogUtil.v("WSRequester Subscribe", "subscribe  count:"+payload.toString());
                        if (null != connectionListener) {
                            connectionListener.onSubScrible(Subscribe.json2SubScrible(payload));
                        }
                    } else if ("roomusers".equals(command)) {
                        JSONArray array = jsonObject.getJSONArray("users");
                        ArrayList<User> users = new ArrayList<User>();
                        if (null != array && array.length() != 0) {
                            for (int i = 0, isize = array.length(); i < isize; i++) {
                                users.add(JsonHelper.parseUser(array.getJSONObject(i)));
                            }
                        }
                        if (null != connectionListener) {
                            connectionListener.onRoomUsers(users);
                        }
                    } else if ("unsubscribe".equals(command)) {
                        if (null != connectionListener) {
                            connectionListener.onUnSubScrible(Subscribe.json2SubScrible(payload));
                        }
                    } else if ("streamstop".equals(command)) {
                        if (null != connectionListener) {
                            connectionListener.onLiveStop();
                        }
                    } else if ("deny".equals(command)) {
                        if (null != connectionListener) {
                            connectionListener.onDeny();
                        }
                    } else if ("undeny".equals(command)) {
                        if (null != connectionListener) {
                            connectionListener.onUnDeny();
                        }
                    } else if ("like".equals(command)) {
                        if (null != connectionListener) {
                            connectionListener.onLike(jsonObject);
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    LogUtil.e(App.TAG, "parse message error....." + e.getLocalizedMessage());
                }


                if (null != connectionListener) {
                    connectionListener.onReceiveTextMessage(payload);
                }
            }

            @Override
            public void onClose(int code, String reason) {
                if (null != connectionListener) {
                    connectionListener.onClose(code, reason);
                }
            }
        });

    }

    public boolean isConencted() {

        if (null != webSocket && webSocket.isConnected()) {
            return true;
        }
        return false;
    }


    public boolean disconencted() {
        if (null != webSocket && webSocket.isConnected()) {
            webSocket.disconnect();
            return true;
        }
        return false;
    }


    public boolean enter(User user, String roomid) {

        JSONObject jsonObject = new JSONObject();

        try {

            jsonObject.put("command", "subscribe");
            jsonObject.put("channel", roomid);

            if (null == user) {
                jsonObject.put("userid", -1);
            } else {
                jsonObject.put("userid", user.getId());
            }

            if (isConencted() && null != webSocket) {
                String content = jsonObject.toString();
                LogUtil.e(App.TAG, "enter json info:" + content);
                webSocket.sendTextMessage(content);
                return true;
            } else {
                return false;
            }

        } catch (JSONException e) {
            e.printStackTrace();

            return false;
        }

    }


    public boolean like() {

        JSONObject jsonObject = new JSONObject();

        try {

            jsonObject.put("command", "like");

            if (isConencted() && null != webSocket) {
                String content = jsonObject.toString();
                webSocket.sendTextMessage(content);
                return true;
            } else {
                return false;
            }

        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean message(User user, String message) {

        JSONObject jsonObject = new JSONObject();

        try {

            jsonObject.put("command", "message");

            jsonObject.put("nickname", user.getNickName());
            jsonObject.put("userid", user.getUserId());
            jsonObject.put("message", message);


            if (isConencted() && null != webSocket) {
                String content = jsonObject.toString();
                webSocket.sendTextMessage(content);
                return true;
            } else {
                return false;
            }

        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }

    }


    public boolean roomusers() {

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("command", "roomusers");


            if (isConencted() && null != webSocket) {
                String content = jsonObject.toString();
                webSocket.sendTextMessage(content);
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }

    }

    Timer timer;

    public void startSendPing() {
        timer = new Timer();

        LogUtil.e(App.TAG, "start send ping....");
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                if (null != webSocket && webSocket.isConnected()) {
                    webSocket.sendPing();
                    LogUtil.e(App.TAG, "send ping...");
                } else {
                    LogUtil.e(App.TAG, "dis connect send ping...");
                    timer.cancel();
                    cancel();
                }
            }
        }, 0, 30 * 1000);
    }


    public ConnectionListener getConnectionListener() {
        return connectionListener;
    }

    public void setConnectionListener(ConnectionListener connectionListener) {
        this.connectionListener = connectionListener;
    }


    // =========================
    public static abstract class ConnectionListener {

        public void onOpen() {
        }

        /**
         * this method work on background thread
         *
         * @param rmessage
         */
        public void onReceiveTextMessage(String rmessage) {


        }


        public void onRoomUsers(ArrayList<User> users) {


        }


        public void onClose(int code, String reason) {

        }

        public void onReceiverMessage(Message message) {


        }

        public void onLike(JSONObject message) {
        }

        public void onSubScrible(Subscribe subscribe) {

        }

        public void onUnSubScrible(Subscribe subscribe) {

        }

        public void onLiveStop() {

        }

        public void onDeny() {

        }

        public void onUnDeny() {

        }


    }
}
