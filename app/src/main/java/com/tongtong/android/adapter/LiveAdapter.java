package com.tongtong.android.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.common.util.StringUtils;
import com.common.util.Tool;
import com.squareup.picasso.Picasso;
import com.tongtong.android.App;
import com.tongtong.android.R;
import com.tongtong.android.activity.VideoActivity;
import com.tongtong.android.domain.Room;
import com.tongtong.android.utils.DateManager;

import java.util.ArrayList;
import java.util.Date;


/**
 * Created by leocx on 2016/10/18.
 */

public class LiveAdapter extends RecyclerView.Adapter<LiveAdapter.BaseViewHolder> {

    ArrayList<Room> rooms = new ArrayList<>();
    Context context;
    int itemW;
    int itemH;

    public LiveAdapter(Context context, ArrayList<Room> rooms, int itemW, int itemH) {
        this.rooms = rooms;
        this.context = context;
        this.itemH = itemH;
        this.itemW = itemW;
    }

    class BaseViewHolder extends RecyclerView.ViewHolder {
        private ImageView pic;
        private TextView title;
        private TextView name;
        private TextView date;

        public BaseViewHolder(View itemView) {
            super(itemView);
            pic = (ImageView) itemView.findViewById(R.id.iv_timelineRoomItem_pic);
            title = (TextView) itemView.findViewById(R.id.tv_timelineRoomItem_title);
            name = (TextView) itemView.findViewById(R.id.tv_timelineRoomItem_name);
            date = (TextView) itemView.findViewById(R.id.tv_timelineRoomItem_date);
        }

    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_timeline_room, parent, false);
        return new BaseViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        final Room room = rooms.get(position);
        holder.name.setText(room.getName());
        holder.title.setText(room.getTitle());
        holder.date.setText(room.getStartLiveTime());
        if (StringUtils.isEmpty(room.getImgUrl())) {
            Picasso.with(context).load(R.drawable.bg_default_broad2).resize(itemW / 2, (int) (itemH * 1.8)).
                    placeholder(R.drawable.bg_default_broad2).centerCrop().config(Bitmap.Config.RGB_565).into(holder.pic);
        } else {
            holder.pic.getLayoutParams().width = itemW / 2;
            holder.pic.getLayoutParams().height = (int) (itemH * 1.8);
            holder.pic.requestLayout();
            Picasso.with(context).load(room.getImgUrl()).placeholder(R.drawable.bg_default_broad2).
                    config(Bitmap.Config.RGB_565).into(holder.pic);
        }
        holder.pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (room.getIsLive().equals("1")) {
                    Toast.makeText(App.getApp().getBaseContext(), "isLive", Toast.LENGTH_SHORT).show();
                    App.getApp().putTemPObject("room", room);
                    Tool.startActivity(context, VideoActivity.class);
                } else {
                    Date startDate = DateManager.getDateFromString("yyyy-MM-dd kk:mm:ss", room.getStartLiveTime());
                    Date endDate = DateManager.getDateFromString("yyyy-MM-dd kk:mm:ss", room.getEndLiveTime());
                    Date now = new Date();
                    if (endDate == null) {
                        Toast.makeText(App.getApp().getBaseContext(), "is history", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (DateManager.compare(endDate, now) == -1) {
                        Toast.makeText(App.getApp().getBaseContext(), "is history", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(App.getApp().getBaseContext(), "is preview", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return rooms.size();
    }

    public void addData(int position, ArrayList<Room> roomArrayList) {
        for (int i = 0; i < roomArrayList.size(); i++) {
            rooms.add(position, roomArrayList.get(i));
            notifyItemInserted(position);
        }
    }

    public void addData(int position, Room room) {
        rooms.add(position, room);
        notifyItemInserted(position);
    }

    public void removeData(int position) {
        rooms.remove(position);
        notifyItemRemoved(position);
    }
}
