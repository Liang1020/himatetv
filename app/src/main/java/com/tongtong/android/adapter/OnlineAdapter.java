package com.tongtong.android.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.common.util.StringUtils;
import com.squareup.picasso.Picasso;
import com.tongtong.android.R;
import com.tongtong.android.domain.User;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 16/8/14.
 */
public class OnlineAdapter extends BaseAdapter {


    private ArrayList<User> arrayListUsers;

    private Context context;

    private LayoutInflater layoutInflater;

    private onClickSpeechAdapterListener onClickSpeechAdapterListener;

    private Set<String> blackSet;

    public OnlineAdapter(Context context, ArrayList<User> users) {

        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.arrayListUsers = users;

        this.blackSet = new HashSet<>();
        refreshBlack();
    }

    private void refreshBlack() {
        for (User user : this.arrayListUsers) {
            String black = user.getBlack();
            if (!"0".equals(black)) {

                blackSet.add(user.getUserId());

            }
        }
    }


    public void blackSet(String useId) {

        if (blackSet.contains(useId)) {
            blackSet.remove(useId);
        } else {
            blackSet.add(useId);
        }

        refreshBlack();

        notifyDataSetChanged();
    }


    public void removeFromBlack(String userId) {
        if (blackSet.contains(userId)) {
            blackSet.remove(userId);
        }
//        refreshBlack();
        notifyDataSetChanged();

    }

    public void add2Black(String userId) {
        blackSet.add(userId);

//        refreshBlack();
        notifyDataSetChanged();

    }


    public void addUser(User user) {
        this.arrayListUsers.add(user);
        refreshBlack();

        notifyDataSetChanged();
    }


    public void addAllUser(ArrayList<User> users) {
        this.arrayListUsers.addAll(users);
        notifyDataSetChanged();
    }

    public void addAllNewUser(ArrayList<User> users) {

//        if (ListUtiles.isEmpty(users)) {
//            return;
//        }

        this.arrayListUsers.clear();
        this.arrayListUsers.addAll(users);
        refreshBlack();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return arrayListUsers.size();
    }

    @Override
    public Object getItem(int i) {
        return arrayListUsers.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {


        ViewHolder viewHolder = null;
        if (null == view) {
            view = layoutInflater.inflate(R.layout.on_line_item, null);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        final User user = arrayListUsers.get(i);

        if (!StringUtils.isEmpty(user.getAvatar())) {
            Picasso.with(context).load(user.getAvatar()).config(Bitmap.Config.RGB_565).resize(50, 50).placeholder(R.drawable.bg_default_header).into(viewHolder.roundImageViewProfile);
        } else {
            Picasso.with(context).load(R.drawable.bg_default_header).config(Bitmap.Config.RGB_565).resize(50, 50).placeholder(R.drawable.bg_default_header).into(viewHolder.roundImageViewProfile);
        }

        viewHolder.textViewName.setText(user.getNickName());

        if (blackSet.contains(user.getUserId())) {
            viewHolder.buttonSpeech.setBackgroundResource(R.drawable.icon_alow_speak);
            viewHolder.buttonSpeech.setText("解除禁言");
            viewHolder.buttonSpeech.setTextColor(Color.parseColor("#E60013"));
        } else {
            viewHolder.buttonSpeech.setBackgroundResource(R.drawable.icon_no_speak);
            viewHolder.buttonSpeech.setText("禁言");
            viewHolder.buttonSpeech.setTextColor(Color.parseColor("#9b9b9b"));

        }

        viewHolder.buttonSpeech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != onClickSpeechAdapterListener) {
                    onClickSpeechAdapterListener.onClickSpeech(user, OnlineAdapter.this, isInBackList(user.getUserId()));
                }
            }
        });


        return view;
    }


    public boolean isInBackList(String userId) {
        return blackSet.contains(userId);
    }


    class ViewHolder {

        @InjectView(R.id.roundImageViewProfile)
        ImageView roundImageViewProfile;

        @InjectView(R.id.textViewName)
        TextView textViewName;

        @InjectView(R.id.textViewNameLastGo)
        TextView textViewNameLastGo;

        @InjectView(R.id.buttonSpeech)
        Button buttonSpeech;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

    public static interface onClickSpeechAdapterListener {

        public void onClickSpeech(User user, OnlineAdapter onlineAdapter, boolean isback);

    }

    public OnlineAdapter.onClickSpeechAdapterListener getOnClickSpeechAdapterListener() {
        return onClickSpeechAdapterListener;
    }

    public void setOnClickSpeechAdapterListener(OnlineAdapter.onClickSpeechAdapterListener onClickSpeechAdapterListener) {
        this.onClickSpeechAdapterListener = onClickSpeechAdapterListener;
    }
}
