package com.tongtong.android.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.squareup.picasso.Picasso;
import com.tongtong.android.App;
import com.tongtong.android.R;
import com.tongtong.android.activity.VideoActivity;
import com.tongtong.android.domain.Room;
import com.tongtong.android.utils.DateManager;

import java.util.ArrayList;
import java.util.Date;


/**
 * Created by leocx on 2016/10/18.
 */

public class TimelineAdapter extends RecyclerView.Adapter<TimelineAdapter.BaseViewHolder> {

    ArrayList<Room> rooms = new ArrayList<>();
    Context context;
    int itemW;
    int itemH;
    int type;
    public static int SHOW_TYPE_LIVE = 0x0001;
    public static int SHOW_TYPE_HISTORY = 0x0002;
    public static int SHOW_TYPE_PREVIEW = 0x0003;
    private int showType = SHOW_TYPE_PREVIEW;

    public TimelineAdapter(Context context, ArrayList<Room> rooms, int itemW, int itemH, int type) {
        this.rooms = rooms;
        this.context = context;
        this.itemH = itemH;
        this.itemW = itemW;
        this.type = type;
    }

    class BaseViewHolder extends RecyclerView.ViewHolder {
        private ImageView pic;
        private TextView title;
        private TextView name;
        private TextView date;

        public BaseViewHolder(View itemView) {
            super(itemView);
            pic = (ImageView) itemView.findViewById(R.id.iv_timelineRoomItem_pic);
            title = (TextView) itemView.findViewById(R.id.tv_timelineRoomItem_title);
            name = (TextView) itemView.findViewById(R.id.tv_timelineRoomItem_name);
            date = (TextView) itemView.findViewById(R.id.tv_timelineRoomItem_date);
        }

    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (type == 0) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_timeline_room, parent, false);
            return new BaseViewHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_live_room, parent, false);
            return new BaseViewHolder(itemView);
        }
    }

    private int getShowType(Room room) {
        int result;
        if ("1".equals(room.getIsLive())) {
            result = SHOW_TYPE_LIVE;
        } else {
            Date start = DateManager.getDateFromString(room.getStartLiveTime(), "yyyy-MM-dd kk:mm:ss");
            Date end = DateManager.getDateFromString(room.getEndLiveTime(), "yyyy-MM-dd kk:mm:ss");
            if (DateManager.compare(start, new Date()) == 1) {
                result = SHOW_TYPE_PREVIEW;
            } else if (end == null) {
                result = SHOW_TYPE_HISTORY;
            } else if (DateManager.compare(end, new Date()) == 1) {
                result = SHOW_TYPE_PREVIEW;
            } else {
                result = SHOW_TYPE_HISTORY;
            }
        }
        LogUtil.v("leo", room.getTitle() + "   " + room.getStartLiveTime() + "   " + room.getEndLiveTime() + "   " + result + "===" + showType);


        return result;

    }


    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        int roomPosition;
        if (type == 0) {
            //this is the home page
            roomPosition = position;
        } else {
            //this is the live page
            roomPosition = getItemCount() - position - 1;
        }
        final Room room = rooms.get(roomPosition);
        if (type == 0) {

        } else {
            if (getShowType(room) != showType) {
                holder.pic.setVisibility(View.GONE);
                holder.name.setVisibility(View.GONE);
                holder.title.setVisibility(View.GONE);
                holder.date.setVisibility(View.GONE);
            } else {
                holder.pic.setVisibility(View.VISIBLE);
                holder.name.setVisibility(View.VISIBLE);
                holder.title.setVisibility(View.VISIBLE);
                holder.date.setVisibility(View.VISIBLE);
            }
        }


        holder.name.setText(room.getUser().getNickName());
        holder.title.setText(room.getTitle());
        holder.date.setTypeface(null, Typeface.BOLD);

        holder.date.setText(DateManager.getStringFromDate(DateManager.getDateFromString(room.getStartLiveTime(), "yyyy-MM-dd kk:mm:ss"), "hh:mm a | dd MMM yyyy"));
        if (type == 0) {
            if (StringUtils.isEmpty(room.getImgUrl())) {
                Picasso.with(context).load(R.drawable.bg_default_broad2).resize(itemW / 2, (int) (itemH * 1.8)).
                        placeholder(R.drawable.bg_default_broad2).centerCrop().config(Bitmap.Config.RGB_565).into(holder.pic);
            } else {
                holder.pic.getLayoutParams().width = itemW / 2;
                holder.pic.getLayoutParams().height = (int) (itemH * 1.8);
                holder.pic.requestLayout();
                Picasso.with(context).load(room.getImgUrl()).placeholder(R.drawable.bg_default_broad2).
                        config(Bitmap.Config.RGB_565).into(holder.pic);
            }
        } else {
            if (StringUtils.isEmpty(room.getImgUrl())) {
                Picasso.with(context).load(R.drawable.bg_default_broad2).resize(itemW / 4, (int) (itemH * 1.0)).
                        placeholder(R.drawable.bg_default_broad2).centerCrop().config(Bitmap.Config.RGB_565).into(holder.pic);
            } else {
                holder.pic.getLayoutParams().width = (int) (itemW / 2.5);
                holder.pic.getLayoutParams().height = (int) (itemH * 1.0);
                holder.pic.requestLayout();
                Picasso.with(context).load(room.getImgUrl()).placeholder(R.drawable.bg_default_broad2).
                        config(Bitmap.Config.RGB_565).into(holder.pic);
            }
        }
        holder.pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (room.getIsLive().equals("1")) {
//                    Toast.makeText(App.getApp().getBaseContext(), "isLive", Toast.LENGTH_SHORT).show();
                    App.getApp().putTemPObject("room", room);
                    Tool.startActivity(context, VideoActivity.class);
                } else {
                    Date startDate = DateManager.getDateFromString("yyyy-MM-dd kk:mm:ss", room.getStartLiveTime());
                    Date endDate = DateManager.getDateFromString("yyyy-MM-dd kk:mm:ss", room.getEndLiveTime());
                    Date now = new Date();
                    if (endDate == null) {
//                        Toast.makeText(App.getApp().getBaseContext(), "is history", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (DateManager.compare(endDate, now) == -1) {
//                        Toast.makeText(App.getApp().getBaseContext(), "is history", Toast.LENGTH_SHORT).show();
                    } else {
//                        Toast.makeText(App.getApp().getBaseContext(), "is preview", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return rooms.size();
    }

    public void addData(int position, ArrayList<Room> roomArrayList) {
        for (int i = 0; i < roomArrayList.size(); i++) {
            rooms.add(position, roomArrayList.get(i));
            notifyItemInserted(position);
        }
        LogUtil.v("leo", rooms.toString());
    }

    public void addData(int position, Room room) {
        rooms.add(position, room);
        notifyItemInserted(position);
    }

    public void removeData(int position) {
        rooms.remove(position);
        notifyItemRemoved(position);
    }

    public void setData(ArrayList<Room> rooms) {
        this.rooms = rooms;
        notifyDataSetChanged();
    }

    public void setShowType(int showType) {
        rooms.size();
        this.showType = showType;
        notifyDataSetChanged();
    }

}
