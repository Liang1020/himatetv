package com.tongtong.android.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.squareup.picasso.Picasso;
import com.tongtong.android.App;
import com.tongtong.android.R;
import com.tongtong.android.activity.VideoActivity;
import com.tongtong.android.domain.Room;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 16/8/9.
 */
public class RoomAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Room> arrayListRooms;
    private LayoutInflater layoutInflater;
    private int itemW, itemH;
    private ListView listView;

    public RoomAdapter(Context context, ArrayList<Room> rooms, int itemW, int itemH) {
        this.context = context;
        this.arrayListRooms = rooms;
        this.layoutInflater = LayoutInflater.from(context);
        this.itemW = itemW;
        this.itemH = itemH;
    }

    public RoomAdapter(Context context, ArrayList<Room> rooms, ListView listView) {
        this.context = context;
        this.arrayListRooms = rooms;
        this.layoutInflater = LayoutInflater.from(context);
        this.listView = listView;
    }


    @Override
    public int getCount() {

        int totalCount = arrayListRooms.size();

        int count = totalCount / 2 + totalCount % 2;

        return count;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int pos, View view, ViewGroup viewGroup) {

        ViewHolder viewHolder = null;

        if (null == view) {
            view = layoutInflater.inflate(R.layout.item_room, null);

            android.widget.AbsListView.LayoutParams vlp;

            if (itemW != 0) {
                vlp = new android.widget.AbsListView.LayoutParams(itemW, itemH);
            } else {
                vlp = new android.widget.AbsListView.LayoutParams(listView.getWidth(), listView.getHeight()/4);
            }

            view.setLayoutParams(vlp);

            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }


        Room leftRoom = arrayListRooms.get(pos * 2);
        setImgForHolder(leftRoom, viewHolder.relativeContent1);

        if (pos * 2 + 1 < arrayListRooms.size()) {
            Room room = arrayListRooms.get((pos * 2 + 1));
            setImgForHolder(room, viewHolder.relativeContent2);
        } else {
            setImgForHolder(null, viewHolder.relativeContent2);
        }

        return view;
    }


    private void setImgForHolder(final Room room, View view) {

        if (null == room) {

            TextView img = ((TextView) view.findViewWithTag("text"));//.setText("");
            img.setText(null);
            ImageView imageView = ((ImageView) view.findViewWithTag("img"));
            imageView.setImageBitmap(null);

            view.setVisibility(View.INVISIBLE);

            view.setOnClickListener(null);

        } else {
            view.setVisibility(View.VISIBLE);

            TextView textView = ((TextView) view.findViewWithTag("text"));//.setText("");
            textView.setText(room.getSubject());

            ImageView imageView = ((ImageView) view.findViewWithTag("img"));


            String imgurl = room.getImgUrl();


            if (itemW == 0) {

                itemW = listView.getWidth();
                itemH = listView.getHeight() / 4;
            }


            if (StringUtils.isEmpty(imgurl)) {
                Picasso.with(context).load(R.drawable.bg_default_broad2).resize(itemW / 2, itemH).placeholder(R.drawable.bg_default_broad2).centerCrop().config(Bitmap.Config.RGB_565).into(imageView);

            } else {
                Picasso.with(context).load(imgurl).resize(itemW / 2,itemH).placeholder(R.drawable.bg_default_broad2).centerCrop().config(Bitmap.Config.RGB_565).into(imageView);
            }

//
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    LogUtil.i(App.TAG, "click room:" + room.getContents());

                    App.getApp().putTemPObject("room", room);

                    Tool.startActivity(context, VideoActivity.class);

                }
            });

//
//            Glide.with(context)
//                    .load(room.getImgUrl())
//                    .centerCrop()
//                    .placeholder(R.drawable.icon_living)
//                    .crossFade()
//                    .into(imageView);


        }


    }


    static class ViewHolder {


        @InjectView(R.id.textViewDesc1)
        TextView textViewDesc;

        @InjectView(R.id.imageView1)
        ImageView imageView1;


        @InjectView(R.id.textViewDesc2)
        TextView textViewDesc2;

        @InjectView(R.id.imageView2)
        ImageView imageView2;


        @InjectView(R.id.relativeContent1)
        RelativeLayout relativeContent1;

        @InjectView(R.id.relativeContent2)
        RelativeLayout relativeContent2;

        public ViewHolder(View view) {

            ButterKnife.inject(this, view);
        }

    }
}
