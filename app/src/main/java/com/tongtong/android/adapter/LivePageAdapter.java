package com.tongtong.android.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tongtong.android.activity.MainActivity;
import com.tongtong.android.domain.LiveType;
import com.tongtong.android.view.RoomView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by clia on 2016/7/27.
 */
public class LivePageAdapter extends PagerAdapter {

    private final static String TAG = "LivePage";

    private Context context;
    private List<LiveType> liveTypes;
    private ArrayList<View> liveTypeView;
    private LayoutInflater layoutInflater;
    private ArrayList<RoomView> roomViews;

    private int itemHeight=0;
    private boolean isLive=false;

    public LivePageAdapter(Context context, List<LiveType> liveTypes,int itemHeight,boolean isLive) {
        this.context = context;
        this.liveTypes = liveTypes;
        this.liveTypeView = new ArrayList<>();
        this.layoutInflater = LayoutInflater.from(context);
        this.roomViews = new ArrayList<>();
        this.itemHeight=itemHeight;
        this.isLive=isLive;

        for (LiveType liveType : liveTypes) {

            RoomView roomView = new RoomView((MainActivity) context, liveType.getId(),itemHeight,isLive);
            roomViews.add(roomView);
            liveTypeView.add(roomView.getRootView());
        }
    }


    public void onShowIndex(int index){
        roomViews.get(index).onShow();
    }


    @Override
    public int getCount() {
        return liveTypes.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return o == view;
    }


    @Override
    public CharSequence getPageTitle(int position) {

        return liveTypes.get(position).getType();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View view = liveTypeView.get(position);

        container.addView(view);


        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }


}
