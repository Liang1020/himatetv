package com.tongtong.android.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.squareup.picasso.Picasso;
import com.tongtong.android.App;
import com.tongtong.android.R;
import com.tongtong.android.activity.VideoActivity;
import com.tongtong.android.domain.Room;
import com.tongtong.android.net.NetInterface;
import com.tongtong.android.utils.DateManager;
import com.tongtong.android.view.DividerLine;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 16/8/9.
 */
public class RoomHomeAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Room> arrayListRooms;
    private LayoutInflater layoutInflater;
    private int itemW, itemH;
    private View viewFirstItem;
    private View viewSecondItem;

    private int type_fist = 0;
    private int type_normal = 1;
    private int listViewtotalheight = 0;
    public RecyclerView recyclerView;
    private TimelineAdapter baseRecyclerViewAdapter;
    private LinearLayoutManager mLayoutManager;
    private int requestPage = 2;
    private boolean isLoadingPage = false;
    private int type;

    public RoomHomeAdapter(final Context context, ArrayList<Room> rooms, int itemW, int itemH, int listViewtotalheight, View homeViewItem, int type) {

        this.context = context;
        this.arrayListRooms = rooms;
        this.layoutInflater = LayoutInflater.from(context);
        this.itemW = itemW;
        this.itemH = itemH;
        this.viewFirstItem = homeViewItem;
        this.listViewtotalheight = listViewtotalheight;
        this.type = type;

        recyclerView = new RecyclerView(context);
        baseRecyclerViewAdapter = new TimelineAdapter(context, arrayListRooms, itemW, itemH, type);
        mLayoutManager = new LinearLayoutManager(context);
        if (type == 0) {
            mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            recyclerView.addItemDecoration(new DividerLine(DividerLine.HORIZONTAL));
        } else {
            mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView.addItemDecoration(new DividerLine(DividerLine.VERTICAL));
        }
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(baseRecyclerViewAdapter);
    }


    @Override
    public int getItemViewType(int position) {

        if (position == 0) {
            return type_fist;
        }

        return type_normal;
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public int getCount() {

//        int totalCount = arrayListRooms.size();
//        int count = totalCount / 2 + totalCount % 2 + 1;

        return 2;
//        return count;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    int overallXScroll = 0;

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public View getView(int pos, View view, ViewGroup viewGroup) {

//        recyclerView.getLayoutParams().height = (int) (itemH * 1.8);
        if (pos == 0) {
            if (null == view) {
                android.widget.AbsListView.LayoutParams vlp = new android.widget.AbsListView.LayoutParams(itemW, (int)(listViewtotalheight / 1.5));
                viewFirstItem.setLayoutParams(vlp);
                RelativeLayout tv_header_title = (RelativeLayout) viewFirstItem.findViewById(R.id.relativeLayoutMore);
                LinearLayout ll_live_header = (LinearLayout) viewFirstItem.findViewById(R.id.ll_live_header);
                if (type == 1) {
                    tv_header_title.setVisibility(View.GONE);
                    ll_live_header.setVisibility(View.VISIBLE);
                } else {
                    tv_header_title.setVisibility(View.VISIBLE);
                    ll_live_header.setVisibility(View.GONE);
                }

                final TextView tv_preview = (TextView) viewFirstItem.findViewById(R.id.tv_preview);
                final TextView tv_history = (TextView) viewFirstItem.findViewById(R.id.tv_history);
                tv_preview.setTextColor(Color.RED);
                tv_history.setTextColor(Color.BLACK);
                tv_preview.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
//                        Toast.makeText(context, "preview", Toast.LENGTH_SHORT).show();
                        baseRecyclerViewAdapter.setShowType(TimelineAdapter.SHOW_TYPE_PREVIEW);
                        tv_preview.setTextColor(Color.RED);
                        tv_history.setTextColor(Color.BLACK);
                    }
                });
                tv_history.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
//                        Toast.makeText(context, "history", Toast.LENGTH_SHORT).show();
                        baseRecyclerViewAdapter.setShowType(TimelineAdapter.SHOW_TYPE_HISTORY);
                        tv_preview.setTextColor(Color.BLACK);
                        tv_history.setTextColor(Color.RED);
                    }
                });
                return viewFirstItem;
            } else {
                return view;
            }
        } else {
            android.widget.AbsListView.LayoutParams vlp = new android.widget.AbsListView.LayoutParams(itemW, (int)(listViewtotalheight - (listViewtotalheight / 1.5)));

            recyclerView.setLayoutParams(vlp);
            if (type == 0) {
                boolean isTodayExist = false;
                for (int i = 0; i < arrayListRooms.size(); i++) {
                    Room room = arrayListRooms.get(i);
                    Date date = DateManager.getDateFromString(room.getStartLiveTime(), "yyyy-MM-dd kk:mm:ss");
                    if (DateManager.compareByDay(date, new Date()) == -1) {
                        if (i == 0) {
                        } else {
                            mLayoutManager.scrollToPositionWithOffset(i - 2, itemW / 4);
                            overallXScroll = (i - 1) * itemW / 2 + itemW / 4;
                            isTodayExist = true;
                        }
                    }
                }
                if (!isTodayExist) {
                    mLayoutManager.scrollToPositionWithOffset(arrayListRooms.size() / 2, itemW / 4);
                    overallXScroll = (arrayListRooms.size() / 2) * itemW / 2 + itemW / 4;
                }
                recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
//                    LogUtil.v("leo", recyclerView.toString() + "    state: " + newState);
                        if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                            if (RecyclerViewPositionHelper.createHelper(recyclerView).findFirstVisibleItemPosition() <= 3) {
                                if (!isLoadingPage) {
                                    isLoadingPage = true;
                                    BaseTask addTimelineTask = new BaseTask(context, new NetCallBack() {
                                        @Override
                                        public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                                            NetResult netResult1 = null;
                                            try {
                                                netResult1 = NetInterface.getTrailerAll(context, "" + requestPage);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                LogUtil.e(App.TAG, "Trailer error:" + e.getLocalizedMessage());
                                            }
                                            if (netResult1 == null) {
                                                netResult1.setCode(NetResult.CODE_ERROR);
                                            } else if (netResult1.isOk()) {
                                                netResult1.setCode(NetResult.CODE_OK);
                                            } else {
                                                netResult1.setCode(NetResult.CODE_ERROR);
                                            }
                                            return netResult1;
                                        }

                                        @Override
                                        public void onFinish(NetResult result, BaseTask baseTask) {
                                            if (null != result && result.isOk()) {

                                                ArrayList<Room> timeLine = new ArrayList<>();
                                                for (int i = 0; i < result.getData().length; i++) {
                                                    timeLine.add((Room) result.getData()[i]);
                                                }
                                                addTimeline(timeLine);
                                                isLoadingPage = false;
                                                if (result.getData().length > 0) {
                                                    requestPage++;
                                                }
                                            }
                                        }

                                        @Override
                                        public void onCanCell(BaseTask baseTask) {
                                        }
                                    });
                                    addTimelineTask.execute(new HashMap<String, String>());
//                                Toast.makeText(App.getApp().getBaseContext(), "load new page " + requestPage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        overallXScroll += dx;

                    }
                });
                return recyclerView;
            } else {
                recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
//                    LogUtil.v("leo", recyclerView.toString() + "    state: " + newState);
                        if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                            if (RecyclerViewPositionHelper.createHelper(recyclerView).findFirstVisibleItemPosition() >= baseRecyclerViewAdapter.getItemCount() - 3) {
                                if (!isLoadingPage) {
                                    isLoadingPage = true;
                                    BaseTask addTimelineTask = new BaseTask(context, new NetCallBack() {
                                        @Override
                                        public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                                            NetResult netResult1 = null;
                                            try {
                                                netResult1 = NetInterface.getTrailerAll(context, "" + requestPage);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                LogUtil.e(App.TAG, "Trailer error:" + e.getLocalizedMessage());
                                            }
                                            if (netResult1 == null) {
                                                netResult1.setCode(NetResult.CODE_ERROR);
                                            } else if (netResult1.isOk()) {
                                                netResult1.setCode(NetResult.CODE_OK);
                                            } else {
                                                netResult1.setCode(NetResult.CODE_ERROR);
                                            }
                                            return netResult1;
                                        }

                                        @Override
                                        public void onFinish(NetResult result, BaseTask baseTask) {
                                            if (null != result && result.isOk()) {

                                                ArrayList<Room> timeLine = new ArrayList<>();
                                                for (int i = 0; i < result.getData().length; i++) {
                                                    timeLine.add((Room) result.getData()[i]);
                                                }
                                                addTimeline(timeLine);
                                                isLoadingPage = false;
                                                if (result.getData().length > 0) {
                                                    requestPage++;
                                                }
                                            }
                                        }

                                        @Override
                                        public void onCanCell(BaseTask baseTask) {
                                        }
                                    });
                                    addTimelineTask.execute(new HashMap<String, String>());
                                }
                            }
                        }
                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        overallXScroll += dx;
                    }
                });
                return recyclerView;
            }
        }
    }

    public void addTimeline(ArrayList<Room> list) {
        baseRecyclerViewAdapter.addData(0, list);
    }

    public void printList() {
        LogUtil.v("leo", arrayListRooms.toString());
    }

    private void setImgForHolder(final Room room, View view) {

        if (null == room) {

            TextView img = ((TextView) view.findViewWithTag("text"));//.setText("");
            img.setText(null);
            ImageView imageView = ((ImageView) view.findViewWithTag("img"));
            imageView.setImageBitmap(null);
            view.setVisibility(View.INVISIBLE);
            view.setOnClickListener(null);

        } else {
            view.setVisibility(View.VISIBLE);

            TextView img = ((TextView) view.findViewWithTag("text"));//.setText("");
            img.setText(room.getSubject());

            ImageView imageView = ((ImageView) view.findViewWithTag("img"));


            String imgurl = room.getImgUrl();
            if (StringUtils.isEmpty(imgurl)) {
                Picasso.with(context).load(R.drawable.bg_default_broad2).resize(itemW / 2, itemH).placeholder(R.drawable.bg_default_broad2).centerCrop().config(Bitmap.Config.RGB_565).into(imageView);
            } else {
                Picasso.with(context).load(imgurl).resize(itemW / 2, itemH).placeholder(R.drawable.bg_default_broad2).centerCrop().config(Bitmap.Config.RGB_565).into(imageView);
            }

//
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    LogUtil.i(App.TAG, "click room:" + room.getContents());

                    App.getApp().putTemPObject("room", room);

                    Tool.startActivity(context, VideoActivity.class);

                }
            });

        }

    }


    static class ViewHolder {


        @InjectView(R.id.textViewDesc1)
        TextView textViewDesc;

        @InjectView(R.id.imageView1)
        ImageView imageView1;


        @InjectView(R.id.textViewDesc2)
        TextView textViewDesc2;

        @InjectView(R.id.imageView2)
        ImageView imageView2;

        @InjectView(R.id.relativeContent1)
        RelativeLayout relativeContent1;

        @InjectView(R.id.relativeContent2)
        RelativeLayout relativeContent2;

        public ViewHolder(View view) {

            ButterKnife.inject(this, view);
        }

    }
}
