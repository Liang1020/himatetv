package com.tongtong.android.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.common.util.Constant;
import com.common.util.StringUtils;
import com.common.view.RelativeTimeTextView;
import com.common.view.RoundImageView;
import com.squareup.picasso.Picasso;
import com.tongtong.android.App;
import com.tongtong.android.R;
import com.tongtong.android.domain.Message;

import java.util.ArrayList;

public class MessageAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater layoutInflater;

    private ArrayList<Message> messages;

    public MessageAdapter(Context context, ArrayList<Message> messages) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.messages = messages;

    }

    public void addMsg(Message msg) {

        messages.add(msg);
        notifyDataSetChanged();
    }

    public void addMsgList(ArrayList<Message> newMsgs) {
        messages.addAll(newMsgs);
        notifyDataSetChanged();
    }


    public int getCount() {

        return messages.size();

    }

    public Message getItem(int position) {


        return messages.get(position);
    }

    public long getItemId(int position) {
        return position;
    }


    public int getViewTypeCount() {
        return 2;
    }


    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (convertView == null) {

            convertView = layoutInflater.inflate(R.layout.item_message, null);

            viewHolder = new ViewHolder();
            viewHolder.tvSendTime = (RelativeTimeTextView) convertView.findViewById(R.id.tv_sendtime);
            viewHolder.tvContent = (TextView) convertView.findViewById(R.id.tv_chatcontent);
            viewHolder.tvTime = (TextView) convertView.findViewById(R.id.tv_time);
            viewHolder.roundImageViewHeader = (RoundImageView) convertView.findViewById(R.id.roundImageViewHeader);
            viewHolder.tvUserName = (TextView) convertView.findViewById(R.id.textViewNickname);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Message msg = messages.get(position);

        //====================

        String label = msg.getTime();
        try {
            viewHolder.tvSendTime.setReferenceTime(Constant.SDFD4.parse(label).getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }


        viewHolder.tvUserName.setText(msg.getNickname());

        if (StringUtils.isEmpty(msg.getAvatar())) {
            Picasso.with(App.getApp()).load(R.drawable.bg_default_header).resize(50, 50).config(Bitmap.Config.RGB_565).into(viewHolder.roundImageViewHeader);
        } else {
            Picasso.with(App.getApp()).load(msg.getAvatar()).placeholder(R.drawable.bg_default_header).resize(50, 50).config(Bitmap.Config.RGB_565).into(viewHolder.roundImageViewHeader);
        }
        //====================
        viewHolder.tvContent.setText(msg.getMessage());
        viewHolder.tvContent.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

        return convertView;
    }


    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    static class ViewHolder {

        public RelativeTimeTextView tvSendTime;
        public TextView tvUserName;
        public TextView tvContent;
        public TextView tvTime;
        public RoundImageView roundImageViewHeader;
    }


    public static interface IMsgViewType {
        int IMVT_COM_MSG = 0;
        int IMVT_TO_MSG = 1;
    }


}
