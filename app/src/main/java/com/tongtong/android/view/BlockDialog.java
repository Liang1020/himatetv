package com.tongtong.android.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.common.util.StringUtils;
import com.squareup.picasso.Picasso;
import com.tongtong.android.App;
import com.tongtong.android.R;
import com.tongtong.android.domain.User;

/**
 * Created by leocx on 2016/9/2.
 */
public abstract class BlockDialog extends MyBasePicker implements View.OnClickListener {

    TextView tv_block_name;
    ImageButton ib_block_close;
    ImageButton ib_block_blockUser;
    CircleImageView circleImageView;

    public BlockDialog(Context context, User user) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.dialog_block, contentContainer);
        tv_block_name = (TextView) findViewById(R.id.tv_block_name);
        ib_block_close = (ImageButton) findViewById(R.id.ib_block_close);
        ib_block_blockUser = (ImageButton) findViewById(R.id.ib_block_blockUser);

        circleImageView=(CircleImageView) findViewById(R.id.circleImageView);


        ib_block_close.setOnClickListener(this);
        ib_block_blockUser.setOnClickListener(this);
        tv_block_name.setText(user.getNickName());

        String avatar = user.getAvatar();
        if (StringUtils.isEmpty(avatar)) {
            Picasso.with(context).load(R.drawable.bg_default_header).resize(512,512).placeholder(R.drawable.bg_default_header).into(circleImageView);
        } else {
            Picasso.with(context).load(avatar).resize(512,512).placeholder(R.drawable.bg_default_header).into(circleImageView);
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ib_block_close:
                dismiss();
                break;
            case R.id.ib_block_blockUser:
                Toast.makeText(App.getApp().getBaseContext(), "block this user", Toast.LENGTH_SHORT).show();
                onBlock();
                dismiss();
                break;
        }

    }

    abstract protected void onBlock();
}
