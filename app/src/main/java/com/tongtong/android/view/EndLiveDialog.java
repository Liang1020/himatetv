package com.tongtong.android.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.common.util.StringUtils;
import com.tongtong.android.R;

/**
 * Created by clia on 2016/8/26.
 */
public class EndLiveDialog extends MyBasePicker{
    private OnConfirmDialogListener onConfirmDialogListener;

    private EditText editTextForgetPwd;

    public EndLiveDialog(Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.dialog_end_live, contentContainer);

        initSubviews("");
    }


    private void initSubviews(String email) {

        editTextForgetPwd = (EditText) findViewById(R.id.editTextForgetPwd);

        if (!StringUtils.isEmpty(email)) {
            (editTextForgetPwd).setText(email);
        }


        findViewById(R.id.buttonYes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onConfirmDialogListener.OnConfirmClick();
                dismiss();
            }
        });

        findViewById(R.id.buttonCancel).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onConfirmDialogListener.OncancelClick();
                dismiss();
            }
        });

    }


    public OnConfirmDialogListener getOnConfirmDialogListener() {
        return onConfirmDialogListener;
    }

    public void setOnConfirmDialogListener(OnConfirmDialogListener onConfirmDialogListener) {
        this.onConfirmDialogListener = onConfirmDialogListener;
    }

    public static interface OnConfirmDialogListener {

        public void OnConfirmClick();

        public void OncancelClick();
    }
}
