package com.tongtong.android.view;

import android.graphics.Point;
import android.os.AsyncTask;
import android.view.View;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.Tool;
import com.common.view.BasePage;
import com.markmao.pulltorefresh.widget.XListView;
import com.tongtong.android.App;
import com.tongtong.android.R;
import com.tongtong.android.activity.MainActivity;
import com.tongtong.android.adapter.RoomAdapter;
import com.tongtong.android.domain.Room;
import com.tongtong.android.net.NetInterface;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by wangzy on 16/8/9.
 */
public class RoomView extends BasePage<MainActivity> {

    private String type;
    private XListView listView;
    private ArrayList<Room> arrayListRooms;
    private RoomAdapter roomAdapter;

    private int startPage = 1;

    private int page = startPage;
    private int total;

    private int itemHeight;
    private boolean isLive;


    public RoomView(MainActivity activity, String type, int itemHeight, boolean isLive) {
        super(activity);
        this.type = type;
        this.arrayListRooms = new ArrayList<>();
        this.itemHeight = itemHeight;
        this.isLive = isLive;
        initView();
    }


    @Override
    public void initView() {
        this.rootView = View.inflate(activity, R.layout.page_room, null);
        this.listView = (XListView) findViewById(R.id.listView);

        this.listView.setPullRefreshEnable(true);
        this.listView.setPullLoadEnable(true);
        this.listView.getFooterView().setVisibility(View.GONE);

        Point p = Tool.getDisplayMetrics(activity);


        roomAdapter = new RoomAdapter(activity, arrayListRooms, p.x, itemHeight);
        listView.setAdapter(roomAdapter);


        this.listView.setXListViewListener(new XListView.IXListViewListener() {
            @Override
            public void onRefresh() {

                page = startPage;
                requestRooms(true);

            }

            @Override
            public void onLoadMore() {

                page++;
                requestRooms(false);

            }
        });
    }

    @Override
    public void onShow() {

        if (ListUtiles.isEmpty(arrayListRooms)) {
            listView.autoRefresh();
        }

    }


    BaseTask roomTask;

    private void requestRooms(final boolean isRefresh) {
        if (null != roomTask && roomTask.getStatus() == AsyncTask.Status.RUNNING) {
            roomTask.cancel(true);
        }


        roomTask = new BaseTask(activity, new NetCallBack() {


            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netresult = null;

                try {
                    if (isLive) {
                        netresult = NetInterface.getRoomsByTypeText(activity, page, type);
                    } else {
                        netresult = NetInterface.getRooms(activity, page, type);
                    }

                } catch (Exception e) {

                }

                return netresult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {

                if (null != result && result.isOk()) {
                    ArrayList rooms = (ArrayList<Room>) result.getData()[0];
                    total = (Integer) result.getData()[1];
                    LogUtil.i(App.TAG, "totao size:" + total);
                    if (isRefresh) {
                        arrayListRooms.clear();
                    }
                    arrayListRooms.addAll(rooms);
                    roomAdapter.notifyDataSetChanged();
                }


                if (isRefresh == false) {
                    if (null == result || !result.isOk()) {
                        page--;
                    }
                }

                stopXlist(listView);
                dealFooter(arrayListRooms, total, listView);
            }
        });


        roomTask.execute(new HashMap<String, String>());

    }


    protected void stopXlist(XListView xlist) {
        xlist.stopRefreshLoadUI();
    }

    protected void dealFooter(ArrayList<Room> arrayList, int totalNumber, XListView listViewNannies) {
        if (arrayList.size() >= totalNumber) {
            listViewNannies.getFooterView().setVisibility(View.GONE);
        } else {
            listViewNannies.getFooterView().setVisibility(View.VISIBLE);
        }
    }


}
