package com.tongtong.android.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.tongtong.android.R;

import java.util.HashMap;
import java.util.UUID;


/**
 * Created by leocx on 2016/9/30.
 */

public class PopularityView extends View implements View.OnTouchListener {
    private final int MAX_RATE = 150;
    private final float HEART_WIDTH = 0.4f;
    private final float HEART_HEIGHT = HEART_WIDTH;
    private final float NUM_WIDTH = 0.6f;
    private final float NUM_HEIGHT = 0.35f;
    private final float NUM_OFFSET = 0.2f;

    private int width;
    private int height;
    int count = 0;
    float totalWidth;
    float heartLeft;
    float heartRight;
    float heartTop;
    float heartBot;
    float numLeft;
    float numRight;
    float numTop;
    float numBot;

    private Paint textPaint;
    private Paint mPaint;
    private HashMap<UUID, Heart> bubbledHearts = new HashMap<>();


    public PopularityView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setTextSize(40);
        textPaint = new Paint();
        textPaint.setColor(Color.WHITE);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setTextSize(50);

        setOnTouchListener(this);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        width = MeasureSpec.getSize(widthMeasureSpec);
        height = MeasureSpec.getSize(heightMeasureSpec);
        totalWidth = width * (HEART_WIDTH + NUM_WIDTH - NUM_OFFSET);
        heartLeft = (width - totalWidth) / 2;
        heartRight = heartLeft + width * HEART_WIDTH;
        heartTop = height - width * HEART_WIDTH;
        heartBot = height;
        numLeft = heartRight - width * NUM_OFFSET;
        numRight = numLeft + width * NUM_WIDTH;
        numTop = heartTop + (HEART_HEIGHT - NUM_HEIGHT) * width / 2;
        numBot = numTop + NUM_HEIGHT * width;
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(Color.TRANSPARENT);
        float textX = heartRight + (NUM_WIDTH - NUM_OFFSET) * width / 2;
        float textY = (numTop + numBot) / 2 - (textPaint.descent() + textPaint.ascent()) / 2;
        //draw number
        Drawable num = getResources().getDrawable(R.drawable.icon_like_num);
        num.setBounds((int) numLeft, (int) numTop, (int) numRight, (int) numBot);
        num.draw(canvas);
        //draw heart base
        Drawable heart = getResources().getDrawable(R.drawable.icon_like_base);
        heart.setBounds((int) heartLeft, (int) heartTop, (int) heartRight, (int) heartBot);
        heart.draw(canvas);
        canvas.drawText("" + count, textX, textY, textPaint);
        //draw bubbled bubbledHearts
        for (UUID key : bubbledHearts.keySet()) {
            Heart tempHeart = bubbledHearts.get(key);
            Drawable tempHeartDrawable;
            switch (tempHeart.color) {
                case 0:
                    tempHeartDrawable = getResources().getDrawable(R.drawable.icon_like_heart_red);
                    break;
                case 1:
                    tempHeartDrawable = getResources().getDrawable(R.drawable.icon_like_heart_green);
                    break;
                case 2:
                    tempHeartDrawable = getResources().getDrawable(R.drawable.icon_like_heart_yellow);
                    break;
                default:
                    tempHeartDrawable = getResources().getDrawable(R.drawable.icon_like_heart_pink);
                    break;
            }
            int alpha = (int) (255 - 255f * tempHeart.rate / MAX_RATE);
            tempHeartDrawable.setAlpha(alpha);
            int tempOffset = (int) (HEART_WIDTH * width * 0.5);
            int left = tempHeart.x - tempOffset;
            int right = tempHeart.x + tempOffset;
            int top = tempHeart.y - tempOffset;
            int bot = tempHeart.y + tempOffset;
            tempHeartDrawable.setBounds(left, top, right, bot);
            tempHeartDrawable.draw(canvas);
        }
    }


    private class ReflectMovement implements Runnable {
        private Heart heart;

        public ReflectMovement(Heart heart) {
            this.heart = heart;
        }

        public void run() {
            invalidate();
            if (heart.rate < MAX_RATE) {
                heart.rate++;
                if (heart.x <= HEART_WIDTH * width / 2 && heart.direct == -1) {
                    heart.direct = 1;
                    heart.speed = randomSpeed();
                } else if (heart.x >= width - HEART_WIDTH * width / 2 && heart.direct == 1) {
                    heart.direct = -1;
                    heart.speed = randomSpeed();
                }
                heart.x += heart.speed * heart.direct;
                heart.y -= height / MAX_RATE;
                bubbledHearts.put(heart.ssid, heart);
                postDelayed(this, 1);
            } else {
                bubbledHearts.remove(heart.ssid);
            }
        }
    }

    private int randomSpeed() {
        return (int) (width * (Math.random() * 3 + 1) / MAX_RATE);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                if (event.getY() > numTop) {
                    count++;
                    setCountWithAnim(count);
                    this.callOnClick();
                }

        }
        return true;
    }

    public void setCountWithAnim(int count) {
        this.count = count;
        Heart heart = new Heart((int) ((heartLeft + heartRight) / 2), (int) ((heartTop + heartBot) / 2), randomSpeed());
        ReflectMovement heartRunnable = new ReflectMovement(heart);
        postDelayed(heartRunnable, 0);
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
        postInvalidate();
    }
}

class Heart {
    public UUID ssid = UUID.randomUUID();
    public int x = 0;
    public int y = 0;
    public int direct;
    public int speed;
    public int color;
    public int rate = 0;

    public Heart(int x, int y, int speed) {
        int rand = (int) (Math.random() * 2);
        if (rand == 0) {
            //right
            direct = 1;
        } else {
            //left
            direct = -1;
        }
        color = (int) (Math.random() * 4);
        this.x = x;
        this.y = y;
        this.speed = speed;
    }
}