package com.tongtong.android.view;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.StringUtils;
import com.tongtong.android.R;
import com.tongtong.android.net.NetInterface;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by clia on 2016/8/8.
 */
public class ReportDialog extends MyBasePicker implements View.OnClickListener{

    private RadioGroup radioGroupReport;
    private TextView textViewReport;
    private ImageView imageViewBack;
    private EditText editTextOhters;

    private BaseTask baseTask;

    private String reason;
    private String userId;
    private boolean isOhters = false;


    public ReportDialog(Context context, String userId) {
        super(context);

        this.userId = userId;
        reason = context.getResources().getString(R.string.report_violence);
        LayoutInflater.from(context).inflate(R.layout.dialog_report, contentContainer);

        setupViews();
    }


    private void setupViews(){

        radioGroupReport = (RadioGroup) findViewById(R.id.radioGroupReport);
        textViewReport = (TextView) findViewById(R.id.textViewReport);
        imageViewBack = (ImageView) findViewById(R.id.imageViewBack);
        editTextOhters = (EditText) findViewById(R.id.editTextOthers);
        editTextOhters.setBackgroundResource(R.drawable.shape_input_big_grey);
        editTextOhters.setHint("");

        textViewReport.setOnClickListener(this);
        imageViewBack.setOnClickListener(this);

        radioGroupReport.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // TODO Auto-generated method stub
                RadioButton radioButton = (RadioButton)findViewById(checkedId);
                if (!StringUtils.isEmpty(radioButton.getText().toString())) {

                    if(checkedId == R.id.radioButtonOthers){
                        editTextOhters.setEnabled(true);
                        editTextOhters.setBackgroundResource(R.drawable.shape_input_big);
                        editTextOhters.setHint(context.getResources().getString(R.string.report_hint));
                        isOhters = true;
                    }else{
                        reason = radioButton.getText().toString();
                        editTextOhters.setEnabled(false);
                        editTextOhters.setBackgroundResource(R.drawable.shape_input_big_grey);
                        editTextOhters.setHint("");
                        isOhters = false;
                    }
                }else{
                    radioButton.startAnimation(AnimationUtils.loadAnimation(context,R.anim.shake));
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.textViewReport:
                if(isOhters) reason = editTextOhters.getText().toString();

                uploadReport();
                break;
            case R.id.imageViewBack:
                dismiss();
                break;
        }
    }

    private void uploadReport(){
        if (null != baseTask && baseTask.getStatus() == AsyncTask.Status.RUNNING) {
            baseTask.cancel(true);
        }

        baseTask = new BaseTask(context, new NetCallBack() {

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("user_id", userId);
                    jsonObject.put("reason", reason);

                    netResult = NetInterface.uploadreport(baseTask.activity, jsonObject);

                } catch (Exception e) {
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (null != result && result.isOk()) {
                    Toast.makeText(context, "Success", Toast.LENGTH_SHORT);
                }
                dismiss();
            }
        });

        baseTask.execute(new HashMap<String, String>());
    }

}
