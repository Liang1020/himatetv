package com.tongtong.android.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.common.util.StringUtils;
import com.tongtong.android.R;
import com.tongtong.android.domain.LiveType;

import java.util.ArrayList;

/**
 * Created by clia on 2016/8/12.
 */
public class CategoryDialog extends MyBasePicker {

    private Context mContext;
    private ArrayList<LiveType> liveTypes;
    private String category;

    RadioGroup radioGroupCategory;

    private OnClickDialogListener onClickDialogListener;

    public CategoryDialog(Context context) {
        super(context);
        this.mContext = context;

        LayoutInflater.from(context).inflate(R.layout.dialog_category, contentContainer);
        LiveType liveType = new LiveType();
        liveTypes.add(liveType);


        setupViews();
    }

    public CategoryDialog(Context context, ArrayList<LiveType> types) {
        super(context);
        this.mContext = context;
        this.liveTypes = types;

        LayoutInflater.from(context).inflate(R.layout.dialog_category, contentContainer);

        setupViews();

    }

    private void setupViews() {
        radioGroupCategory = (RadioGroup) findViewById(R.id.radioGroupCategory);
        if (liveTypes.size() <= 0) return;

        for (int i = 0; i < liveTypes.size(); i++) {
            RadioButton tempButton = new RadioButton(mContext);
            tempButton.setButtonDrawable(R.drawable.selector_rbt_category);
            tempButton.setBackgroundResource(R.drawable.selector_rb_category);
            tempButton.setPadding(100, 10, 0, 0);
            tempButton.setText(liveTypes.get(i).getType());

            LinearLayout.LayoutParams lpp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) context.getResources().getDimension(R.dimen.input_item_height));

            lpp.topMargin = 10;
            lpp.bottomMargin = 10;

            radioGroupCategory.addView(tempButton, lpp);
        }

        radioGroupCategory.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // TODO Auto-generated method stub
                RadioButton radioButton = (RadioButton) findViewById(checkedId);
                if (!StringUtils.isEmpty(radioButton.getText().toString())) {
                    dismiss();
                    if (null != onClickDialogListener) {
                        onClickDialogListener.OnOkClick(radioButton.getText().toString());
                    }
                } else {
                    radioButton.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                }
            }
        });

        findViewById(R.id.viewCover).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }


    public interface OnClickDialogListener {
        void OnOkClick(String category);

        void OnCancelClick();
    }

    public void setOnClickDialogListener(OnClickDialogListener onClickDialogListener) {
        this.onClickDialogListener = onClickDialogListener;
    }

    public OnClickDialogListener getOnClickDialogListener() {
        return onClickDialogListener;
    }


}
