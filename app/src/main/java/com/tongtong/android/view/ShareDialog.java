package com.tongtong.android.view;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.google.android.exoplayer.C;
import com.sina.weibo.sdk.api.TextObject;
import com.sina.weibo.sdk.api.WebpageObject;
import com.sina.weibo.sdk.api.WeiboMessage;
import com.sina.weibo.sdk.api.WeiboMultiMessage;
import com.sina.weibo.sdk.api.share.IWeiboShareAPI;
import com.sina.weibo.sdk.api.share.SendMessageToWeiboRequest;
import com.sina.weibo.sdk.api.share.SendMultiMessageToWeiboRequest;
import com.sina.weibo.sdk.auth.AuthInfo;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WeiboAuthListener;
import com.sina.weibo.sdk.exception.WeiboException;
import com.sina.weibo.sdk.utils.Utility;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.SendMessageToWX;
import com.tencent.mm.sdk.openapi.WXMediaMessage;
import com.tencent.mm.sdk.openapi.WXWebpageObject;
import com.tencent.mm.sdk.platformtools.Log;
import com.tencent.mm.sdk.platformtools.Util;
import com.tongtong.android.App;
import com.tongtong.android.R;
import com.tongtong.android.activity.LiveActivity;
import com.tongtong.android.domain.Room;
import com.tongtong.android.utils.AccessTokenKeeper;

import java.util.List;
import java.util.Locale;

import static com.tongtong.android.utils.Constants.APP_KEY;
import static com.tongtong.android.utils.Constants.REDIRECT_URL;
import static com.tongtong.android.utils.Constants.SCOPE;


/**
 * Created by wangzy on 16/8/26.
 */
public class ShareDialog extends MyBasePicker {

    //74c641683cdd38bf7ad7f0cba1daaf57 weibo secret
    //2224658031 weibo key

    private OnClickShare2Listener onClickShare2Listener;
    private IWXAPI api;
    private IWeiboShareAPI weiboApi;
    private Context context;
    private Room room;

    public ShareDialog(final Context context, final Room room, IWXAPI xapi, IWeiboShareAPI wbapi, final byte[] thumbData) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.dialog_share, contentContainer);

        this.context = context;
        this.room = room;
        this.api = xapi;
        this.weiboApi = wbapi;

        WXWebpageObject webpage = new WXWebpageObject();

        webpage.webpageUrl = room.getAppUrl();

        final WXMediaMessage msg = new WXMediaMessage(webpage);
        msg.title = room.getSubject();
        msg.description = room.getContents();

//        WeiboMultiMessage weiboMessage = new WeiboMultiMessage();
//        WebpageObject mediaObject = new WebpageObject();
//        mediaObject.actionUrl = room.getAppUrl();
//        mediaObject.title = room.getTitle();
//        mediaObject.description = room.getContents();

//        Bitmap bmp = BitmapFactory.decodeStream(new URL(url).openStream());
//        Bitmap thumbBmp = Bitmap.createScaledBitmap(bmp, THUMB_SIZE, THUMB_SIZE, true);
//        bmp.recycle();
//        msg.thumbData = Util.bmpToByteArray(thumbBmp, true);


        if (null != thumbData) {
            msg.thumbData = thumbData;
//            mediaObject.thumbData = thumbData;
        }

//        weiboMessage.mediaObject = mediaObject;
        msg.mediaObject = webpage;

        final SendMessageToWX.Req req = new SendMessageToWX.Req();

        req.transaction = String.valueOf(System.currentTimeMillis());
        req.message = msg;

//        final SendMultiMessageToWeiboRequest weiboRequest = new SendMultiMessageToWeiboRequest();
//        weiboRequest.transaction = String.valueOf(System.currentTimeMillis());
//        weiboRequest.multiMessage = weiboMessage;

        findViewById(R.id.viewShare2WechatFriend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                req.scene = SendMessageToWX.Req.WXSceneSession;
                if (checkValue(room, context)) {
                    api.sendReq(req);
                }

                if (null != onClickShare2Listener) {
                    onClickShare2Listener.share2Session();
                }

                dismiss();

            }
        });

        findViewById(R.id.viewShare2TimeLine).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                req.scene = SendMessageToWX.Req.WXSceneTimeline;

                if (checkValue(room, context)) {
                    api.sendReq(req);
                }
                if (null != onClickShare2Listener) {
                    onClickShare2Listener.share2TimeLine();
                }

                dismiss();
            }
        });

        findViewById(R.id.viewShare2Weibo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checkValue(room, context)) {
                    sendMultiMessage(room, thumbData);
                }

                if(!isWeiboInstalled(context)){
                    Tool.showMessageDialog("Weibo is not installed", context);
                }

                if (null != onClickShare2Listener) {
                    onClickShare2Listener.share2Weibo();
                }

                dismiss();
            }
        });

        findViewById(R.id.viewSpace).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

    }

    private String getSharedText() {
        int formatId = R.string.weibosdk_demo_share_video_template;
        String format = context.getResources().getString(formatId);
        String text = format;
        String url = room.getAppUrl();

        text = String.format(format, room.getTitle(), url);
        return text;
    }

    private  boolean isWeiboInstalled(@NonNull Context context) {
        PackageManager pm;
        if ((pm = context.getApplicationContext().getPackageManager()) == null) {
            return false;
        }
        List<PackageInfo> packages = pm.getInstalledPackages(0);
        for (PackageInfo info : packages) {
            String name = info.packageName.toLowerCase(Locale.ENGLISH);
            if ("com.sina.weibo".equals(name)) {
                return true;
            }
        }
        return false;
    }

    private boolean checkValue(Room room, Context context) {

        String appUrl = room.getAppUrl();
        if (StringUtils.isEmpty(appUrl)) {
            Toast.makeText(context, "房间地址不存在", Toast.LENGTH_LONG);
            return false;
        } else {
            return true;
        }
    }


    public OnClickShare2Listener getOnClickShare2Listener() {
        return onClickShare2Listener;
    }

    public void setOnClickShare2Listener(OnClickShare2Listener onClickShare2Listener) {
        this.onClickShare2Listener = onClickShare2Listener;
    }

    public static interface OnClickShare2Listener {

        /**
         * 分享朋友圈
         */
        public void share2TimeLine();

        /**
         * 分享给好友
         */
        public void share2Session();

        /**
         * 分享微博
         */
        public void share2Weibo();
    }

    private void sendMultiMessage(Room room,  final byte[] thumbData) {
        WeiboMultiMessage weiboMessage = new WeiboMultiMessage();

        WebpageObject mediaObject = new WebpageObject();
        mediaObject.identify = Utility.generateGUID();
        mediaObject.actionUrl = room.getAppUrl();
        mediaObject.title = room.getTitle();
        mediaObject.description = room.getContents();
        mediaObject.defaultText="Himate";
        mediaObject.thumbData = thumbData;

        weiboMessage.mediaObject = mediaObject;

        SendMultiMessageToWeiboRequest request = new SendMultiMessageToWeiboRequest();
        request.transaction = String.valueOf(System.currentTimeMillis());
        request.multiMessage = weiboMessage;
        Log.e("Activity----------------------------->", ""+context);
        weiboApi.sendRequest((Activity)context, request);
    }
    int THUMB_SIZE = 150;

    private void loadPicture4Share(Room room, final SendMessageToWX.Req req, final WXMediaMessage msg) {
        String imgurl = room.getImgUrl();

        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                if (null != bitmap) {
                    Bitmap thumbBmp = Bitmap.createScaledBitmap(bitmap, THUMB_SIZE, THUMB_SIZE, true);
                    msg.thumbData = Util.bmpToByteArray(thumbBmp, true);
                    api.sendReq(req);
                    thumbBmp.recycle();

                }
//                ((BaseTongTongActivity) context).hideProgressDialog();
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

//                ((BaseTongTongActivity) context).showProgressDialog(false);


            }
        };


//        Bitmap bmp = BitmapFactory.decodeStream(new URL(url).openStream());
//        Bitmap thumbBmp = Bitmap.createScaledBitmap(bmp, THUMB_SIZE, THUMB_SIZE, true);

        if (StringUtils.isEmpty(imgurl)) {
            Picasso.with(context).with(context).load(R.mipmap.ic_launcher).into(target);
        } else {
            Picasso.with(context).with(context).load(imgurl).placeholder(R.mipmap.ic_launcher).into(target);
        }


    }


}
