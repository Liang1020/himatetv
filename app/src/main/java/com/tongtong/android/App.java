package com.tongtong.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Environment;
import android.widget.Toast;

import com.common.BaseApp;
import com.common.net.NetResult;
import com.common.util.SharePersistent;
import com.common.util.Tool;
import com.tongtong.android.activity.SplashActivity;
import com.tongtong.android.domain.LiveType;
import com.tongtong.android.domain.Profile;
import com.tongtong.android.domain.User;

import java.io.File;
import java.util.ArrayList;


/**
 * Created by clia on 2016/5/18.
 */
public class App extends BaseApp {

    public static String TAG = "TONGTONG";
    public static App app;

    public static final int INTENT_NICKNAME = 0x1000;
    public static final int INTENT_CITY = 0x1001;
    public static final int INTENT_TYPE = 0x1002;
    public static final int INTENT_TOPIC = 0x1003;
    public static final int INTENT_DES = 0x1004;

    public static String CACHE_DIR;
    public static String CACHE_DIR_PRODUCTS;
    public static String CACHE_DIR_EXCEL;
    public static String CACHE_DIR_PDF;
    public static final String DirFileName = "cretve_cache";


    public static final String USERNAME_TAG = "lkjhgfdsa";
    public static final String PASSWORD_TAG = "mnbvcxz";

    private User user;
    private Profile profile;
    private ArrayList<LiveType> types;


    @Override
    public void onCreate() {
        super.onCreate();
        app = this;

    }

    public static App getApp() {
        return app;
    }


    public static String returnCacheDir() {
        firstRun();
        return CACHE_DIR;
    }

    public static void firstRun() {
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            CACHE_DIR = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + DirFileName;
        } else {
            CACHE_DIR = Environment.getRootDirectory().getAbsolutePath() + "/" + DirFileName;
        }
        CACHE_DIR_PRODUCTS = CACHE_DIR + "/products";
        File cacheDir = new File(App.CACHE_DIR);
        if (!cacheDir.exists()) {
            cacheDir.mkdirs();
        }
        File cacheDirProducts = new File(App.CACHE_DIR_PRODUCTS);
        if (!cacheDirProducts.exists()) {
            cacheDirProducts.mkdirs();
        }
    }

    @Override
    public User getUser() {

        try {
            User user = (User) SharePersistent.getObjectValue(this, "user");
            return user;
        } catch (Exception e) {
        }

        return null;
//        return user;
    }

    public void setUser(User user) {
//        this.user = user;

        SharePersistent.setObjectValue(this, "user", user);
    }


    public Profile getProfile() {
        return (Profile) SharePersistent.getObjectValue(this, "profile");
//        return profile;
    }

    public void setProfile(Profile profile) {
//        this.profile = profile;

        SharePersistent.setObjectValue(this, "profile", profile);
    }

    public ArrayList<LiveType> getLiveTypes() {
        return types;
    }

    public void setLiveTypes(ArrayList<LiveType> types) {
        this.types = types;
    }


    public void showError(NetResult netResult, Activity activity) {
        if (NetResult.CODE_NOT_ANCHOR.equals(netResult.getCode())) {
            savePassword(activity, null, null);
            activity.startActivity(new Intent(activity, SplashActivity.class));
            Toast.makeText(activity, "非主播账号，请重新登陆", Toast.LENGTH_LONG).show();
            activity.finish();
        } else {
            Tool.showMessageDialog(netResult.getMessage(), activity);
        }
    }

    public void savePassword(Activity activity, String username, String password){

        SharePersistent.savePreferenceEn(activity, USERNAME_TAG, username);
        SharePersistent.savePreferenceEn(activity, PASSWORD_TAG, password);
    }
}

