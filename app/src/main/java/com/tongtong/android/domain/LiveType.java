package com.tongtong.android.domain;

/**
 * Created by wangzy on 16/8/9.
 */
public class LiveType {

    private String id;
    private String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static String getTypeTextById(String id){

        final int tid=Integer.parseInt(id);
        switch (tid){

            case 1:
                return "户外直播";
            case 2:
                return "美食直播";
            case 3:
                return "手工匠人";
            case 4:
                return "设计创意";
            case 5:
                return "时尚潮流";
            case 6:
                return "留学移民";
        }

        return "";
    }



//
//    {
//        "status_code": "200",
//            "data": [
//        {
//            "id": 1,
//                "title": "户外直播"
//        },
//        {
//            "id": 2,
//                "title": "美食直播"
//        },
//        {
//            "id": 3,
//                "title": "手工匠人"
//        },
//        {
//            "id": 4,
//                "title": "设计创意"
//        },
//        {
//            "id": 5,
//                "title": "时尚潮流"
//        },
//        {
//            "id": 6,
//                "title": "留学移民"
//        }
//        ]
//    }


}
