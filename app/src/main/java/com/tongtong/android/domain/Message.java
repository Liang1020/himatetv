package com.tongtong.android.domain;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by wangzy on 16/8/14.
 */
public class Message {


    private boolean isSend=false;

    private String time;
    private String message;
    private String userid;
    private String nickname;
    private String senderImg;
    private String avatar;

    public Message() {

    }

    public boolean isSend() {
        return isSend;
    }

    public void setSend(boolean send) {
        isSend = send;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSenderImg() {
        return senderImg;
    }

    public void setSenderImg(String senderImg) {
        this.senderImg = senderImg;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public static Message jsonObject2Message(String json) {
//        {"time":"2016-08-16 08:01:00","message":"\u8bf4\u70b9\u5565","command":"message","userid":1,"nickname":"baozi"}
//        {"time":"2016-08-17 02:16:09","message":"\u8bf4\u70b9\u5565","command":"message","userid":12,"nickname":"sand","avatar":"http:\/\/106.185.55.185:8768\/uploads\/12.png?1471251478"}

        Message message = new Message();

        try {

            JSONObject jsonObjectMessage=new JSONObject(json);

            JSONObject userObjectMessage =  jsonObjectMessage.getJSONObject("user");
            JSONObject profileObjectMessage =  userObjectMessage.getJSONObject("profile");

            message.setTime(getStringFroJso("time", jsonObjectMessage));
            message.setMessage(getStringFroJso("message", jsonObjectMessage));
            message.setUserid(getStringFroJso("id", userObjectMessage));
            message.setNickname(getStringFroJso("nickname", userObjectMessage));
            message.setAvatar(getStringFroJso("avatar", profileObjectMessage));

        } catch (Exception e) {

        }


        return message;

    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    private static String getStringFroJso(String key, JSONObject jobj) throws JSONException {
        if (jobj.has(key)) {

            String result = jobj.getString(key);

            if ("null".equals(result)) {
                return "";
            } else {
                return result;
            }

        } else {
            return "";
        }
    }

    @Override
    public String toString() {
        return "Message{" +
                "isSend=" + isSend +
                ", time='" + time + '\'' +
                ", message='" + message + '\'' +
                ", userid='" + userid + '\'' +
                ", nickname='" + nickname + '\'' +
                ", senderImg='" + senderImg + '\'' +
                ", avatar='" + avatar + '\'' +
                '}';
    }
}
