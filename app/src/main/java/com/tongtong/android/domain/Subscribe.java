package com.tongtong.android.domain;

import com.common.util.LogUtil;
import com.tongtong.android.json.JsonHelper;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by wangzy on 16/8/18.
 */
public class Subscribe {


    private User user;

    private String userId;

    private String channel;

    private int count;
    private int like;


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;

        user = new User();
        user.setUserId(this.userId);
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public static Subscribe json2SubScrible(String json) {
//        {"command":"subscribe","channel":"1","userid":"1"}

        try {
            JSONObject jsonObject = new JSONObject(json);

            Subscribe subscribe = new Subscribe();

            subscribe.setCount(jsonObject.getInt("count"));

            User user = JsonHelper.parseUser(jsonObject.getJSONObject("user"));

//            subscribe.setLike(jsonObject.getJSONObject("user").getJSONObject("room").getInt("like"));
            subscribe.setUserId(JsonHelper.getStringFroJso("userid", jsonObject));
            subscribe.setChannel(JsonHelper.getStringFroJso("channel", jsonObject));
            subscribe.setUser(user);

            LogUtil.v("leo", "count: " + jsonObject.getInt("count"));
            return subscribe;

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;

    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }
}
