package com.tongtong.android.domain;

import com.common.bean.IBeanInterface;
import com.common.util.StringUtils;

/**
 * Created by wangzy on 16/8/8.
 */
public class Room implements IBeanInterface{

    private String id;
    private String name;
    private String userId;
    private String title;
    private String imgUrl;
    private String scores;
    private String url;
    private String contents;
    private String tags;
    private String wowzaLink;
    private String wowzaAndroid;
    private String isLive;
    private String disable;
    private String typeId;
    private String totalPeople;
    private String startLiveTime;
    private String endLiveTime;
    private String subject;
    private String appUrl;


    private User user;

    public String getWowzaAndroid() {
        return wowzaAndroid;
    }

    public void setWowzaAndroid(String wowzaAndroid) {
        this.wowzaAndroid = wowzaAndroid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getScores() {
        return scores;
    }

    public void setScores(String scores) {
        this.scores = scores;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getWowzaLink() {
        return wowzaLink;
    }

    public void setWowzaLink(String wowzaLink) {
        this.wowzaLink = wowzaLink;
    }

    public String getIsLive() {
        return isLive;
    }

    public void setIsLive(String isLive) {
        this.isLive = isLive;
    }

    public String getDisable() {
        return disable;
    }

    public void setDisable(String disable) {
        this.disable = disable;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTotalPeople() {
        return totalPeople;
    }

    public void setTotalPeople(String totalPeople) {
        this.totalPeople = totalPeople;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getStartLiveTime() {
        return startLiveTime;
    }

    public void setStartLiveTime(String startLiveTime) {
        this.startLiveTime = startLiveTime;
    }

    public String getSubject() {
        if(!StringUtils.isEmpty(subject)){
            return  subject;
        }
        return title;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getAppUrl() {
        return appUrl;
    }

    public void setAppUrl(String appUrl) {
        this.appUrl = appUrl;
    }

    public String getEndLiveTime() {
        return endLiveTime;
    }

    public void setEndLiveTime(String endLiveTime) {
        this.endLiveTime = endLiveTime;
    }

    @Override
    public String toString() {
        return "Room{" +
                "title='" + title + '\'' +
                '}';
    }
}
