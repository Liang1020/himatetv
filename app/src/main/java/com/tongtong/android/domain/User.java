package com.tongtong.android.domain;

import com.domain.BaseUser;

import java.io.Serializable;

/**
 * Created by wangzy on 16/8/8.
 */
public class User extends BaseUser {


    private String role;

    private String disable;

    private String apply;

    private String city;


    private String black;

    private Room room;


    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDisable() {
        return disable;
    }

    public void setDisable(String disable) {
        this.disable = disable;
    }

    public String getApply() {
        return apply;
    }

    public void setApply(String apply) {
        this.apply = apply;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getUserId() {
        return getId();
    }

    public void setUserId(String userId) {

        this.setId(userId);
    }

    public String isBlack() {
        return black;
    }

    public void setBlack(String black) {
        this.black = black;
    }

    public boolean isUser() {
        if ("user".equalsIgnoreCase(role)) {
            return true;
        }
        return false;
    }


    public String getBlack() {
        return black;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
}
