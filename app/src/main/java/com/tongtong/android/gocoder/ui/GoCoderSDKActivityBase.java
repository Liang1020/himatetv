/**
 *  This is sample code provided by Wowza Media Systems, LLC.  All sample code is intended to be a reference for the
 *  purpose of educating developers, and is not intended to be used in any production environment.
 *
 *  IN NO EVENT SHALL WOWZA MEDIA SYSTEMS, LLC BE LIABLE TO YOU OR ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 *  OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 *  EVEN IF WOWZA MEDIA SYSTEMS, LLC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  WOWZA MEDIA SYSTEMS, LLC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. ALL CODE PROVIDED HEREUNDER IS PROVIDED "AS IS".
 *  WOWZA MEDIA SYSTEMS, LLC HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *  Copyright © 2015 Wowza Media Systems, LLC. All rights reserved.
 */

package com.tongtong.android.gocoder.ui;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.view.WindowManager;

import com.tongtong.android.gocoder.config.ConfigPrefs;
import com.wowza.gocoder.sdk.api.WZPlatformInfo;
import com.wowza.gocoder.sdk.api.WZVersionInfo;
import com.wowza.gocoder.sdk.api.WowzaGoCoder;
import com.wowza.gocoder.sdk.api.broadcast.WZBroadcast;
import com.wowza.gocoder.sdk.api.broadcast.WZBroadcastConfig;
import com.wowza.gocoder.sdk.api.configuration.WZMediaConfig;
import com.wowza.gocoder.sdk.api.configuration.WowzaConfig;
import com.wowza.gocoder.sdk.api.devices.WZCameraView;
import com.wowza.gocoder.sdk.api.errors.WZStreamingError;
import com.wowza.gocoder.sdk.api.logging.WZLog;
import com.wowza.gocoder.sdk.api.status.WZStatus;
import com.wowza.gocoder.sdk.api.status.WZStatusCallback;

import java.util.Arrays;

public abstract class GoCoderSDKActivityBase extends Activity
    implements WZStatusCallback {

    private final static String TAG = GoCoderSDKActivityBase.class.getSimpleName();

    //private static final String SDK_SAMPLE_APP_LICENSE_KEY = "GSDK-CA41-0001-E32F-0CF1-93EC";
    private static final String SDK_SAMPLE_APP_LICENSE_KEY = "GSDK-AC42-0003-9CFE-32D0-873D";
    private static final int PERMISSIONS_REQUEST_CODE = 0x1;

    protected String[] mRequiredPermissions = {};

    private static Object sBroadcastLock = new Object();
    private static boolean sBroadcastEnded = true;

    // GoCoder SDK top level interface
    protected static WowzaGoCoder sGoCoder = null;

    // indicates whether this is a full screen activity or note
    protected static boolean sFullScreenActivity = true;

    /**
     * Build an array of WZMediaConfigs from the frame sizes supported by the active camera
     * @param goCoderCameraView the camera view
     * @return an array of WZMediaConfigs from the frame sizes supported by the active camera
     */
    protected static WZMediaConfig[] getVideoConfigs(WZCameraView goCoderCameraView) {
        WZMediaConfig configs[] = WowzaConfig.PRESET_CONFIGS;

        if (goCoderCameraView != null && goCoderCameraView.getCamera() != null) {
            WZMediaConfig cameraConfigs[] = goCoderCameraView.getCamera().getSupportedConfigs();
            Arrays.sort(cameraConfigs);
            configs = cameraConfigs;
        }

        return configs;
    }

    protected WZBroadcast mBroadcast = null;
    public WZBroadcast getBroadcast() {
        return mBroadcast;
    }

    public void setBroadcast(WZBroadcast broadcast) {
        mBroadcast = broadcast;
    }

    protected WZBroadcastConfig mBroadcastConfig = null;
    public WZBroadcastConfig getBroadcastConfig() {
        return mBroadcastConfig;
    }

    public void setBroadcastConfig(WZBroadcastConfig broadcastConfig) {
        mBroadcastConfig = broadcastConfig;
    }

    protected boolean mPermissionsGranted = false;
    public boolean getPermissionsGranted() {
        return mPermissionsGranted;
    }

    public void setPermissionsGranted(boolean permissionsGranted) {
        mPermissionsGranted = permissionsGranted;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize the GoCoder SDK
        if (sGoCoder == null) {
            // Enable detailed logging from the GoCoder SDK
            WZLog.LOGGING_ENABLED = true;
            sGoCoder = WowzaGoCoder.init(this, SDK_SAMPLE_APP_LICENSE_KEY);
            if (sGoCoder == null) {
                WZLog.error(TAG, WowzaGoCoder.getLastError());
            } else {
                WZLog.info(TAG, "==== GoCoder SDK Version ===\n"
                        + WZVersionInfo.getInstance().toVerboseString()
                            + "\n============================");
                WZLog.info(TAG, "======= Device Information =======\n"
                        + WowzaGoCoder.PLATFORM_INFO
                            + "\n==================================");
                WZLog.info(TAG, "======= Display Information =======\n"
                        + WZPlatformInfo.displayInfo(this)
                            + "\n===================================");
                WZLog.info(TAG, "=================== OpenGL ES Information ===================\n"
                        + WowzaGoCoder.OPENGLES_INFO
                            + "\n=============================================================");
            }
        }

        if (sGoCoder != null) {
            mBroadcast = new WZBroadcast();
            mBroadcastConfig = new WZBroadcastConfig(sGoCoder.getConfig());
            mBroadcastConfig.setLogLevel(WZLog.LOG_LEVEL_DEBUG);
        }
    }

    /**
     * Android Activity lifecycle methods
     */
    @Override
    protected void onResume() {
        super.onResume();

        if (mBroadcast != null) {
            mPermissionsGranted = (mRequiredPermissions.length > 0 ? WowzaGoCoder.hasPermissions(this, mRequiredPermissions) : true);
            if (mPermissionsGranted) {
                ConfigPrefs.updateConfigFromPrefs(PreferenceManager.getDefaultSharedPreferences(this), mBroadcastConfig);
            } else {
                ActivityCompat.requestPermissions(this,
                        mRequiredPermissions, PERMISSIONS_REQUEST_CODE);
            }
        }
    }

    @Override
    protected void onPause() {
        // Stop any active live stream
        if (mBroadcast != null && mBroadcast.getBroadcastStatus().isRunning()) {
            endBroadcast(true);
        }

        super.onPause();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        mPermissionsGranted = true;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CODE: {
                for(int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        mPermissionsGranted = false;
                    }
                }
            }
        }
    }

    /**
     * Enable Android's sticky immersive full-screen mode
     * See http://developer.android.com/training/system-ui/immersive.html#sticky
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (sFullScreenActivity && hasFocus) {
            View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
            if (rootView != null)
                rootView.setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);}
    }

    /**
     * WZStatusCallback interface methods
     */
    @Override
    public void onWZStatus(final WZStatus goCoderStatus) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (goCoderStatus.isReady())
                    // Keep the screen on while the broadcast is active
                   getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                else if (goCoderStatus.isIdle())
                    // Clear the "keep screen on" flag
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

                WZLog.debug(TAG, goCoderStatus.toString());
            }
        });
    }

    @Override
    public void onWZError(final WZStatus goCoderStatus) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                WZLog.error(TAG, goCoderStatus.getLastError());
            }
        });
    }

    protected synchronized WZStreamingError startBroadcast() {
        WZStreamingError configValidationError = null;

        if (mBroadcast.getBroadcastStatus().isIdle()) {
            WZLog.info(TAG, "=============== Broadcast Configuration ===============\n"
                    + mBroadcastConfig.toString()
                        + "\n=======================================================");

            configValidationError = mBroadcastConfig.validateForBroadcast();
            if (configValidationError == null) {
                mBroadcast.startBroadcast(mBroadcastConfig, this);
            }
        } else {
            WZLog.error(TAG, "startBroadcast() called while another broadcast is active");
        }
        return configValidationError;
    }

    protected synchronized void endBroadcast(boolean appPausing) {
        if (!mBroadcast.getBroadcastStatus().isIdle()) {
            if (appPausing) {
                // Stop any active live stream
                sBroadcastEnded = false;
                mBroadcast.endBroadcast(new WZStatusCallback() {
                    @Override
                    public void onWZStatus(WZStatus wzStatus) {
                        synchronized (sBroadcastLock) {
                            sBroadcastEnded = true;
                            sBroadcastLock.notifyAll();
                        }
                    }

                    @Override
                    public void onWZError(WZStatus wzStatus) {
                        WZLog.error(TAG, wzStatus.getLastError());
                        synchronized (sBroadcastLock) {
                            sBroadcastEnded = true;
                            sBroadcastLock.notifyAll();
                        }
                    }
                });

                while(!sBroadcastEnded) {
                    try{
                        sBroadcastLock.wait();
                    } catch (InterruptedException e) {}
                }
            } else {
                mBroadcast.endBroadcast(this);
            }
        }  else {
            WZLog.error(TAG, "endBroadcast() called without an active broadcast");
        }
    }

    protected synchronized void endBroadcast() {
        endBroadcast(false);
    }
}
