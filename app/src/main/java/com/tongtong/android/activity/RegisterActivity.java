package com.tongtong.android.activity;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.widget.EditText;
import android.widget.Toast;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.common.util.ValidateTool;
import com.tongtong.android.App;
import com.tongtong.android.R;
import com.tongtong.android.domain.Profile;
import com.tongtong.android.domain.User;
import com.tongtong.android.net.NetInterface;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class RegisterActivity extends BaseTongTongActivity {


    @InjectView(R.id.editTextEmail)
    EditText editTextEmail;

    @InjectView(R.id.editTextPassword)
    EditText editTextPassword;

    @InjectView(R.id.editTextConfirmPwd)
    EditText editTextConfirmPwd;

    @InjectView(R.id.editTextNickname)
    EditText editTextNickname;

    @InjectView(R.id.textInputLayoutUserEmail)
    TextInputLayout textInputLayoutUserEmail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.inject(this);
        App.getApp().addActivity(this);

    }

    @OnClick(R.id.buttonRegister)
    public void register() {

        if (checkValue()) {

            reSetTask();

            baseTask = new BaseTask(this, new NetCallBack() {

                @Override
                public void onPreCall(BaseTask baseTask) {

                    baseTask.showDialogForSelf(true);
                }

                @Override
                public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {


                    NetResult netResult = null;


                    try {

                        String email = getInput(editTextEmail);
                        String pwd = getInput(editTextPassword);
                        String nickName = getInput(editTextNickname);


                        JSONObject jsonObject = new JSONObject();

                        jsonObject.put("nickname", nickName);
                        jsonObject.put("email", email);
                        jsonObject.put("password", pwd);

                        netResult = NetInterface.register(RegisterActivity.this, jsonObject);
                    } catch (Exception e) {

                    }

                    return netResult;

                }

                @Override
                public void onFinish(NetResult result, BaseTask baseTask) {
                    baseTask.hideDialogSelf();
                    if (null != result) {
                        if (result.isOk()) {

                            User user = (User) result.getData()[0];
                            App.getApp().setUser((User) result.getData()[0]);

                            App.getApp().saveApiToken((String) result.getData()[1]);

                            Profile profile = new Profile();
                            profile.setEmail(user.getEmail());
                            profile.setNickname(user.getNickName());

                            App.getApp().setProfile(profile);

                            Tool.startActivity(RegisterActivity.this, MainActivity.class);

                        } else {
                            Tool.showMessageDialog(result.getMessage(), RegisterActivity.this);
                        }
                    } else {
                        Tool.showMessageDialog(R.string.error_net, RegisterActivity.this);
                    }

                }
            });

            baseTask.execute(new HashMap<String, String>());

        }

    }


    private boolean checkValue() {

        String email = getInput(editTextEmail);

        if (!ValidateTool.checkEmail(email)) {
            editTextEmail.setError("Email错误。");
            return false;
        }
        String nickName = getInput(editTextNickname);
        if (StringUtils.isEmpty(nickName)) {
            editTextNickname.setError("请输入昵称。");
            return false;
        }


        String password = getInput(editTextPassword);
        if (StringUtils.computStrlen(password) < 6) {
            editTextPassword.setError("请至少输入6位密码。");
            return false;
        }

        String confirmPwd = getInput(editTextConfirmPwd);
        if (!password.equals(confirmPwd)) {
            editTextConfirmPwd.setError("确认密码错误。");
            return false;
        }


        return true;
    }


    @OnClick(R.id.buttonBack)
    public void onSignUpClick() {
        Tool.startActivity(this, MainActivity.class);
        finish();
    }


    public void register(final String email, final String password, final String nickName) {
        reSetTask();

        App.getApp().clearTokan();

        baseTask = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {

                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {

                    JSONObject jo = new JSONObject();

                    jo.put("nickname", nickName);
                    jo.put("email", email);
                    jo.put("password", password);

                    netResult = NetInterface.register(RegisterActivity.this, jo);

                } catch (Exception e) {
                    LogUtil.e(App.TAG, "register error:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result) {
                    if (result.isOk()) {

                        Tool.startActivity(RegisterActivity.this, MainActivity.class);

                    } else {
                        Tool.showMessageDialog(R.string.error_net, RegisterActivity.this);
                    }


                } else {
                    Tool.showMessageDialog(R.string.error_net, RegisterActivity.this);
                }


            }
        });

        baseTask.execute(new HashMap<String, String>());

    }

    private long mPressedTime = 0;

    @Override
    public void onBackPressed() {
        LogUtil.i(App.TAG, "backpressed:....");
        long mNowTime = System.currentTimeMillis();
        if ((mNowTime - mPressedTime) > 2000) {
            Toast.makeText(this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
            mPressedTime = mNowTime;
        } else {
            App.getApp().exit();
        }

    }
}
