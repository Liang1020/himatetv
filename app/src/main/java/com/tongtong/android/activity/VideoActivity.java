package com.tongtong.android.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.Constant;
import com.common.util.DialogCallBackListener;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.SharePersistent;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.sina.weibo.sdk.api.share.IWeiboShareAPI;
import com.sina.weibo.sdk.api.share.WeiboShareSDK;
import com.squareup.picasso.Picasso;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.mm.sdk.platformtools.Util;
import com.tongtong.android.App;
import com.tongtong.android.R;
import com.tongtong.android.adapter.ChannelPageAdapter;
import com.tongtong.android.adapter.MessageAdapter;
import com.tongtong.android.domain.LiveType;
import com.tongtong.android.domain.Message;
import com.tongtong.android.domain.Room;
import com.tongtong.android.domain.Subscribe;
import com.tongtong.android.domain.User;
import com.tongtong.android.net.WSRequester;
import com.tongtong.android.utils.ScreenUtil;
import com.tongtong.android.view.CircleImageView;
import com.tongtong.android.view.PopularityView;
import com.tongtong.android.view.ShareDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import tv.danmaku.ijk.media.player.IMediaPlayer;
import tv.danmaku.ijk.media.player.IjkMediaPlayer;
import tv.danmaku.ijk.media.widget.media.AndroidMediaController;
import tv.danmaku.ijk.media.widget.media.IjkVideoView;


public class VideoActivity extends BaseTongTongActivity {
    private static final String TAG = "LiveActivity";

    @InjectView(R.id.relativeLayoutMedia)
    RelativeLayout relativeLayoutMedia;

    @InjectView(R.id.relativeLayoutBottom)
    RelativeLayout relativeLayoutBottom;

    @InjectView(R.id.relativeLayoutTopMediaController)
    RelativeLayout relativeLayoutTopMediaController;

    @InjectView(R.id.relativeLayoutBottomMediaController)
    RelativeLayout relativeLayoutBottomMediaController;

    @InjectView(R.id.linearLayoutChatBox)
    LinearLayout linearLayoutChatBox;

    @InjectView(R.id.linearLayoutBack)
    LinearLayout linearLayoutBack;

    @InjectView(R.id.linearLayoutChatList)
    LinearLayout linearLayoutChatList;

    @InjectView(R.id.imageViewUpDown)
    ImageView imageViewUpDown;

    @InjectView(R.id.textViewUpDown)
    TextView textViewUpDown;

    @InjectView(R.id.linearLayoutNumbers)
    LinearLayout linearLayoutNumbers;

    @InjectView(R.id.textViewUserCount)
    TextView textViewUserCountTop;

    @InjectView(R.id.imageViewFullScreen)
    ImageView imageViewFullScreen;

    @InjectView(R.id.textViewFirst)
    TextView textViewFirst;

    @InjectView(R.id.textViewSecond)
    TextView textViewSecond;

    @InjectView(R.id.textViewThird)
    TextView textViewThird;


    @InjectView(R.id.tabLayout)
    TabLayout tabLayout;

    @InjectView(R.id.vpChannel)
    ViewPager viewPagerChannel;

    @InjectView(R.id.textViewTitle)
    TextView textViewTitle;

    @InjectView(R.id.imageViewShare)
    ImageView imageViewShare;

    @InjectView(R.id.textViewUserCount)
    TextView textViewUserCount;

    @InjectView(R.id.progressBarBuffer)
    ProgressBar progressBarBuffer;

    @InjectView(R.id.pv_like)
    PopularityView pv_like;

    @OnClick(R.id.pv_like)
    public void like() {
        boolean result = wsRequester.like();
    }


    IjkVideoView videoView;

    private ArrayList<View> viewList;
    private ArrayList<String> titleList;

    private View viewChatView, viewAnchor;
    private ListView listViewChat;

    private Room room;

    private EditText editTextMessage;
    private WSRequester wsRequester;
    private TextView textViewSend;

    private ArrayList<Message> arrayListMessage;
    private MessageAdapter messageAdapter;

    private AndroidMediaController androidMediaController;

    private int count = 0;

    private IWXAPI api;
    private IWeiboShareAPI weiboApi;

    private static final int TIMEOUT = 5000;

    private static final int MSG_SURFACE_START = 0x00000001;
    private static final int MSG_SURFACE_TOUCH = 0x00000003;

    private static final int MSG_SCREEN_FULL = 0x00000004;
    private static final int MSG_SCREEN_WRAP = 0x00000005;

    private static final int MSG_UPDATE_PROGRESS = 0x00000006;
    private static final int MSG_MEDIA_CONTROLLER_HIDE = 0x00000007;
    private static final int MSG_FADE_OUT = 0x00000007;
    private static final int MSG_REFRESH = 0x00000008;

    private static final int MSG_STOP = 0x00000009;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_SURFACE_START:
                    progressBarBuffer.setVisibility(View.GONE);
                    videoView.start();
                    break;

                case MSG_SURFACE_TOUCH:
                    if (relativeLayoutTopMediaController.getVisibility() == View.VISIBLE) {
                        mediaControllerHide();
                    } else {
                        mediaControllerShow();
                    }
                    break;

                case MSG_SCREEN_FULL:
                    screenFullModeUI();
                    break;

                case MSG_SCREEN_WRAP:
                    screenWrapModeUI();
                    break;

                case MSG_UPDATE_PROGRESS:
                    break;

                case MSG_MEDIA_CONTROLLER_HIDE:
                    mediaControllerHide();
                    break;

                case MSG_REFRESH:
                    textViewUserCount.setText(msg.arg1 + getResources().getString(R.string.live_user_counts));
                    textViewUserCountTop.setText(msg.arg1 + getResources().getString(R.string.live_user_counts));
                    break;

                case MSG_STOP:
                    if(videoView.canPause()){
                        LogUtil.i("CAN PAUSE", "---------------->");
                        videoView.pause();
                        videoView.suspend();
                    }

                    finish();
                    break;

                default:
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LogUtil.i("VideoOnCreate", "--------------->");
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
//        if (getRequestedOrientation() != ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//        }
        setContentView(R.layout.activity_video);
        App.getApp().addActivity(this);
        ButterKnife.inject(this);

        arrayListMessage = new ArrayList<>();
        messageAdapter = new MessageAdapter(this, arrayListMessage);


        room = (Room) App.getApp().getTempObject("room");

        if (null != room) {
            SharePersistent.setObjectValue(this, "room", room);
        }

        IjkMediaPlayer.loadLibrariesOnce(null);
        IjkMediaPlayer.native_profileBegin("libijkplayer.so");
        videoView = (IjkVideoView) findViewById(R.id.ijkPlayer);
        setListener();


        //  videoView.setBackgroundColor(Color.parseColor("#F6F6F6"));
        //  videoView.setOnErrorListener(new ErrorListener());
        androidMediaController = new AndroidMediaController(this, false);
        videoView.setMediaController(androidMediaController);
//        String url = "http://devimages.apple.com.edgekey.net/streaming/examples/bipbop_4x3/gear3/prog_index.m3u8";
        if (room != null) {
            String mVideoPath = room.getWowzaAndroid();
            videoView.setVideoURI(Uri.parse(mVideoPath));
            videoView.start();
        } else {
            Toast.makeText(VideoActivity.this, "房间不存在！", Toast.LENGTH_SHORT);
        }

        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            screenFullModeUI();
        } else if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            screenWrapModeUI();
        }

        initBottomLayout();
        //=======================
        initWsrequester();

        api = WXAPIFactory.createWXAPI(this, getResources().getString(R.string.app_id));
        weiboApi = WeiboShareSDK.createWeiboAPI(this, getResources().getString(R.string.weibo_api));
        weiboApi.registerApp();

    }

    private void setListener() {
        videoView.setOnPreparedListener(new IMediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(IMediaPlayer mp) {
                mHandler.sendEmptyMessage(MSG_SURFACE_START);
            }
        });

        videoView.setOnErrorListener(new IMediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(IMediaPlayer mp, int what, int extra) {
                LogUtil.e(TAG, "MediaPalyer:" + what + ", " + extra);
                if (what < 0) {
                    Tool.showMessageDialog("该直播可能被关闭,或者网络问题,请尝试重新进入房间。", VideoActivity.this, new DialogCallBackListener() {
                        @Override
                        public void onDone(boolean yesOrNo) {
                            finish();
//                            videoView.start();
                        }
                    });
                }
                videoView.pause();
                progressBarBuffer.setVisibility(View.GONE);
                return true;
            }
        });

        videoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        mHandler.sendEmptyMessage(MSG_SURFACE_TOUCH);
                        break;
                    case MotionEvent.ACTION_UP:
                        android.os.Message msg = mHandler.obtainMessage(MSG_FADE_OUT);
                        mHandler.sendMessageDelayed(msg, TIMEOUT);
                        break;
                    default:
                        break;
                }
                return false;
            }
        });
    }

    @OnClick(R.id.linearLayoutChatBg)
    public void onChatBgClick() {
        if (textViewUpDown.getText().toString().equals("收起")) {
            textViewUpDown.setText("展开");
            imageViewUpDown.setImageResource(R.drawable.icon_down_arrow);
            linearLayoutChatList.setVisibility(View.INVISIBLE);
        } else {
            textViewUpDown.setText("收起");
            imageViewUpDown.setImageResource(R.drawable.icon_up_arrow);
            linearLayoutChatList.setVisibility(View.VISIBLE);
        }
    }

    BaseTask loadImgtask;

    @OnClick(R.id.imageViewShare)
    public void onShareClick() {

        if (null != loadImgtask && loadImgtask.getStatus() == AsyncTask.Status.RUNNING) {
            loadImgtask.cancel(true);
        }

        loadImgtask = new BaseTask(VideoActivity.this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = new NetResult();
                Bitmap bitmap = null;
                String roomImgUrl = room.getImgUrl();
                int THUMB_SIZE = 100;

                if (!StringUtils.isEmpty(roomImgUrl)) {
                    try {
                        Bitmap bmp = BitmapFactory.decodeStream(new URL(roomImgUrl).openStream());
                        bitmap = Bitmap.createScaledBitmap(bmp, THUMB_SIZE, THUMB_SIZE, true);
                        bmp.recycle();
                    } catch (Exception e) {
                    }
                } else {
                    Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.icon_weshre);
                    bitmap = Bitmap.createScaledBitmap(bmp, THUMB_SIZE, THUMB_SIZE, true);
                    bmp.recycle();
                }

                if (null != bitmap) {
                    byte[] data = Util.bmpToByteArray(bitmap, true);
                    netResult.setData(new Object[]{data});
                    return netResult;
                } else {
                    return null;
                }

            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result) {
                    BaseTongTongActivity.showShareDialog(VideoActivity.this, room, new ShareDialog.OnClickShare2Listener() {
                        @Override
                        public void share2TimeLine() {

                        }

                        @Override
                        public void share2Session() {

                        }

                        @Override
                        public void share2Weibo() {

                        }
                    }, api, weiboApi, (byte[]) result.getData()[0]);
                } else {
                    BaseTongTongActivity.showShareDialog(VideoActivity.this, room, new ShareDialog.OnClickShare2Listener() {
                        @Override
                        public void share2TimeLine() {

                        }

                        @Override
                        public void share2Session() {

                        }

                        @Override
                        public void share2Weibo() {

                        }
                    }, api, weiboApi, null);
                }

            }

        });

        loadImgtask.execute(new HashMap<String, String>());

    }


    @OnClick(R.id.imageViewFullScreen)
    public void onTogleScrean() {
        if (Tool.getScreenOrientation(this) == Configuration.ORIENTATION_LANDSCAPE) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else if (Tool.getScreenOrientation(this) == Configuration.ORIENTATION_PORTRAIT) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    @OnClick(R.id.imageButtonBack)
    public void onBackClick() {
        LogUtil.i(App.TAG, "OnBackPressed------------->");

        android.os.Message msg = mHandler.obtainMessage(MSG_STOP);
        mHandler.sendMessage(msg);

    }

    @Override
    protected void onPause() {
        super.onPause();
        videoView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (null == room) {
            try {
                room = (Room) SharePersistent.getObjectValue(this, "room");
            } catch (Exception e) {
            }
        }

        videoView.resume();

        refreshEnableSendMessage();

    }

    private void mediaControllerHide() {
        relativeLayoutTopMediaController.setVisibility(View.GONE);
        androidMediaController.hide();
        relativeLayoutBottomMediaController.setVisibility(View.GONE);
    }

    private void mediaControllerShow() {
        relativeLayoutTopMediaController.setVisibility(View.VISIBLE);
        androidMediaController.hide();
        relativeLayoutBottomMediaController.setVisibility(View.VISIBLE);
//        imageViewPlayPause.setImageResource(mVideoView.isPlaying() ? R.drawable.selector_pause : R.drawable.selector_play);

    }

    private void refreshEnableSendMessage() {
        if (null == App.getApp().getUser()) {
            textViewSend.setEnabled(false);
            editTextMessage.setEnabled(false);
            textViewSend.setTextColor(Color.parseColor("#c8c8c8"));
        } else {
            textViewSend.setEnabled(true);
            editTextMessage.setEnabled(true);
            textViewSend.setTextColor(Color.parseColor("#007AFF"));
        }
    }


    private void disableMsg() {
        textViewSend.setEnabled(false);
        editTextMessage.setEnabled(false);
        textViewSend.setTextColor(Color.parseColor("#c8c8c8"));
        editTextMessage.setText("");
    }

    private void enableMsg() {
        textViewSend.setEnabled(true);
        editTextMessage.setEnabled(true);
        textViewSend.setTextColor(Color.parseColor("#007AFF"));
        editTextMessage.setText("");
    }


    /**
     * 禁言
     */
    private void denay() {

        textViewSend.setEnabled(false);
        editTextMessage.setEnabled(false);
        textViewSend.setTextColor(Color.parseColor("#c8c8c8"));
        editTextMessage.setText("");
        editTextMessage.setHint("被禁言");

    }

    private void undenay() {
        textViewSend.setEnabled(true);
        editTextMessage.setEnabled(true);
        textViewSend.setTextColor(Color.parseColor("#007AFF"));
        editTextMessage.setText("");
        editTextMessage.setHint("说两句");
    }


    public void screenFullModeUI() {
        relativeLayoutBottom.setVisibility(View.GONE);
        textViewTitle.setText(room.getTitle());
        imageViewFullScreen.setImageResource(R.drawable.icon_zoom_shrink);
        linearLayoutChatBox.setVisibility(View.VISIBLE);
        setScreenLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    public void screenWrapModeUI() {
        relativeLayoutBottom.setVisibility(View.VISIBLE);
        textViewTitle.setText(room.getTitle());
        imageViewFullScreen.setImageResource(R.drawable.icon_live_zoom_full_btn);
        linearLayoutChatBox.setVisibility(View.GONE);
        setScreenLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ScreenUtil.dip2px(this, 500));
        videoView.requestFocus();
    }

    private void setScreenLayoutParams(int width, int height) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width, height);
        relativeLayoutMedia.setLayoutParams(layoutParams);
        videoView.requestFocus();

    }

    private int getScreenOrientation() {
        return getResources().getConfiguration().orientation;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LogUtil.i("VideoOnConfigurationChanged", "--------------->");
        int value = getScreenOrientation();
        room = (Room) SharePersistent.getObjectValue(this, "room");
        if (value == Configuration.ORIENTATION_LANDSCAPE) {
            LogUtil.i("TAG", "---->Full Screen");
//            room = (Room) App.getApp().getTempObject("room");
//            screenFullModeUI();
            mHandler.sendEmptyMessage(MSG_SCREEN_FULL);
        } else if (value == Configuration.ORIENTATION_PORTRAIT) {
            mHandler.sendEmptyMessage(MSG_SCREEN_WRAP);
//            room = (Room) App.getApp().getTempObject("room");
//            screenWrapModeUI();
            LogUtil.i("TAG", "---->Wrap Screen");
        }
    }

    private void initBottomLayout() {

        viewChatView = LayoutInflater.from(this).inflate(R.layout.page_chat_view, null);
        viewAnchor = LayoutInflater.from(this).inflate(R.layout.page_blackboard, null);
        listViewChat = (ListView) viewChatView.findViewById(R.id.listView);
        listViewChat.setAdapter(messageAdapter);


        editTextMessage = (EditText) viewChatView.findViewById(R.id.editTextMessage);
        textViewSend = (TextView) viewChatView.findViewById(R.id.textViewSend);


        textViewSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LogUtil.i(App.TAG, "send msg:" + editTextMessage.getText().toString());

                String msgContent = editTextMessage.getText().toString();
                msgContent = StringUtils.isEmpty(msgContent) ? "" : msgContent;

                if (null != wsRequester && !StringUtils.isEmpty(msgContent)) {

                    boolean result = wsRequester.message(App.getApp().getUser(), editTextMessage.getText().toString());
                    if (result) {


//                        {"time":"2016-08-16 08:01:00","message":"\u8bf4\u70b9\u5565","command":"message","userid":1,"nickname":"baozi"}
                        User user = App.getApp().getUser();

                        Message message = new Message();

                        message.setMessage(editTextMessage.getText().toString());
                        message.setTime(Constant.SDFD4.format(new Date()));
                        message.setUserid(user.getUserId());
                        message.setNickname(user.getNickName());
                        message.setAvatar(user.getAvatar());

                        arrayListMessage.add(message);
                        messageAdapter.notifyDataSetChanged();
                        Tool.setListViewHeightBasedOnChildren(listViewChat);
                        editTextMessage.setText("");

                        listViewChat.post(new Runnable() {
                            @Override
                            public void run() {

                                listViewChat.setSelection(messageAdapter.getCount() - 1);
                            }
                        });

                    }
                } else {
                    LogUtil.e(App.TAG, "ws lost connect or empty msg");
                }

            }
        });

        //直播公告
        LinearLayout linearLayoutAnchor = (LinearLayout) viewAnchor.findViewById(R.id.linearLayoutAnchorInfo);
        ((TextView) viewAnchor.findViewById(R.id.textViewBroadcastTitle)).setText(room.getSubject());
        ((TextView) viewAnchor.findViewById(R.id.textViewRoomAddress)).setText("房间地址:" + room.getAppUrl());
        try {
            Date date = Constant.SDFD4.parse(room.getStartLiveTime());
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd HH:ss");
            ((TextView) viewAnchor.findViewById(R.id.textViewTime)).setText("时间:" + sdf.format(date));
        } catch (Exception e) {
        }


        ((TextView) viewAnchor.findViewById(R.id.textViewComments)).setText(room.getContents());


        ArrayList<LiveType> lives = App.getApp().getLiveTypes();

        if (!ListUtiles.isEmpty(lives)) {

            String ltid = room.getTypeId();
            for (LiveType lt : lives) {
                if (ltid.equals(lt.getId())) {
                    ((TextView) viewAnchor.findViewById(R.id.textViewLiveType)).setText("类型:" + lt.getType());
                    break;
                }
            }
        } else {
            ((TextView) viewAnchor.findViewById(R.id.textViewLiveType)).setText("类型:" + LiveType.getTypeTextById(room.getTypeId()));
        }


        String ancherAvatar = room.getUser().getAvatar();

        CircleImageView circleImageView = (CircleImageView) viewAnchor.findViewById(R.id.roundImageViewProfile);

        if (!StringUtils.isEmpty(ancherAvatar)) {
            Picasso.with(App.getApp()).load(ancherAvatar).resize(50, 50).placeholder(R.drawable.bg_default_header).into(circleImageView);
        } else {
            Picasso.with(App.getApp()).load(R.drawable.bg_default_header).resize(50, 50).into(circleImageView);
        }

        linearLayoutAnchor.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent();

                intent.putExtra("userId", room.getUser().getId());
                intent.putExtra("imgUrl", room.getUser().getAvatar());
                Tool.startActivity(VideoActivity.this, AnchorInfoActivity.class, intent);
            }
        });

        viewList = new ArrayList<>();
        viewList.add(viewChatView);
        viewList.add(viewAnchor);

        titleList = new ArrayList<>();
        titleList.add(getResources().getString(R.string.watch_chat));
        titleList.add(getResources().getString(R.string.watch_notification_board));

        ChannelPageAdapter pagerAdapter = new ChannelPageAdapter(viewList, titleList);

        viewPagerChannel.setAdapter(pagerAdapter);

        tabLayout.setupWithViewPager(viewPagerChannel);


        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int pos = tab.getPosition();
                LogUtil.i(App.TAG, "select pos:" + pos);
                if (pos == 1) {
                    refreshOnLineList();
                }
                viewPagerChannel.setCurrentItem(pos);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                int pos = tab.getPosition();
                LogUtil.i(App.TAG, "select pos:" + pos);
                if (pos == 1) {
                    refreshOnLineList();
                }


            }
        });


        refreshEnableSendMessage();


    }


    private void refreshOnLineList() {
        if (null != wsRequester) {
            wsRequester.roomusers();
            LogUtil.i(App.TAG, "刷新房间人数");
        }
    }


    private boolean initWsrequester() {
        try {
            wsRequester = WSRequester.getInstance(new WSRequester.ConnectionListener() {

                @Override
                public void onOpen() {
                    LogUtil.e(App.TAG, "=========onOpen ws=========");
                    if (null == App.getApp().getUser()) {
                        disableMsg();
                    } else {
                        enableMsg();
                    }

                    String roomId = null;
                    roomId = room.getId();

                    User user = App.getApp().getUser();
                    wsRequester.enter(user, roomId);
                    wsRequester.roomusers();

                }

                @Override
                public void onLike(JSONObject message) {
                    try {
                        pv_like.setCountWithAnim(message.getInt("like"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSubScrible(Subscribe subscribe) {
                    if(subscribe == null){
                        return;
                    }
                    if (null != App.getApp().getUser()) {
                        if (App.getApp().getUser().getUserId().equals(subscribe.getUser().getUserId())
                                && !"0".equals(subscribe.getUser().getBlack())) {
                            denay();
                        }
                    }
                    pv_like.setCount(subscribe.getLike());

                    Log.i("Subscribe Count", subscribe.getCount()+"");
                    android.os.Message msg = mHandler.obtainMessage(MSG_REFRESH);
                    msg.arg1 = subscribe.getCount();
                    mHandler.sendMessage(msg);
//                    textViewUserCount.setText(subscribe.getCount() + getResources().getString(R.string.live_user_counts));
//                    textViewUserCountTop.setText(subscribe.getCount() + getResources().getString(R.string.live_user_counts));

                }

                @Override
                public void onUnSubScrible(Subscribe subscribe){
                    if(subscribe == null){
                        return;
                    }
                    if (null != App.getApp().getUser()) {
                        if (App.getApp().getUser().getUserId().equals(subscribe.getUser().getUserId())
                                && !"0".equals(subscribe.getUser().getBlack())) {
                            denay();
                        }
                    }
                    pv_like.setCount(subscribe.getLike());

                    Log.i("UnSubscribe Count", subscribe.getCount()+"");
                    android.os.Message msg = mHandler.obtainMessage(MSG_REFRESH);
                    msg.arg1 = subscribe.getCount();
                    mHandler.sendMessage(msg);
                }
                @Override
                public void onRoomUsers(ArrayList<User> users) {
//
                }

                @Override
                public void onDeny() {
                    denay();
                }

                @Override
                public void onUnDeny() {
                    undenay();
                }

                @Override
                public void onReceiverMessage(Message message) {
                    messageAdapter.addMsg(message);
                    listViewChat.post(new Runnable() {
                        @Override
                        public void run() {
                            listViewChat.setSelection(messageAdapter.getCount() - 1);
                        }
                    });
                    Tool.setListViewHeightBasedOnChildren(listViewChat);

                    count++;

                    if (count < 3) {
                        textViewFirst.setText(textViewSecond.getText());
                        textViewSecond.setText(textViewThird.getText());
                        textViewThird.setText(Html.fromHtml("<font color='blue'>" + message.getNickname() + "</font>:" + message.getMessage()));
                    } else {
                        textViewFirst.setText(textViewSecond.getText());
                        textViewSecond.setText(textViewThird.getText());
                        textViewThird.setText(Html.fromHtml("<font color='blue'>" + message.getNickname() + "</font>:" + message.getMessage()));
                        count = 0;
                    }

                }

                @Override
                public void onLiveStop() {
                    Tool.showMessageDialog("主播停止了直播", VideoActivity.this, new DialogCallBackListener() {
                        @Override
                        public void onDone(boolean yesOrNo) {


                            finish();


                        }
                    });
                }

                @Override
                public void onClose(int code, String reason) {

                    LogUtil.e(App.TAG, "=========close ws=========");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            disableMsg();
                        }
                    });

                }
            });

            return true;
        } catch (Exception e) {

            LogUtil.e(App.TAG, "init ws fail:" + e.getLocalizedMessage());
        }

        return false;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != wsRequester) {
            wsRequester.disconencted();
        }
    }
}
