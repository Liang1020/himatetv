package com.tongtong.android.activity;

import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.common.util.LogUtil;
import com.common.util.Tool;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tongtong.android.App;
import com.tongtong.android.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by clia on 2016/5/18.
 */
public class SplashActivity extends BaseTongTongActivity {


    private IWXAPI api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        App.getApp().addActivity(this);

        regToWx();

        Timer timer = new Timer();

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                SharedPreferences sharedPreferences = getSharedPreferences(App.TAG, MODE_PRIVATE);
                try {
                    String currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
                    String lastVersion = sharedPreferences.getString("lastVersion", "None");
                    LogUtil.v("leo", "current: " + currentVersion + "   last: " + lastVersion);
                    if (!lastVersion.equals(currentVersion)) {
                        Tool.startActivity(SplashActivity.this, TutorialActivity.class);
                    } else {
                        Tool.startActivity(SplashActivity.this, LoginActivity.class);
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    Tool.startActivity(SplashActivity.this, TutorialActivity.class);
                    e.printStackTrace();
                }
                finish();
            }
        }, 500);

    }

    private void regToWx() {

        api = WXAPIFactory.createWXAPI(this, getResources().getString(R.string.app_id));
        api.registerApp(getResources().getString(R.string.app_id));

    }

}
