
package com.tongtong.android.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.callback.OnUploadCallBack;
import com.common.net.FormFile;
import com.common.net.NetException;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.BitmapTool;
import com.common.util.Constant;
import com.common.util.LogUtil;
import com.common.util.SharePersistent;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.squareup.picasso.Picasso;
import com.tongtong.android.App;
import com.tongtong.android.R;
import com.tongtong.android.adapter.MessageAdapter;
import com.tongtong.android.adapter.OnlineAdapter;
import com.tongtong.android.domain.Message;
import com.tongtong.android.domain.User;
import com.tongtong.android.gocoder.config.ConfigPrefs;
import com.tongtong.android.gocoder.config.ConfigPrefsActivity;
import com.tongtong.android.gocoder.ui.AutoFocusListener;
import com.tongtong.android.gocoder.ui.GoCoderSDKActivityBase;
import com.tongtong.android.net.NetInterface;
import com.tongtong.android.net.WSRequester;
import com.tongtong.android.view.CircleImageView;
import com.tongtong.android.view.MyLiveDialog;
import com.wowza.gocoder.sdk.api.broadcast.WZBroadcastConfig;
import com.wowza.gocoder.sdk.api.configuration.WZMediaConfig;
import com.wowza.gocoder.sdk.api.devices.WZCamera;
import com.wowza.gocoder.sdk.api.devices.WZCameraView;
import com.wowza.gocoder.sdk.api.encoder.WZEncoderAPI;
import com.wowza.gocoder.sdk.api.errors.WZStreamingError;
import com.wowza.gocoder.sdk.api.h264.WZProfileLevel;
import com.wowza.gocoder.sdk.api.logging.WZLog;
import com.wowza.gocoder.sdk.api.status.WZState;
import com.wowza.gocoder.sdk.api.status.WZStatus;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


/**
 * old live activity,now it's not use anywhere
 * may be delete this calss in the future
 */


@Deprecated
public class MyLiveMainActivity extends GoCoderSDKActivityBase {

    private final static String TAG = MyLiveMainActivity.class.getSimpleName();

    protected int SCALE_MODES[] = {
            WZBroadcastConfig.FILL_VIEW,
            WZBroadcastConfig.RESIZE_TO_ASPECT
    };
    protected int mScaleModeIndex = 0;


    RelativeLayout myliveMainTopPart;

    RelativeLayout myliveMainBottomPart;

    RelativeLayout myliveMainDashboardPort;

    RelativeLayout myliveMainDashboardLand;


    protected WZCameraView mGoCoderCameraView = null;

    protected GestureDetectorCompat mAutoFocusDetector = null;

    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private LayoutInflater mInflater;
    private List<String> mTitleList = new ArrayList<>();
    private View chatTabView, onlineUsertabView, liveBroadCastView;
    private List<View> mViewList = new ArrayList<>();


    private MyLiveDialog dialogLiveWait;

    private ListView listViewChat;
    private ListView listViewOnLine;
    private ListView listViewLiveNotify;


    private EditText editTextMessage;
    private WSRequester wsRequester;
    private TextView textViewSend;

    private String keyBroad = "broad";
    private String keybbraod = "kbroad";
    private ArrayList<Message> arrayListMesssage;
    private MessageAdapter messageAdapter;


    private OnlineAdapter onlineAdapter;
    private ArrayList<User> arrayListOnlineUsers;


    @InjectView(R.id.imageButtonClose)
    ImageButton imageButtonClose;


    private JSONObject jsonObjectRoomInfo;
    private JSONObject jsonObjectTextInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);//removing title bar
        setContentView(R.layout.activity_mylive_main_page);
        ButterKnife.inject(this);
        super.onCreate(savedInstanceState);


        jsonObjectRoomInfo = (JSONObject) App.getApp().getTempObject("info");
        jsonObjectTextInfo = (JSONObject) App.getApp().getTempObject("binfo");


        arrayListMesssage = new ArrayList<>();
        messageAdapter = new MessageAdapter(this, arrayListMesssage);


        arrayListOnlineUsers = new ArrayList<>();
        onlineAdapter = new OnlineAdapter(this, arrayListOnlineUsers);


        if (null != jsonObjectTextInfo && null != jsonObjectTextInfo) {

            String broadCastObjcetString = jsonObjectRoomInfo.toString();
            String kbroadCastObjcetString = jsonObjectTextInfo.toString();

            SharePersistent.savePreferenceEn(this, keyBroad, broadCastObjcetString);
            SharePersistent.savePreferenceEn(this, keybbraod, kbroadCastObjcetString);

            LogUtil.i(App.TAG, "save success info");

        }

        mRequiredPermissions = new String[]{
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO
        };


        myliveMainTopPart = (RelativeLayout) findViewById(R.id.mylive_main_top_part);
        myliveMainBottomPart = (RelativeLayout) findViewById(R.id.mylive_main_bottom_part);
        myliveMainDashboardPort = (RelativeLayout) findViewById(R.id.mylive_main_dashboard_port);
        myliveMainDashboardLand = (RelativeLayout) findViewById(R.id.mylive_main_dashboard_land);

        myliveMainBottomPart.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View view, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {

                Point p = Tool.getDisplayMetrics(MyLiveMainActivity.this);

                int sh = Tool.getStatusBarHeight(MyLiveMainActivity.this);
                int delta = (oldBottom - p.y);

//                if (delta / sh == 2) {
//                    myliveMainBottomPart.setPadding(0, 0, 0, delta);
//                } else {
//                    myliveMainBottomPart.setPadding(0, 0, 0, 0);
//                }

                //LogUtil.e(App.TAG, "layout chagne:" + view.toString());
                //LogUtil.e(App.TAG, "layout chagne:" + "bottom:" + bottom + " old:" + oldBottom + "  p.y" + p.y + " delta:" + delta + "sh:" + sh);

            }
        });


        initCameraView();

        initBelowContent();

        initControlsOfLanscape();


        try {

            initBottomPartOfPortrait();

        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.e(App.TAG, "error exception:" + e.getLocalizedMessage());
        }


        if (Tool.getScreenOrientation(this) == Configuration.ORIENTATION_PORTRAIT) {
            screenPortraitModeUI();
        } else if (Tool.getScreenOrientation(this) == Configuration.ORIENTATION_LANDSCAPE) {
            screenLandscapeModeUI();
        }

        dialogLiveWait = new MyLiveDialog(this);

        showWaitDialog();

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideWaitDialog();
                    }
                });
            }
        }, 3 * 1000);

        startBroadcast();
        LogUtil.e(App.TAG, "调用sdk 开始直播");

    }

    private void showWaitDialog() {
        if (null != dialogLiveWait) {
            dialogLiveWait.show();
        }

    }

    private void hideWaitDialog() {
        if (null != dialogLiveWait && dialogLiveWait.isShowing()) {
            dialogLiveWait.dismiss();
        }
    }

    @OnClick(R.id.imageButtonFullScreenInPort)
    public void togleScrenOnPort() {
        toogleScreen();
    }

    @OnClick(R.id.buttonToogleScrean)
    public void onToogleScreen() {
        toogleScreen();
    }


    private void toogleScreen() {
        if (Tool.getScreenOrientation(this) == Configuration.ORIENTATION_LANDSCAPE) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else if (Tool.getScreenOrientation(this) == Configuration.ORIENTATION_PORTRAIT) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }


    public void screenPortraitModeUI() {
        myliveMainDashboardLand.setVisibility(View.GONE);
        myliveMainDashboardPort.setVisibility(View.VISIBLE);
        myliveMainBottomPart.setVisibility(View.VISIBLE);


        //readjust size of top part as width:height = 16:9
        //abtain device width
        //base 1280*800

        Point p = Tool.getDisplayMetrics(this);
        int width = p.x;
        int height = (int) (width * (720.0f / 1280));
        resetMyLiveTopPartLayoutParams(width, height);

    }

    public void screenLandscapeModeUI() {
        myliveMainDashboardPort.setVisibility(View.GONE);
        myliveMainBottomPart.setVisibility(View.GONE);
        resetMyLiveTopPartLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        myliveMainDashboardLand.setVisibility(View.VISIBLE);


    }

    private void resetMyLiveTopPartLayoutParams(int width, int height) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, height);
        myliveMainTopPart.setLayoutParams(layoutParams);
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        int value = Tool.getScreenOrientation(this);
        if (value == Configuration.ORIENTATION_LANDSCAPE) {
            screenLandscapeModeUI();
        } else if (value == Configuration.ORIENTATION_PORTRAIT) {
            screenPortraitModeUI();
        }
    }


    private void initCameraView() {
        if (sGoCoder != null) {
            mGoCoderCameraView = (WZCameraView) findViewById(R.id.cameraPreview);
            sGoCoder.setCameraView(mGoCoderCameraView);

            try {


                JSONObject dataObject = jsonObjectRoomInfo.getJSONObject("data");


                String username = dataObject.getString("username");
                String streamName = username;
                String password = dataObject.getString("password");

                LogUtil.e(App.TAG, "woaza:" + username + ":" + password);


                WZBroadcastConfig config = sGoCoder.getDefaultBroadcastConfig();

                String hostAddress = "52.64.199.245";
                int portNumber = 1935;
                String applicationName = "tongtongworkshop";


                config.setHostAddress(hostAddress);
                config.setPortNumber(portNumber);
                config.setApplicationName(applicationName);
                config.setStreamName(streamName);
                config.setUsername(username);
                config.setPassword(password);

//                config.setVideoFrameSize(1280,720);

                getBroadcastConfig().set(config);

                getBroadcastConfig().setVideoBroadcaster(mGoCoderCameraView.getBroadcaster());

                mAutoFocusDetector = new GestureDetectorCompat(this, new AutoFocusListener(this, mGoCoderCameraView));

            } catch (Exception e) {
                LogUtil.e(App.TAG, "erros broadcast:" + e.getLocalizedMessage());

            }

        } else {
//            mStatusView.setErrorMessage(WowzaGoCoder.getLastError().getErrorDescription());
        }
    }

    private void initBottomPartOfPortrait() throws Exception {
        mViewPager = (ViewPager) findViewById(R.id.my_view_pager);
        mTabLayout = (TabLayout) findViewById(R.id.my_tabs);


        String hostAddress = "52.63.94.182";
        int portNumber = 1935;
        String applicationName = "himate";

        JSONObject dataObject = jsonObjectRoomInfo.getJSONObject("data");

        String username = dataObject.getString("username");
        String streamName = username;
        String password = dataObject.getString("password");

        LogUtil.e(App.TAG, "broad cast user:" + username + " streamName:" + streamName + " pwd:" + password);


        WZBroadcastConfig config = sGoCoder.getDefaultBroadcastConfig();

        config.setHostAddress(hostAddress);
        config.setPortNumber(portNumber);
        config.setApplicationName(applicationName);
        config.setStreamName(streamName);
        config.setUsername(username);
        config.setPassword(password);


        sGoCoder.setConfig(config);

        getBroadcastConfig().set(config);

        getBroadcastConfig().setVideoBroadcaster(mGoCoderCameraView.getBroadcaster());

        mAutoFocusDetector = new GestureDetectorCompat(this, new AutoFocusListener(this, mGoCoderCameraView));


        mTitleList.add("聊天");
        mTitleList.add("在线");
        mTitleList.add("直播公告");

        mTabLayout.setTabMode(TabLayout.MODE_FIXED);

        mTabLayout.addTab(mTabLayout.newTab().setText(mTitleList.get(0)));
        mTabLayout.addTab(mTabLayout.newTab().setText(mTitleList.get(1)));
        mTabLayout.addTab(mTabLayout.newTab().setText(mTitleList.get(2)));

        MyPagerAdapter pagerAdapter = new MyPagerAdapter(mViewList);

        mViewPager.setAdapter(pagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int pos = tab.getPosition();
                LogUtil.i(App.TAG, "select pos:" + pos);

                if (pos == 1) {
                    refreshRooms();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                int pos = tab.getPosition();
                LogUtil.i(App.TAG, "select pos:" + pos);

                if (pos == 1) {
                    refreshRooms();
                }

            }
        });

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {


    }

    private void initBelowContent() {

        //==================message adater=========================================
        mInflater = LayoutInflater.from(this);
        chatTabView = mInflater.inflate(R.layout.page_chat_view, null);
        listViewChat = (ListView) chatTabView.findViewById(R.id.listView);


        onlineUsertabView = mInflater.inflate(R.layout.page_online, null);
        listViewOnLine = (ListView) onlineUsertabView.findViewById(R.id.listView);
        liveBroadCastView = mInflater.inflate(R.layout.page_blackboard, null);


        editTextMessage = (EditText) chatTabView.findViewById(R.id.editTextMessage);
        textViewSend = (TextView) chatTabView.findViewById(R.id.textViewSend);
        textViewSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LogUtil.i(App.TAG, "send msg:" + editTextMessage.getText().toString());

                String msgContent = editTextMessage.getText().toString();
                msgContent = StringUtils.isEmpty(msgContent) ? "" : msgContent;


                if (null != wsRequester && !StringUtils.isEmpty(msgContent)) {
                    boolean result = wsRequester.message(App.getApp().getUser(), editTextMessage.getText().toString());
                    if (result) {
//                        {"time":"2016-08-16 08:01:00","message":"\u8bf4\u70b9\u5565","command":"message","userid":1,"nickname":"baozi"}

                        editTextMessage.setText("");

                        User user = App.getApp().getUser();

                        Message message = new Message();

                        message.setMessage(editTextMessage.getText().toString());
                        message.setTime(Constant.SDFD4.format(new Date()));
                        message.setUserid(user.getUserId());
                        message.setNickname(user.getNickName());

                        arrayListMesssage.add(message);
                        messageAdapter.notifyDataSetChanged();


                        Tool.setListViewHeightBasedOnChildren(listViewChat);
                    }
                } else {
                    LogUtil.e(App.TAG, "ws lost connect or empty msg");
                }

            }
        });


        //===========================================
        CircleImageView roundImageViewProfile = (CircleImageView) liveBroadCastView.findViewById(R.id.roundImageViewProfile);

        User currentUser = App.getApp().getUser();

        if (null != currentUser && !StringUtils.isEmpty(currentUser.getAvatar())) {
            Picasso.with(App.getApp()).load(currentUser.getAvatar()).resize(150, 150).config(Bitmap.Config.RGB_565).into(roundImageViewProfile);
        }


        TextView textViewBroadcastTitle = (TextView) liveBroadCastView.findViewById(R.id.textViewBroadcastTitle);
        TextView textViewComments = (TextView) liveBroadCastView.findViewById(R.id.textViewComments);

        try {
            textViewBroadcastTitle.setText(jsonObjectTextInfo.getString("title"));
            textViewComments.setText(jsonObjectTextInfo.getString("comments"));
        } catch (Exception e) {
            LogUtil.e(App.TAG, "error:" + e.getLocalizedMessage());
        }

        //==========================================
        listViewChat.setAdapter(messageAdapter);
        Tool.setListViewHeightBasedOnChildren(listViewChat);

        //=====================user adapter===================================================

        listViewOnLine.setAdapter(onlineAdapter);

        Tool.setListViewHeightBasedOnChildren(listViewOnLine);

        //===================================

        mViewList.add(chatTabView);
        mViewList.add(onlineUsertabView);
        mViewList.add(liveBroadCastView);

    }

    private void initControlsOfLanscape() {


    }


    @Override
    protected void onResume() {
        super.onResume();

        if (null == jsonObjectTextInfo || null == jsonObjectRoomInfo) {

            try {
                jsonObjectTextInfo = new JSONObject(SharePersistent.getPreferenceDe(this, keyBroad));
                jsonObjectRoomInfo = new JSONObject(SharePersistent.getPreferenceDe(this, keybbraod));

                LogUtil.i(App.TAG, "load broadcast into succes");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        if (mGoCoderCameraView != null && mPermissionsGranted) {
            // Log information about the cameras discovered on the local device
            WZLog.info(TAG, "=================== Camera Information ===================\n"
                    + WZCamera.getCameraInfo()
                    + "\n==========================================================");

            // Let's check if we were able to access all of the cameras on this device
            ArrayList<String> badCameras = new ArrayList<>();
            WZCamera[] allCameras = WZCamera.getDeviceCameras();
            for (int c = 0; c < allCameras.length; c++) {
                if (!allCameras[c].isAvailable()) {
                    badCameras.add(allCameras[c].getDirection() == WZCamera.DIRECTION_FRONT ? "front" : "back");
                }
            }
            if (badCameras.size() > 0) {
                TextUtils.join(" and ", badCameras);
                LogUtil.e(App.TAG, "There was an error attempting to initialize the " + TextUtils.join(" and ", badCameras) + " camera" + (badCameras.size() > 1 ? "s" : ""));

            }

            // Initialize the camera view
            int numberOfCameras = mGoCoderCameraView.getCameras().length;
            if (numberOfCameras > 1) {
                int activeCameraId = ConfigPrefs.getActiveCamera(PreferenceManager.getDefaultSharedPreferences(this));
                WZCamera activeCamera = mGoCoderCameraView.getCameraById(activeCameraId);
                if (activeCamera != null && activeCamera.isAvailable())
                    mGoCoderCameraView.setCamera(activeCameraId);
            } else if (numberOfCameras < 1) {
//                mStatusView.setErrorMessage("Could not detect or gain access to any cameras on this device");
                getBroadcastConfig().setVideoEnabled(false);

            } else if (numberOfCameras < 1)
                getBroadcastConfig().setVideoEnabled(false);

            mGoCoderCameraView.setCameraConfig(getBroadcastConfig());
            //mGoCoderCameraView.setScaleMode(SCALE_MODES[mScaleModeIndex]);
            mGoCoderCameraView.setScaleMode(WZBroadcastConfig.RESIZE_TO_ASPECT);

            // If video is enabled in the current broadcast configuration, turn on the camera preview
            if (getBroadcastConfig().isVideoEnabled()) {

                LogUtil.d(App.TAG, "====>ScaleMode of camera is " + mGoCoderCameraView.getScaleMode());
                mGoCoderCameraView.setScaleMode(WZMediaConfig.FILL_VIEW);
                LogUtil.d(App.TAG, "<====ScaleMode of camera is " + mGoCoderCameraView.getScaleMode());
                mGoCoderCameraView.startPreview();
                LogUtil.e(App.TAG, "camera info:" + getBroadcastConfig().getLabel(true, true, false, true));

                WZCamera activeCamera = mGoCoderCameraView.getCamera();
                if (activeCamera != null && activeCamera.hasCapability(WZCamera.FOCUS_MODE_CONTINUOUS)) {
                    activeCamera.setFocusMode(WZCamera.FOCUS_MODE_CONTINUOUS);
                }
            }

            updateUIControls();
            setScaleButtonLabel();

        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mGoCoderCameraView != null)
            mGoCoderCameraView.stopPreview();
    }

    private Handler handler = new Handler() {

        @Override
        public void handleMessage(android.os.Message msg) {

            int what = msg.what;
            switch (what) {

                case WZState.RUNNING: {
                    try {

                        requestStartEndLive(jsonObjectTextInfo.getString("title"), jsonObjectTextInfo.getString("comments"), "1");
                        LogUtil.i(App.TAG, "准备开始直播,调用startendlive api");

                    } catch (Exception e) {
                        LogUtil.e(App.TAG, "request start live error:" + e.getLocalizedMessage());
                    }


                }
                break;
                case WZState.STOPPING: {
                    try {
                        requestStartEndLive(jsonObjectTextInfo.getString("title"), jsonObjectTextInfo.getString("comments"), "0");
                    } catch (Exception e) {
                        LogUtil.e(App.TAG, "request end live error:" + e.getLocalizedMessage());
                    }

                }
                break;

            }


        }
    };

    @Override
    public void onWZStatus(final WZStatus goCoderStatus) {

        final StringBuffer statusMessage = new StringBuffer("Broadcast status: ");

        switch (goCoderStatus.getState()) {
            case WZState.STARTING:
                statusMessage.append("Broadcast initialization");
                break;

            case WZState.READY:
                statusMessage.append("Ready to begin streaming");
                break;

            case WZState.RUNNING:
                statusMessage.append("Streaming is active");
                handler.sendEmptyMessage(WZState.RUNNING);
                break;

            case WZState.STOPPING:
                statusMessage.append("Broadcast shutting down");
                handler.sendEmptyMessage(WZState.STOPPING);
                break;
            case WZState.IDLE:
                statusMessage.append("The broadcast is stopped");
                break;

            default:
                return;
        }

        LogUtil.e(App.TAG, statusMessage.toString());

    }


    BaseTask baseTask;

    private void requestStartEndLive(final String title, final String contents, final String isactive) {
        if (null != baseTask && baseTask.getStatus() == AsyncTask.Status.RUNNING) {
            baseTask.cancel(true);
        }

        baseTask = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;

                try {

                    JSONObject jsonObject = new JSONObject();

                    jsonObject.put("title", title);
                    jsonObject.put("contents", contents);
                    jsonObject.put("active", isactive);
//                    jsonObject.put("trailerid", trailerId);


                    netResult = NetInterface.startEndLive(MyLiveMainActivity.this, jsonObject);

                } catch (Exception e) {
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                LogUtil.v("leo", "*************************************");

                if (null != result) {
                    if (result.isOk()) {
                        LogUtil.i(App.TAG, "request start end live success ");
                        if ("0".equals(isactive)) {
                            if (null != wsRequester) {
                                wsRequester.disconencted();
                            }
                            finish();
                        } else if ("1".equals(isactive)) {
                            boolean issuccess = initWsrequester();
                            if (issuccess) {
                                LogUtil.i(App.TAG, "init send msg ws success");
                            } else {
                                LogUtil.e(App.TAG, "init send msg ws fail");
                            }
                        }
                    } else {
                        Tool.showMessageDialog(result.getMessage(), MyLiveMainActivity.this);
                        LogUtil.i(App.TAG, "request start end live success erroe ");
                    }
                } else {
                    Tool.showMessageDialog(R.string.error_net, MyLiveMainActivity.this);
                }

            }
        });
        baseTask.execute(new HashMap<String, String>());

    }


    private boolean initWsrequester() {
        try {
            wsRequester = WSRequester.getInstance(new WSRequester.ConnectionListener() {

                @Override
                public void onOpen() {

                    textViewSend.setEnabled(true);
                    String roomId = null;
                    try {
                        roomId = jsonObjectRoomInfo.getJSONObject("data").getString("room_id");
                        wsRequester.enter(App.getApp().getUser(), roomId);
                        LogUtil.i(App.TAG, "enter room id:" + roomId);

                        wsRequester.roomusers();


                    } catch (JSONException e) {
                        LogUtil.e(App.TAG, "enter room error");
                    }

                }

                @Override
                public void onReceiveTextMessage(String rmessage) {
                    LogUtil.i(App.TAG, "receive msg:" + rmessage);

                    try {


                        JSONObject jsonObject = new JSONObject(rmessage);
                        if ("message".equals(jsonObject.getString("command"))) {
                            messageAdapter.addMsg(Message.jsonObject2Message(rmessage));
                        } else if ("roomusers".equals(jsonObject.getString("command"))) {

                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onClose(int code, String reason) {
                    textViewSend.setEnabled(false);
                }
            });

            return true;
        } catch (Exception e) {
            LogUtil.e(App.TAG, "init ws fail:" + e.getLocalizedMessage());
        }

        return false;

    }


    private void refreshRooms() {

        if (null != wsRequester) {
            wsRequester.roomusers();
            LogUtil.i(App.TAG, "refresh rooms");
        }

    }


    @Override
    public void onWZError(final WZStatus goCoderStatus) {

        LogUtil.e(App.TAG, "直播错误:" + goCoderStatus.getState());
        LogUtil.e(App.TAG, "直播错误:" + goCoderStatus.getLastError().getErrorDescription());

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
//                mStatusView.setStatus(goCoderStatus);
                updateUIControls();
            }
        });
    }


    public void onToggleBroadcast(View v) {
        if (getBroadcast() == null) return;

        if (getBroadcast().getBroadcastStatus().isIdle()) {
            // Update the video source orientation in the broadcast config with the current device orientation
//            getBroadcastConfig().setVideoSourceOrientation(WZDeviceUtils.getDeviceOrientation(this));

            WZStreamingError configError = startBroadcast();
            if (configError != null) {
//                mStatusView.setErrorMessage(configError.getErrorDescription());
            }
        } else {
            endBroadcast();
        }
    }

    /**
     * Click handler for the settings button
     */
    public void onSettings(View v) {
        if (getBroadcastConfig() == null) return;

        WZMediaConfig configs[] = getVideoConfigs(mGoCoderCameraView);

        int avcProfiles[] = WZEncoderAPI.getProfiles();
        if (avcProfiles.length > 1) Arrays.sort(avcProfiles);

        WZProfileLevel avcProfileLevels[] = WZEncoderAPI.getProfileLevels();
        if (avcProfileLevels.length > 1) Arrays.sort(avcProfileLevels);

        ArrayList<WZProfileLevel> allProfileLevels = new ArrayList<>();

        if (avcProfiles.length > 0) {
            for (int avcProfile : avcProfiles) {
                WZProfileLevel autoLevel = new WZProfileLevel(avcProfile, WZProfileLevel.PROFILE_LEVEL_AUTO);
                allProfileLevels.add(autoLevel);

                for (WZProfileLevel avcProfileLevel : avcProfileLevels) {
                    if (avcProfileLevel.getProfile() == avcProfile) {
                        allProfileLevels.add(avcProfileLevel);
                    }
                }
            }
        }

        avcProfileLevels = allProfileLevels.toArray(new WZProfileLevel[allProfileLevels.size()]);

        Intent intent = new Intent(this, ConfigPrefsActivity.class);
        intent.putExtra(ConfigPrefs.PREFS_TYPE, ConfigPrefs.ALL_PREFS);
        intent.putExtra(ConfigPrefs.VIDEO_CONFIGS, configs);
        intent.putExtra(ConfigPrefs.H264_PROFILE_LEVELS, avcProfileLevels);
        startActivity(intent);
    }


    public void onSwitchCamera(View v) {
        if (mGoCoderCameraView == null) return;

        WZCamera newCamera = mGoCoderCameraView.switchCamera();
        if (newCamera != null)
            ConfigPrefs.setActiveCamera(PreferenceManager.getDefaultSharedPreferences(this), newCamera.getCameraId());

        boolean hasTorch = (newCamera != null && newCamera.hasCapability(WZCamera.TORCH));
//        mBtnTorch.setEnabled(hasTorch);
        if (hasTorch) {
//            mBtnTorch.setStateOn(newCamera.isTorchOn());
        }
//        mBtnTorch.setVisible(hasTorch);
    }

    /**
     * Click handler for the torch/flashlight button
     */
    public void onToggleTorch(View v) {
        if (mGoCoderCameraView == null) return;

        WZCamera activeCamera = mGoCoderCameraView.getCamera();
//        activeCamera.setTorchOn(mBtnTorch.toggleState());
    }

    /**
     * Click handler for the mic/mute button
     */
    public void onToggleMute(View v) {
        if (sGoCoder == null) return;
//        sGoCoder.getDefaultAudioDevice().setMuted(!mBtnMic.toggleState());
    }

    /**
     * Click handler for the scale mode button
     */
    public void onScaleMode(View v) {
        if (mGoCoderCameraView == null) return;

        mScaleModeIndex = ++mScaleModeIndex % SCALE_MODES.length;
        mGoCoderCameraView.setScaleMode(SCALE_MODES[mScaleModeIndex]);
        setScaleButtonLabel();
    }

    private void setScaleButtonLabel() {
        int scaleMode = mGoCoderCameraView != null ? mGoCoderCameraView.getScaleMode() : SCALE_MODES[mScaleModeIndex];
        Button btn = (Button) findViewById(R.id.btnScale);


//        switch(scaleMode) {
//            case WowzaConfig.FILL_VIEW:
//                btn.setText(getString(R.string.resize_aspect_fill_mode));
//                break;
//            case WowzaConfig.RESIZE_TO_ASPECT:
//                btn.setText(getString(R.string.resize_aspect_mode));
//                break;
//        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mAutoFocusDetector != null)
            mAutoFocusDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }


    BaseTask uploadTask;

    protected void uploadSceen(final Context context, final String fileUrl, final OnUploadCallBack onUploadCallBack) {

        if (null != uploadTask && uploadTask.getStatus() == AsyncTask.Status.RUNNING) {
            uploadTask.cancel(true);

        }

        uploadTask = new BaseTask(new NetCallBack() {

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {
                    byte[] b = BitmapTool.readStream(new FileInputStream(fileUrl));

                    FormFile form = new FormFile("avatar", b, "crop_cache_file.png", "application/octet-stream");
                    FormFile[] temp = {form};
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("api_token", App.getApp().getApiToken());
                    netResult = NetInterface.uploadScreen(temp, map);
                } catch (Exception e) {
                    e.printStackTrace();
                    LogUtil.e(App.TAG, "upload error:" + e.getLocalizedMessage());
                    if (e instanceof NetException) {
                        LogUtil.e(App.TAG, "upload error:" + ((NetException) e).getCode());
                    }
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
//                baseTask.hideDialogSelf();

                if (null != onUploadCallBack) {
                    onUploadCallBack.onUploadEnd(result, baseTask.activity);
                }


                if (null != result && StringUtils.isEmpty(result.getMessage())) {
                    try {
                        File file = new File(fileUrl);
                        if (null != file && file.exists() && file.isFile()) {
                            file.delete();
                        }
                        LogUtil.e(App.TAG, "deleted file:" + fileUrl);
                    } catch (Exception e) {
                        LogUtil.e(App.TAG, "delete file fail");
                    }

                }


            }
        }, context);
        uploadTask.execute(new HashMap<String, String>());

    }

    /**
     * Update the state of the UI controls===============================================
     */
    protected boolean updateUIControls() {
        boolean disableControls = (getBroadcast() == null ||
                !(getBroadcast().getBroadcastStatus().isIdle() ||
                        getBroadcast().getBroadcastStatus().isRunning()));

        if (disableControls) {
//            mBtnBroadcast.setEnabled(false);
//            mBtnSwitchCamera.setEnabled(false);
//            mBtnTorch.setEnabled(false);
//            mBtnMic.setVisible(false);
//            mBtnSettings.setEnabled(false);
            //mBtnScale.setEnabled(false);
        } else {
            boolean isStreaming = getBroadcast().getBroadcastStatus().isRunning();

//            mBtnBroadcast.setStateOn(isStreaming);
//            mBtnBroadcast.setEnabled(true);
//            mBtnSettings.setEnabled(!isStreaming);

            boolean isDisplayingVideo = (getBroadcastConfig().isVideoEnabled() && mGoCoderCameraView.getCameras().length > 0);
            if (isDisplayingVideo) {
                WZCamera activeCamera = mGoCoderCameraView.getCamera();

                boolean hasTorch = (activeCamera != null && activeCamera.hasCapability(WZCamera.TORCH));
//                mBtnTorch.setEnabled(hasTorch);
                if (hasTorch) {
//                    mBtnTorch.setStateOn(activeCamera.isTorchOn());
                }
//                mBtnTorch.setVisible(hasTorch);
//
//                mBtnSwitchCamera.setEnabled(mGoCoderCameraView.isSwitchCameraAvailable());
//                mBtnSwitchCamera.setVisible(mGoCoderCameraView.isSwitchCameraAvailable());
            } else {
//                mBtnSwitchCamera.setVisible(false);
//                mBtnTorch.setVisible(false);
            }

            //mBtnScale.setVisibility(isDisplayingVideo ? View.VISIBLE : View.INVISIBLE);
            //mBtnScale.setEnabled(isDisplayingVideo);

            boolean isStreamingAudio = (isStreaming && getBroadcastConfig().isAudioEnabled());
//            mBtnMic.setEnabled(isStreamingAudio);
//            mBtnMic.setStateOn(!sGoCoder.getDefaultAudioDevice().isMuted());
//            mBtnMic.setVisible(isStreamingAudio);

//            if (isStreaming && !mTimerView.isRunning()) {
//                mTimerView.startTimer();
//            } else if (getBroadcast().getBroadcastStatus().isIdle() && mTimerView.isRunning()) {
//                mTimerView.stopTimer();
//            }
        }

        return disableControls;
    }


    //ViewPager适配器
    class MyPagerAdapter extends PagerAdapter {
        private List<View> mViewList;

        public MyPagerAdapter(List<View> mViewList) {
            this.mViewList = mViewList;
        }

        @Override
        public int getCount() {
            return mViewList.size();//页卡数
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;//官方推荐写法
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            container.addView(mViewList.get(position));//添加页卡
            return mViewList.get(position);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(mViewList.get(position));//删除页卡
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitleList.get(position);//页卡标题
        }

    }


    @OnClick(R.id.imageButtonClose)
    public void onClickClose() {

        closeAndFinish();

    }


    @Override
    public void onBackPressed() {
        closeAndFinish();
    }

    private void closeAndFinish() {

        if (null != dialogLiveWait && dialogLiveWait.isShowing()) {
            dialogLiveWait.dismiss();
            return;
        }

        endBroadcast();

        requestStartEndLive("", "", "0");
    }
}
