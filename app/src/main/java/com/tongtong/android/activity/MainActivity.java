package com.tongtong.android.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.Window;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.callback.OnUploadCallBack;
import com.common.net.FormFile;
import com.common.net.NetException;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.BitmapTool;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.common.view.BaseFragment;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.tongtong.android.App;
import com.tongtong.android.R;
import com.tongtong.android.domain.LiveType;
import com.tongtong.android.domain.User;
import com.tongtong.android.fragment.FragmentFactory;
import com.tongtong.android.fragment.ProfileFragment;
import com.tongtong.android.fragment.RegisterLoginFragment;
import com.tongtong.android.net.NetInterface;
import com.tongtong.android.photoutils.PhotoLibUtils;
import com.tongtong.android.photoutils.SharedpreferencesUtil;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends FragmentActivity implements RadioGroup.OnCheckedChangeListener, RegisterLoginFragment.OnRegisterLoginFragmentInteractionListener, ProfileFragment.OnFragmentInteractionListener {

    private FragmentManager fragmentManager;
    private BaseFragment currentFragment;
    private int type = 0;
    private ProfileFragment profileFragment;
    private RadioGroup radioGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        App.getApp().addActivity(this);
        fragmentManager = getSupportFragmentManager();
        radioGroup = (RadioGroup) findViewById(R.id.rg_tab);
        radioGroup.setOnCheckedChangeListener(this);

        findViewById(R.id.rb_home).performClick();

        requestLiveTypes();
        LogUtil.i("MainActivity", "onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();
//        showFragment(R.id.rb_register_login);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        showFragment(checkedId);
    }

    @Override
    public void onRegisterLoginFragmentInteraction(boolean isShow) {
        if (isShow) {
            LogUtil.i("SHOW", String.valueOf(isShow));
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void showMoreFragment() {
        radioGroup.check(R.id.rb_live);
    }


    public void showFragment(int checkedId) {
        LogUtil.i("MainActivity", "showFragment");

        FragmentTransaction transaction = fragmentManager.beginTransaction();

        Fragment targetFragment = FragmentFactory.getInstanceByIndex(checkedId);
        if (targetFragment instanceof ProfileFragment) {
            profileFragment = (ProfileFragment) targetFragment;
        }

        currentFragment = (BaseFragment) targetFragment;

        transaction.replace(R.id.content, targetFragment);

        transaction.addToBackStack(null);

        transaction.commit();


    }


    //====================picture selct=====================================================================

    public final short SHORT_REQUEST_ADDCATCH = 1000;
    public final short SHORT_REQUEST_SELECTIMAGE = 1001;
    public final short SHORT_ACCESS_LOCATION = 1003;
    public final short SHORT_REQUEST_READ_EXTEAL = 1004;
    public final short SHORT_REQUEST_READ_EXTEAL_CATCH = 1005;
    public final short SHORT_REQUEST_MEDIAS = 1006;
    public final short SHORT_REQUEST_PHONE_STATE = 1007;
    public final short SHORT_REQUEST_CANCEL_VERIFY = 1008;
    public final short SHORT_REQUESET_VERIFY = 1009;

    public short SELECT_PIC_KITKAT = 801;
    public short SELECT_PIC = 802;
    public short TAKE_BIG_PICTURE = 803;
    public short CROP_BIG_PICTURE = 804;
    public int outputX = 512;
    public int outputY = 512;


    public byte TYPE_REQEST_PICTURE_TAKE = 0;
    public byte TYPE_REQEST_PICTURE_SELECT = 1;
    public SharedpreferencesUtil sharedpreferencesUtil = new SharedpreferencesUtil(this);

    public byte type_request_picture = TYPE_REQEST_PICTURE_TAKE;


    public void startTakePicture() {
        type_request_picture = TYPE_REQEST_PICTURE_TAKE;
        if (!sharedpreferencesUtil.getImageTempNameUri().equals("")) {
            sharedpreferencesUtil.delectImageTemp();
        }
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, sharedpreferencesUtil.getImageTempNameUri());
        startActivityForResult(intent, TAKE_BIG_PICTURE);
    }

    public void startSelectPicture() {
        type_request_picture = TYPE_REQEST_PICTURE_SELECT;
        if (!sharedpreferencesUtil.getImageTempNameUri().equals("")) {
            sharedpreferencesUtil.delectImageTemp();
        }
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/jpeg");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            startActivityForResult(intent, SELECT_PIC_KITKAT);
        } else {
            startActivityForResult(intent, SELECT_PIC);
        }
    }


    public void onTakePhtoClick() {

        type_request_picture = TYPE_REQEST_PICTURE_TAKE;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                    ) {
                startTakePicture();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, SHORT_REQUEST_READ_EXTEAL);
            }
        } else {
            startTakePicture();
        }
    }

    public void onChosePhtoClick() {
        type_request_picture = TYPE_REQEST_PICTURE_SELECT;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                startSelectPicture();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission_group.CAMERA}, SHORT_REQUEST_READ_EXTEAL);
            }
        } else {
            startSelectPicture();
        }
    }

    public void cropImageUri(Uri uriIn, Uri uriOut, int outputX, int outputY, int requestCode) {


        //======use local api

//        Intent intent = new Intent("com.android.camera.action.CROP");
//        intent.setDataAndType(uriIn, "image/*");
//        intent.putExtra("crop", "true");
//        intent.putExtra("aspectX", 1);
//        intent.putExtra("aspectY", 1);
//        intent.putExtra("outputX", outputX);
//        intent.putExtra("outputY", outputY);
//        intent.putExtra("scale", true);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, uriOut);
//        intent.putExtra("return-data", false);
//        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
//        intent.putExtra("noFaceDetection", true); // no face detection
//        startActivityForResult(intent, requestCode);


        //===============use crop new api=========
        String imgUrl = uriIn.getPath();

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imgUrl, options);

        int w = options.outWidth;
        int h = options.outHeight;

        if (w < outputX || h < outputY) {
            Tool.showMessageDialog("This picture is too small please select biger picture", this);
            return;
        }

        CropImage.activity(uriIn)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this, outputX, outputY, outputX, outputY, CropImageView.CropShape.RECTANGLE, Bitmap.CompressFormat.PNG);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        LogUtil.i(App.TAG, "OnActiviytResult---------->enter");
        LogUtil.i(App.TAG, "requestCode:" + requestCode + " resultCode:" + resultCode);


        if (null != profileFragment) {
            profileFragment.onActivityResult(requestCode, resultCode, data);
        }


        String mFileName;
        if (requestCode == SELECT_PIC && resultCode == RESULT_OK) {
            if (resultCode == RESULT_OK && data != null) {
                mFileName = PhotoLibUtils.getDataColumn(getApplicationContext(), data.getData(), null, null);
                PhotoLibUtils.copyFile(mFileName, sharedpreferencesUtil.getImageTempNameString2());
                Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
                cropImageUri(sharedpreferencesUtil.getImageTempNameUri(), outoutUri, outputX, outputY, CROP_BIG_PICTURE);
            }
        }
        if (requestCode == SELECT_PIC_KITKAT && resultCode == RESULT_OK) {
            if (resultCode == RESULT_OK && data != null) {
                mFileName = PhotoLibUtils.getPath(getApplicationContext(), data.getData());
                String newPath = sharedpreferencesUtil.getImageTempNameString2();
                PhotoLibUtils.copyFile(mFileName, newPath);
                Uri uri = sharedpreferencesUtil.getImageTempNameUri();
                Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
                cropImageUri(uri, outoutUri, outputX, outputY, CROP_BIG_PICTURE);

            }
        }
        if (requestCode == TAKE_BIG_PICTURE && resultCode == RESULT_OK) {
            Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
            cropImageUri(sharedpreferencesUtil.getImageTempNameUri(), outoutUri, outputX, outputY, CROP_BIG_PICTURE);
        }


        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                String imgURI = (String) result.getUri().getPath();//.getStringExtra("URI");
                onCropImgSuccess(imgURI);
//                ((ImageView) findViewById(R.id.quick_start_cropped_image)).setImageURI(result.getUri());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }

        }


        if (requestCode == CROP_BIG_PICTURE && resultCode == Activity.RESULT_OK) {
            if (sharedpreferencesUtil.getImageTempNameUri() != null) {
                Uri result = sharedpreferencesUtil.getImageTempNameUriCroped();
                App.firstRun();
                onCropImgSuccess(result.getPath());
            }
        }
    }


    public void onCropImgSuccess(String picPath) {

        if (!StringUtils.isEmpty(picPath)) {
            File selectFile = new File(picPath);
            if (null != selectFile && selectFile.exists()) {
                try {

                    upLoadHeaderImg(MainActivity.this, picPath, new OnUploadCallBack() {
                        @Override
                        public void onUploadEnd(NetResult netResult, Context context) {

                            if (null != netResult) {
                                if (netResult.isOk()) {
                                    String url = (String) netResult.getData()[0];
                                    profileFragment.updateUserHeader(url);

                                    User user = App.getApp().getUser();
                                    user.setAvatar(url);
                                    App.getApp().setUser(user);

                                } else {
                                    Tool.showMessageDialog(netResult.getMessage(), (Activity) context);
                                }
                            }
                        }
                    });

                } catch (Exception e) {
                    LogUtil.e(App.TAG, "compress picture fail!");
                }
            } else {
                LogUtil.i(App.TAG, "file not exist!");
            }

        }

    }


    BaseTask uploadTask;

    protected void upLoadHeaderImg(final Context context, final String fileUrl, final OnUploadCallBack onUploadCallBack) {

        if (null != uploadTask && uploadTask.getStatus() == AsyncTask.Status.RUNNING) {
            uploadTask.cancel(true);

        }

        uploadTask = new BaseTask(new NetCallBack() {
            @Override
            public void onPreCall(BaseTask baseTask) {

                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {
                    byte[] b = BitmapTool.readStream(new FileInputStream(fileUrl));


                    FormFile form = new FormFile("avatar", b, "crop_cache_file.png", "application/octet-stream");
                    FormFile[] temp = {form};
                    HashMap<String, String> map = new HashMap<String, String>();

                    map.put("api_token", App.getApp().getApiToken());
                    netResult = NetInterface.uploadAvatar(temp, map);
                } catch (Exception e) {
                    e.printStackTrace();
                    LogUtil.e(App.TAG, "upload error:" + e.getLocalizedMessage());
                    if (e instanceof NetException) {
                        LogUtil.e(App.TAG, "upload error:" + ((NetException) e).getCode());
                    }
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();

                if (null != onUploadCallBack) {
                    onUploadCallBack.onUploadEnd(result, baseTask.activity);
                }


                if (null != result && StringUtils.isEmpty(result.getMessage())) {
                    try {
                        File file = new File(fileUrl);
                        if (null != file && file.exists() && file.isFile()) {
                            file.delete();
                        }
                        LogUtil.e(App.TAG, "deleted file:" + fileUrl);
                    } catch (Exception e) {
                        LogUtil.e(App.TAG, "delete file fail");
                    }

                }
            }
        }, context);
        uploadTask.execute(new HashMap<String, String>());

    }

    BaseTask baseTask;

    private void requestLiveTypes() {

        if (null != baseTask && baseTask.getStatus() == AsyncTask.Status.RUNNING) {
            baseTask.cancel(true);
        }

        baseTask = new BaseTask(this, new NetCallBack() {


            @Override
            public void onPreCall(BaseTask baseTask) {

            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;

                try {
                    netResult = NetInterface.getLiveType(MainActivity.this, new JSONObject());
                } catch (Exception e) {
                    LogUtil.i(App.TAG, "request live type error");
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {

                if (null != result && result.isOk()) {
                    ArrayList<LiveType> liveTypes = ((ArrayList<LiveType>) result.getData()[0]);
                    App.getApp().setLiveTypes(liveTypes);

                }
            }
        });

        baseTask.execute(new HashMap<String, String>());
    }

    private long mPressedTime = 0;

    @Override
    public void onBackPressed() {
        if (null != currentFragment && currentFragment.onBackPressed() == true) {
            return;
        }
        LogUtil.i(App.TAG, "backpressed:....");
        long mNowTime = System.currentTimeMillis();
        if ((mNowTime - mPressedTime) > 2000) {
            Toast.makeText(this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
            mPressedTime = mNowTime;
        } else {
            App.getApp().exit();
        }

    }


}

