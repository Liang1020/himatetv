package com.tongtong.android.activity;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.StringUtils;
import com.squareup.picasso.Picasso;
import com.tongtong.android.App;
import com.tongtong.android.R;
import com.tongtong.android.domain.Profile;
import com.tongtong.android.net.NetInterface;
import com.tongtong.android.utils.LiveTypeUtil;
import com.tongtong.android.view.ReportDialog;
import com.tongtong.android.view.ShapedImageView;

import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class AnchorInfoActivity extends AppCompatActivity {

    @InjectView(R.id.textViewNickname)
    TextView textViewNickName;

    @InjectView(R.id.textViewEmail)
    TextView textViewEmail;

    @InjectView(R.id.textViewCity)
    TextView textViewCity;

    @InjectView(R.id.textViewCategory)
    TextView textViewCategory;

    @InjectView(R.id.textViewItem)
    TextView textViewItem;

    @InjectView(R.id.textViewAddr)
    TextView textViewAddr;

    @InjectView(R.id.textViewDesc)
    TextView textViewDescription;

    @InjectView(R.id.roundImageViewProfile)
    ShapedImageView roundImageView;

    private BaseTask taskProfile;
    private String userId;
    private Profile profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anchor_info);
        ButterKnife.inject(this);
        App.getApp().addActivity(this);
        userId = getIntent().getStringExtra("userId");
        updateUserHeader(getIntent().getStringExtra("imgUrl"));
        requestProfile();

    }

    @OnClick(R.id.imageViewBack)
    public void onBackClick(){
        finish();
    }

    @OnClick(R.id.textViewReport)
    public void onReportClick(){
        ReportDialog dialog = new ReportDialog(AnchorInfoActivity.this, userId);
        dialog.show();

    }
    public void updateUserHeader(String path) {

        if (StringUtils.isEmpty(path)) {
            Picasso.with(App.getApp()).load(R.drawable.bg_default_header).resize(200, 200).config(Bitmap.Config.RGB_565).into(roundImageView);
        } else {
            Picasso.with(App.getApp()).load(path).resize(200, 200).placeholder(R.drawable.bg_default_header).config(Bitmap.Config.RGB_565).into(roundImageView);
        }

    }
    private void requestProfile() {

        if (null != taskProfile && taskProfile.getStatus() == AsyncTask.Status.RUNNING) {
            taskProfile.cancel(true);
        }

        taskProfile = new BaseTask(AnchorInfoActivity.this, new NetCallBack() {

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {

                    netResult = NetInterface.getProfile(baseTask.activity, userId);

                } catch (Exception e) {
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (null != result && result.isOk()) {
                    profile = (Profile) result.getData()[0];
                    buildAdapter(profile);
                }
            }

        });

        taskProfile.execute(new HashMap<String, String>());
    }

    private void buildAdapter(Profile profile){
        textViewNickName.setText(profile.getNickname());
        textViewEmail.setText(profile.getEmail());
        textViewCity.setText(profile.getCity());
        textViewCategory.setText(LiveTypeUtil.convertIdtoString(App.getApp().getLiveTypes(), profile.getLiveType()));
        textViewItem.setText(profile.getLivTopic());
        textViewAddr.setText(profile.getLiveAddr());
        textViewDescription.setText(profile.getDescription());
    }

}

