package com.tongtong.android.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.LogUtil;
import com.common.util.Tool;
import com.markmao.pulltorefresh.widget.XListView;
import com.tongtong.android.App;
import com.tongtong.android.R;
import com.tongtong.android.adapter.RoomAdapter;
import com.tongtong.android.domain.Room;
import com.tongtong.android.net.NetInterface;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class SearchActivity extends BaseTongTongActivity {


    @InjectView(R.id.editTextSearchContent)
    EditText editTextSearchContent;

    @InjectView(R.id.listView)
    XListView listView;


    private int startPage = 1;
    private int currentpage = startPage;
    private boolean isRefresh = false;

    private RoomAdapter roomAdapter;
    private ArrayList<Room> roomArrayList;
    private int total;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.inject(this);
        App.getApp().addActivity(this);

        this.listView.setPullRefreshEnable(true);
        this.listView.setPullLoadEnable(true);
        this.listView.getFooterView().setVisibility(View.GONE);

        listView.setXListViewListener(new XListView.IXListViewListener() {
            @Override
            public void onRefresh() {
                String searchContent = getInput(editTextSearchContent);
                requestSearch(searchContent, true, false);

            }

            @Override
            public void onLoadMore() {
                String searchContent = getInput(editTextSearchContent);
                requestSearch(searchContent, false, false);
            }
        });

        roomArrayList = new ArrayList<>();
        roomAdapter = new RoomAdapter(this, roomArrayList, listView);
        listView.setAdapter(roomAdapter);


    }


    @OnTextChanged(R.id.editTextSearchContent)
    public void onInputChanged() {
//        listView.autoRefresh();
    }


    @OnClick(R.id.viewSearch)
    public void onSearchClick() {
        listView.autoRefresh();
    }

    private void requestSearch(final String content, final boolean isRefresh, final boolean showDialog) {
        reSetTask();
        baseTask = new BaseTask(new NetCallBack() {


            @Override
            public void onPreCall(BaseTask baseTask) {

                if (showDialog) {
                    baseTask.showDialogForSelf(true);
                }

            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;

                try {


                    JSONObject jsonObject = new JSONObject();

                    jsonObject.put("search", content);
                    jsonObject.put("page", currentpage);
                    netResult = NetInterface.searchRoom(SearchActivity.this, jsonObject);

                } catch (Exception e) {
                    e.printStackTrace();
                    LogUtil.e(App.TAG, "msg:" + e.getLocalizedMessage());

                }

                return netResult;

            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {

                if (showDialog) {
                    baseTask.hideDialogSelf();
                }

                if (null != result) {

                    if (result.isOk()) {

                        int total = (Integer) result.getData()[1];
                        if (isRefresh) {
                            roomArrayList.clear();
                            ArrayList<Room> rooms = (ArrayList<Room>) result.getData()[0];
                            roomArrayList.addAll(rooms);
                        } else {
                            ArrayList<Room> rooms = (ArrayList<Room>) result.getData()[0];
                            roomArrayList.addAll(rooms);
                        }
                        roomAdapter.notifyDataSetChanged();

                    } else {
                        Tool.showMessageDialog(result.getMessage(), SearchActivity.this);
                    }
                } else {
                    Tool.showMessageDialog(R.string.error_net, SearchActivity.this);
                }

                listView.stopRefreshLoadUI();


                listView.dealFooter(roomArrayList, total, listView);

            }
        }, this);

        baseTask.execute(new HashMap<String, String>());
    }


    @OnClick(R.id.imageButtonBack)
    public void onBackClick() {
        finish();
    }


}
