package com.tongtong.android.activity;

import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.SharePersistent;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.tongtong.android.App;
import com.tongtong.android.R;
import com.tongtong.android.net.NetInterface;
import com.tongtong.android.view.ForgetPasswordDialog;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class LoginActivity extends BaseTongTongActivity {

    @InjectView(R.id.editTextEmail)
    EditText editTextEmail;

    @InjectView(R.id.editTextpwd)
    EditText editTextpwd;

    @InjectView(R.id.textViewSkip)
    TextView textViewSkip;

    @InjectView(R.id.textViewForgetpwd)
    TextView textViewForgetpwd;


    ForgetPasswordDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.inject(this);
        App.getApp().addActivity(this);

        textViewSkip.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        textViewSkip.getPaint().setAntiAlias(true);

        String username = SharePersistent.getPreferenceDe(this, App.USERNAME_TAG);
        String password = SharePersistent.getPreferenceDe(this, App.PASSWORD_TAG);
        editTextEmail.setText(username);
        editTextpwd.setText(password);
        if (username != null && password != null) {
            login(username, password);
        }

//        if (null != App.getApp().getUser()) {
//            Tool.startActivity(this, MainActivity.class);
//        }


    }


    public void login(final String username, final String password) {
        requestLogin(username, password, LoginActivity.this, true, new NetCallBack() {
            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (!isFinishing()) {
                    if (null != result) {
                        if (result.isOk()) {
                            App.getApp().savePassword(LoginActivity.this, username, password);

                            Tool.startActivity(LoginActivity.this, MainActivity.class);
                            finish();
                        } else {
                            Tool.showMessageDialog(result.getMessage(), LoginActivity.this);
                        }
                    } else {
                        Tool.showMessageDialog("Net error", LoginActivity.this);
                    }
                }

            }
        });
    }

    @OnClick(R.id.buttonSignIn)
    public void onSignInClick() {
        login(getInput(editTextEmail), getInput(editTextpwd));
    }

    @OnClick(R.id.buttonSignUp)
    public void onSignUpClick() {
        Tool.startActivity(LoginActivity.this, RegisterActivity.class);
    }

    @OnClick(R.id.textViewSkip)
    public void onSkipLogin() {
        Tool.startActivity(LoginActivity.this, MainActivity.class);
    }

    @OnClick(R.id.textViewForgetpwd)
    public void onForgetClick() {
        dialog = new ForgetPasswordDialog(LoginActivity.this, editTextEmail.getText().toString());
        dialog.setOnConfirmDialogListener(new ForgetPasswordDialog.OnConfirmDialogListener() {
            @Override
            public void OnConfirmClick(String email) {
                if (!StringUtils.isEmpty(email)) {
                    forgetPwd(email);
                }
            }

            @Override
            public void OncancelClick() {

            }
        });
        dialog.show();
    }

    BaseTask baseTask;

    private void forgetPwd(final String email) {
        if (null != baseTask && baseTask.getStatus() == AsyncTask.Status.RUNNING) {
            baseTask.cancel(true);
        }

        baseTask = new BaseTask(LoginActivity.this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("token", App.getApp().getApiToken());
                    jsonObject.put("email", email);
                    netResult = NetInterface.forgetPassword(baseTask.activity, jsonObject);

                } catch (Exception e) {
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null == result) {
                    showNotifyTextIn5Seconds(getResources().getString(R.string.error_net));
                } else {
                    if (result.isOk()) {
//                        showNotifyTextIn5Seconds("Success");
                        Tool.showMessageDialog(getResources().getString(R.string.reset_password_successfully), LoginActivity.this);
                    } else {
                        showNotifyTextIn5Seconds(result.getMessage());
                    }
                }
            }

        });

        baseTask.execute(new HashMap<String, String>());


    }


    private long mPressedTime = 0;

    @Override
    public void onBackPressed() {

        if (null != dialog && dialog.isShowing()) {
            dialog.dismiss();
            return;
        }

        long mNowTime = System.currentTimeMillis();
        if ((mNowTime - mPressedTime) > 2000) {
            Toast.makeText(this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
            mPressedTime = mNowTime;
        } else {
            App.getApp().exit();
        }

    }

}
