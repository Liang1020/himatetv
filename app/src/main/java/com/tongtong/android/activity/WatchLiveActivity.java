package com.tongtong.android.activity;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.common.util.LogUtil;
import com.tongtong.android.App;
import com.tongtong.android.R;
import com.tongtong.android.adapter.ChannelPageAdapter;
import com.tongtong.android.domain.Room;
import com.tongtong.android.net.NetInterface;
import com.tongtong.android.utils.Media;
import com.tongtong.android.utils.NetworkUtil;
import com.tongtong.android.utils.ScreenUtil;
import com.tongtong.android.utils.TimeUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class WatchLiveActivity extends BaseTongTongActivity {
    private String path = "rtsp://10.0.1.2:1935/live/myStream";
//    private String path = "rtsp://10.0.1.15:1935/live/tt";

    @InjectView(R.id.textureVideoView)
    VideoView mVideoView;

    @InjectView(R.id.imageViewPlayPause)
    ImageView imageViewPlayPause;

    @InjectView(R.id.imageViewFullScreen)
    ImageView imageViewFullScreen;

    @InjectView(R.id.relativeLayoutMedia)
    RelativeLayout relativeLayoutMedia;

    @InjectView(R.id.relativeLayoutBottomMediaController)
    RelativeLayout relativeLayoutBottom;

    @InjectView(R.id.relativeLayoutTopMediaController)
    RelativeLayout relativeLayoutTop;

    @InjectView(R.id.relativeLayoutBottom)
    RelativeLayout relativeLayoutInfo;

    @InjectView(R.id.textViewDuration)
    TextView textViewDuration;

    @InjectView(R.id.textViewTitle)
    TextView textViewTitle;

    @InjectView(R.id.progressBarBuffer)
    ProgressBar progressBarBuffer;

    @InjectView(R.id.tabLayout)
    TabLayout tabLayout;

    @InjectView(R.id.vpChannel)
    ViewPager viewPagerChannel;

    private MediaPlayer mMediaPlayer;

    public static final String TAG = "WATCHLIVEACTIVITY";

    private static final int MSG_SURFACE_PREPARE = 0x00000000;
    private static final int MSG_SURFACE_START = 0x00000001;
    private static final int MSG_SURFACE_DESTORY = 0x00000003;

    private static final int MSG_SCREEN_FULL = 0x00000004;
    private static final int MSG_SCREEN_WRAP = 0x00000005;

    private static final int MSG_UPDATE_PROGRESS = 0x00000006;
    private static final int MSG_MEDIA_CONTROLLER_HIDE = 0x00000007;

    private static final String KEY_CURRENT_POSITION = "KEY_CURRENT_POSITION";

    private Timer mServerTimer = null;
    private Timer mControllerTimer = null;

    private int mResumePostion = 0;
    private int mDuration = -1;
    private boolean mIsHandPause = false;

    private int mCurrentState = 0;

    private ArrayList<View> viewList;
    private ArrayList<String> titleList;
    private View viewInfo, viewAnchor;

    /**
     * 由于mediaplay为异步加载，防止程序进入后台后播放
     * onPause()时，记录当前播放状态，重新onResume()恢复之前状态
     * mIsPrepred 参数为onPause()判断当前是否初始化完成
     */
    private boolean mIsOnPauseStatus = false;  //记录onPause()之前的播放状态 播放或者暂停
    private boolean mIsPrepred = false;        //记录onPause()之前 视频是否初始化完成
    private boolean mIsBackPrepared = false;           //由于mediaplay是异步加载，当home时可能会在后台播放的可能
    private Room room;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_SURFACE_PREPARE:
                    Media mediaInfor = (Media) msg.obj;
                    setDataSource(mediaInfor.getmPath(), mediaInfor.getmName());
                    mDuration = 1000;
                    break;

                case MSG_SURFACE_START:
                    mIsPrepred = true;
                    progressBarBuffer.setVisibility(View.GONE);
                    imageViewPlayPause.setImageResource(R.drawable.selector_pause);
                    mVideoView.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                    mVideoView.start();
                    startProgressTimer();
                    startControllerShowOrHide();
                    break;

                case MSG_SURFACE_DESTORY:
                    if (relativeLayoutBottom.getVisibility() == View.VISIBLE) {
                        mediaControllerHide();
                    } else {
                        mediaControllerShow();
                    }
                    break;

                case MSG_SCREEN_FULL:
                    screenFullModeUI();
                    break;

                case MSG_SCREEN_WRAP:
                    screenWrapModeUI();
                    break;

                case MSG_UPDATE_PROGRESS:
                    setTime(mDuration);
                    break;

                case MSG_MEDIA_CONTROLLER_HIDE:
                    mediaControllerHide();
                    break;

                default:
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_watch_live);
        ButterKnife.inject(this);

        initBottomLayout();
        resetTime();

        path = "rtsp://" + NetInterface.live_path + ":1935/live/myStream";

        room = (Room) App.getApp().getTempObject("room");
        path = room.getUrl();

        Log.d("media url=====", path);
        playMedia(path, "title");
//        try
//        {
//            mVideoView.setVideoURI(Uri.parse(path));
//            mVideoView.requestFocus();
//            mVideoView.postInvalidateDelayed(100);
//            mVideoView.start();
//
//
//        }catch (Exception e) {
//            Tool.showMessageDialog("Error Occured:- " + e.getMessage(), this);
//        }
    }

    @OnClick(R.id.imageViewPlayPause)
    public void onPlayPauseClick() {
        if (mVideoView.isPlaying()) {
            mVideoView.pause();
            imageViewPlayPause.setImageResource(R.drawable.selector_play);
            mIsHandPause = true;
        } else {
            mVideoView.start();
            imageViewPlayPause.setImageResource(R.drawable.selector_pause);
            mIsHandPause = false;
        }
    }

    @OnClick(R.id.imageViewFullScreen)
    public void onFullScreenClick() {
        if (getScreenOrientation() == Configuration.ORIENTATION_PORTRAIT) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            return;
        }
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @OnClick(R.id.image_back)
    public void onOrientationBackClick() {
        if (getScreenOrientation() == Configuration.ORIENTATION_PORTRAIT) {
            finish();
            return;
        }
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private MediaPlayer.OnErrorListener mOnErrorListener = new MediaPlayer.OnErrorListener() {
        @Override
        public boolean onError(MediaPlayer mp, int what, int extra) {
            LogUtil.e(TAG, "MediaPalyer:" + what + ", " + extra);
            mVideoView.pause();
            resetTime();
            imageViewPlayPause.setImageResource(R.drawable.selector_play);
            return true;
        }
    };

    private MediaPlayer.OnCompletionListener mOnCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
//            if (mp.getDuration() != -1 && mp.getCurrentPosition() >= mp.getDuration() - 1000) {
//                cancleControllerTimer();
//                imageViewPlayPause.setImageResource(R.drawable.selector_play);
//                mediaControllerShow();
//            }

        }
    };

    private MediaPlayer.OnInfoListener mOnInfoListener = new MediaPlayer.OnInfoListener() {
        @Override
        public boolean onInfo(MediaPlayer mp, int what, int extra) {
            switch (what) {
                case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                    progressBarBuffer.setVisibility(View.VISIBLE);
                    break;

                case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                    if (mVideoView.isPlaying() || mIsHandPause) {
                        progressBarBuffer.setVisibility(View.GONE);
                    }
                    break;

                default:
                    break;
            }
            return true;
        }
    };

    private MediaPlayer.OnPreparedListener mOnPreparedListener = new MediaPlayer.OnPreparedListener() {
        @Override
        public void onPrepared(MediaPlayer mp) {
            int videoWidth = mp.getVideoWidth();
            int videoHeight = mp.getVideoHeight();
            float videoProportion = (float) videoWidth / (float) videoHeight;
            ;
            int screenWidth = getWindowManager().getDefaultDisplay().getWidth();
            int screenHeight = getWindowManager().getDefaultDisplay().getHeight();
            float screenProportion = (float) screenWidth / (float) screenHeight;
            android.view.ViewGroup.LayoutParams lp = mVideoView.getLayoutParams();

            if (videoProportion > screenProportion) {
                lp.width = screenWidth;
                lp.height = (int) ((float) screenWidth / videoProportion);
            } else {
                lp.width = (int) (videoProportion * (float) screenHeight);
                lp.height = screenHeight;
            }

            if (mIsBackPrepared) {
                mVideoView.pause();
                mIsBackPrepared = false;
            } else {
                mHandler.sendEmptyMessage(MSG_SURFACE_START);
            }
            if (mMediaPlayer == null) {
                mMediaPlayer = mp;
                mMediaPlayer.setVideoScalingMode(MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
                mMediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                        LogUtil.i(App.TAG, width + " " + height);


                    }
                });
                mMediaPlayer.setOnInfoListener(mOnInfoListener);
                mMediaPlayer.setOnBufferingUpdateListener(mOnBufferingUpdateListener);
            }
        }

    };

    private MediaPlayer.OnBufferingUpdateListener mOnBufferingUpdateListener = new MediaPlayer.OnBufferingUpdateListener() {
        @Override
        public void onBufferingUpdate(MediaPlayer mp, int percent) {
            if (mServerTimer != null && percent > 0) {
                return;
            }
        }
    };

    private View.OnTouchListener mOnTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            int action = event.getAction();
            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    mHandler.sendEmptyMessage(MSG_SURFACE_DESTORY);
                    break;
                default:
                    break;
            }
            return false;
        }
    };

    private void setListener() {
        mVideoView.setOnTouchListener(mOnTouchListener);
        mVideoView.setOnErrorListener(mOnErrorListener);
        mVideoView.setOnCompletionListener(mOnCompletionListener);
        mVideoView.setOnPreparedListener(mOnPreparedListener);
    }

    public void playMedia(String path, String name) {
        if (!NetworkUtil.isNetWorkEnable(this)) {
            return;
        }
        Message msg = Message.obtain();
        Media mediaInfor = new Media();
        mediaInfor.setmName(name);
        mediaInfor.setmPath(path);
        msg.obj = mediaInfor;
        msg.what = MSG_SURFACE_PREPARE;
        mHandler.sendMessage(msg);
    }

    public void setDataSource(String path, String name) {
        if (TextUtils.isEmpty(path)) return;
        progressBarBuffer.setVisibility(View.VISIBLE);
        resetTime();
        mVideoView.requestFocus();
        // mVideoView.setBackgroundColor(getResources().getColor(R.color.videobackcolor));
        try {
            setListener();
            mVideoView.setVideoPath(path);
            mVideoView.getWidth();
//            mVideoView.setScaleX(16f);
//            mVideoView.setScaleY(9f);
            mVideoView.start();
            textViewTitle.setText(name);
        } catch (IllegalStateException e) {
            LogUtil.e(TAG, e.getMessage());
        } catch (IllegalArgumentException e) {
            LogUtil.e(TAG, e.getMessage());
        }
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        int value = getScreenOrientation();
        if (value == Configuration.ORIENTATION_LANDSCAPE) {
            LogUtil.i(App.TAG, "---->Full Screen");
            mHandler.sendEmptyMessage(MSG_SCREEN_FULL);
        } else if (value == Configuration.ORIENTATION_PORTRAIT) {
            mHandler.sendEmptyMessage(MSG_SCREEN_WRAP);
            LogUtil.i(App.TAG, "---->Wrap Screen");
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (getScreenOrientation() == Configuration.ORIENTATION_LANDSCAPE) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                return false;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private int getMediaCurrentPostion() {
        if (mVideoView != null) {
            return mVideoView.getCurrentPosition();
        }
        return 0;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_CURRENT_POSITION, getMediaCurrentPostion());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            mResumePostion = savedInstanceState.getInt(KEY_CURRENT_POSITION, 0);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mIsBackPrepared = false;
        try {
            if (mVideoView != null) {
                mVideoView.seekTo(mResumePostion > 0 ? mResumePostion : 0);
                Log.e(TAG, "onResume:");
                mVideoView.start();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

        getCurrentState();

    }

    private void getCurrentState() {
        Field field;
        try {
            field = VideoView.class.getDeclaredField("mCurrentState");
            field.setAccessible(true);
            mCurrentState = field.getInt(mVideoView);
            Log.e(TAG, "mCurrentState: " + mCurrentState);
            if (mCurrentState == 0) {

            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        mIsBackPrepared = true;
        mIsOnPauseStatus = mVideoView.isPlaying();
        try {
            if (mVideoView != null) {
                mVideoView.pause();
                mIsHandPause = true;
                mResumePostion = getMediaCurrentPostion();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mVideoView != null) {
            mVideoView = null;
        }
        cancleControllerTimer();
    }

    public void screenFullModeUI() {
        relativeLayoutInfo.setVisibility(View.GONE);
        imageViewFullScreen.setImageResource(R.drawable.icon_shrink);
        setScreenLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        imageViewFullScreen.postInvalidate();
    }

    public void screenWrapModeUI() {
        relativeLayoutInfo.setVisibility(View.VISIBLE);
        imageViewFullScreen.setImageResource(R.drawable.icon_enlarge);
        setScreenLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ScreenUtil.dip2px(this, 220));
    }

    private int getScreenOrientation() {
        return getResources().getConfiguration().orientation;
    }

    private void setScreenLayoutParams(int width, int height) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width, height);
//        if (width == ViewGroup.LayoutParams.MATCH_PARENT && height == ViewGroup.LayoutParams.MATCH_PARENT) {
//            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
//            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
//            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//        }
        relativeLayoutMedia.setLayoutParams(layoutParams);
        mVideoView.requestFocus();
    }

    private void setTime(int duration) {
        textViewDuration.setText(TimeUtil.stringForTime(duration));
    }

    private void resetTime() {
        mDuration = 0;
        textViewDuration.setText("00:00");
    }

    private void mediaControllerHide() {
        relativeLayoutBottom.setVisibility(View.GONE);
        relativeLayoutTop.setVisibility(View.GONE);
        cancleControllerTimer();
    }

    private void mediaControllerShow() {
        cancleControllerTimer();
        relativeLayoutTop.setVisibility(View.VISIBLE);
        relativeLayoutBottom.setVisibility(View.VISIBLE);
        imageViewPlayPause.setImageResource(mVideoView.isPlaying() ? R.drawable.selector_pause : R.drawable.selector_play);
        startProgressTimer();
        startControllerShowOrHide();
    }

    public int getDuration() {
        int du = mVideoView.getDuration();
        int duration = du != -1 ? du : mDuration;
        return duration;
    }

    private void startProgressTimer() {
        cancleControllerTimer();
        mServerTimer = new Timer();
        mServerTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    mDuration += 1000;
                    mHandler.sendEmptyMessage(MSG_UPDATE_PROGRESS);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
            }
        }, 0, 1000);
    }

    private void startControllerShowOrHide() {
        mControllerTimer = new Timer();
        mControllerTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                mHandler.sendEmptyMessage(MSG_MEDIA_CONTROLLER_HIDE);
            }
        }, 3000, 1);
    }

    private void cancleControllerTimer() {
        if (mServerTimer != null) {
            mServerTimer.cancel();
            mServerTimer = null;
        }
        if (mControllerTimer != null) {
            mControllerTimer.cancel();
            mControllerTimer = null;
        }
    }

    private void initBottomLayout() {
        viewInfo = LayoutInflater.from(this).inflate(R.layout.page_live_info, null);
        viewAnchor = LayoutInflater.from(this).inflate(R.layout.page_participants, null);

        viewList = new ArrayList<>();
        viewList.add(viewInfo);
        viewList.add(viewAnchor);

        titleList = new ArrayList<>();
        titleList.add("直播信息");
        titleList.add("主播信息");

        ChannelPageAdapter pagerAdapter = new ChannelPageAdapter(viewList, titleList);
        viewPagerChannel.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPagerChannel);
    }
}
