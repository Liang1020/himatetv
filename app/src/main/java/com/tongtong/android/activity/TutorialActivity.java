package com.tongtong.android.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import com.common.util.LogUtil;
import com.tongtong.android.App;
import com.tongtong.android.R;

import java.util.ArrayList;

/**
 * Created by leocx on 2016/10/21.
 */

public class TutorialActivity extends BaseTongTongActivity {
    private ViewPager viewPager;
    private ArrayList<View> pageview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //设置无标题栏
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_tutorial);

        viewPager = (ViewPager) findViewById(R.id.vp_tutorial);

        //查找布局文件用LayoutInflater.inflate
//        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LayoutInflater inflater = getLayoutInflater();
        View view1 = inflater.inflate(R.layout.view_tut1, null);
        View view2 = inflater.inflate(R.layout.view_tut2, null);
        View view3 = inflater.inflate(R.layout.view_tut3, null);
        view3.findViewById(R.id.ib_tut3View_start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getSharedPreferences(App.TAG, MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                PackageInfo pInfo = null;
                try {
                    pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                    String version = pInfo.versionName;
                    int code = pInfo.versionCode;
                    editor.putString("lastVersion", version);
                    LogUtil.v("leo", "version: " + version + "  code: " + code);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                editor.commit();
                startActivity(new Intent(TutorialActivity.this, LoginActivity.class));
                finish();
            }
        });

        //将view装入数组
        pageview = new ArrayList<View>();
        pageview.add(view1);
        pageview.add(view2);
        pageview.add(view3);


        //数据适配器
        PagerAdapter mPagerAdapter = new PagerAdapter() {

            @Override
            //获取当前窗体界面数
            public int getCount() {
                // TODO Auto-generated method stub
                return pageview.size();
            }

            @Override
            //断是否由对象生成界面
            public boolean isViewFromObject(View arg0, Object arg1) {
                // TODO Auto-generated method stub
                return arg0 == arg1;
            }

            //是从ViewGroup中移出当前View
            public void destroyItem(View arg0, int arg1, Object arg2) {
                ((ViewPager) arg0).removeView(pageview.get(arg1));
            }

            //返回一个对象，这个对象表明了PagerAdapter适配器选择哪个对象放在当前的ViewPager中
            public Object instantiateItem(View arg0, int arg1) {
                ((ViewPager) arg0).addView(pageview.get(arg1));
                return pageview.get(arg1);
            }


        };

        //绑定适配器
        viewPager.setAdapter(mPagerAdapter);
    }
}
