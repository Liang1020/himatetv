package com.tongtong.android.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.callback.OnUploadCallBack;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.DialogUtils;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.tongtong.android.App;
import com.tongtong.android.R;
import com.tongtong.android.domain.Profile;
import com.tongtong.android.domain.User;
import com.tongtong.android.net.NetInterface;
import com.tongtong.android.photoutils.PhotoLibUtils;
import com.tongtong.android.photoutils.PhotoSelectDialog;
import com.tongtong.android.utils.NetworkUtil;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class StartLiveActivity extends BaseTongTongActivity {


    @InjectView(R.id.imageButtonClose)
    ImageButton imageButtonClose;

    @InjectView(R.id.imageButtonStart)
    ImageButton imageButtonStart;

    @InjectView(R.id.editTextTitle)
    EditText editTextTitle;

    @InjectView(R.id.editTextComments)
    EditText editTextComments;

    @InjectView(R.id.imageViewCover)
    ImageView imageViewCover;


    String[] mRequiredPermissions = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO
    };

    private short SHORT_GO_LIVE = 101;

    private String imgUploadUrl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_live);
        ButterKnife.inject(this);
        App.getApp().addActivity(this);
        initView();
    }

    public void initView() {


        User user = App.getApp().getUser();

        Profile profile = App.getApp().getProfile();

        if (null != profile && (!profile.getLivTopic().isEmpty() || !profile.getDescription().isEmpty())) {
            editTextTitle.setText(profile.getLivTopic());
            editTextComments.setText(profile.getDescription());
        }

        if (null != user && null != user.getRoom() && !StringUtils.isEmpty(user.getRoom().getImgUrl())) {
            imgUploadUrl = user.getRoom().getImgUrl();
            imageViewCover.setTag(imgUploadUrl);

            LogUtil.i(App.TAG, "刷新url:" + imgUploadUrl);

            Picasso.with(this).load((imgUploadUrl)).
                    config(Bitmap.Config.RGB_565).//networkPolicy(NetworkPolicy.NO_CACHE).
                    resize(512, 512).centerCrop().
                    placeholder(R.drawable.bg_himate_rect).into(imageViewCover);
        }


    }


    private boolean liveWithNowifi = false;

    @OnClick(R.id.imageButtonStart)
    public void onBeginLiving(View v) {

        if (!NetworkUtil.isWIFI(this) && liveWithNowifi == false) {

            DialogUtils.showConfirmDialog(this, "", "The current is not wifi whether or not to continue?", "Confirm", "Cancel", new DialogCallBackListener() {
                @Override
                public void onDone(boolean yesOrNo) {
                    liveWithNowifi = yesOrNo;
                    if (yesOrNo) {
                        onBeginLiving(null);
                    }
                }
            });

            return;

        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(this, mRequiredPermissions[0]) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, mRequiredPermissions[1]) == PackageManager.PERMISSION_GRANTED) {
//                startEndLive(getInput(editTextTitle), getInput(editTextComments));
                startLive();
            } else {
                ActivityCompat.requestPermissions(this, mRequiredPermissions, SHORT_REQUESET_PERMISSION);
            }
        } else {
            startLive();
//            startEndLive(getInput(editTextTitle), getInput(editTextComments));
        }


//        this.startActivity(new Intent(this, MyLiveMainActivity.class));

    }


    private void startLive() {

        if (null == imageViewCover.getTag() || StringUtils.isEmpty(imgUploadUrl)) {
            Tool.showMessageDialog("请选择并上传一张封面图片", StartLiveActivity.this);
            return;
        }

        String title = getInput(editTextTitle);
        if (StringUtils.isEmpty(title)) {
            Tool.showMessageDialog("Please input a title.", StartLiveActivity.this);
            return;
        }
        String content = getInput(editTextComments);
        if (StringUtils.isEmpty(content)) {
            Tool.showMessageDialog("Please input content.", StartLiveActivity.this);
            return;
        }

        requestUserLivePassword();
        updateRoom();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == SHORT_REQUESET_PERMISSION) {

            if (grantResults.length >= 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
//                startEndLive(getInput(editTextTitle), getInput(editTextComments));
                requestUserLivePassword();
            }
        }

        if (requestCode == SHORT_REQUEST_READ_EXTEAL && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            if (type_request_picture == TYPE_REQEST_PICTURE_SELECT) {
                onChosePhtoClick();
            } else if (type_request_picture == TYPE_REQEST_PICTURE_TAKE) {
                onTakePhtoClick();
            }
        }


    }


    @OnClick(R.id.imageButtonClose)
    public void onCloseClick() {
        finish();
    }

    @OnClick(R.id.imageViewCover)
    public void onImageButtonCover() {


//        outputX = 342 * 2;
//        outputY = 242 * 2;


        outputX = 512;
        outputY = 512;

        PhotoSelectDialog photoSelectDialog = new PhotoSelectDialog(this);
        photoSelectDialog.setOnAddCatchListener(new PhotoSelectDialog.OnPhotoSelectionActionListener() {

            @Override
            public void onTakePhotoClick() {
                onTakePhtoClick();
            }

            @Override
            public void onChoosePhotoClick() {
                onChosePhtoClick();
            }

            @Override
            public void onDismis() {

            }
        });
        photoSelectDialog.getRootView().clearAnimation();
        photoSelectDialog.show();

    }

    BaseTask taskProfile;

    private void updateRoom() {

        if (null != taskProfile && taskProfile.getStatus() == AsyncTask.Status.RUNNING) {
            taskProfile.cancel(true);
        }

        taskProfile = new BaseTask(StartLiveActivity.this, new NetCallBack() {

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("token", App.getApp().getApiToken());
                    jsonObject.put("nickname", App.getApp().getProfile().getNickname());
                    jsonObject.put("city", App.getApp().getProfile().getCity());
                    jsonObject.put("subject", editTextTitle.getText().toString());
                    jsonObject.put("contents", editTextComments.getText().toString());
                    jsonObject.put("type_id", App.getApp().getProfile().getLiveType());
                    App.getApp().getProfile().setLivTopic(editTextTitle.getText().toString());
                    App.getApp().getProfile().setDescription(editTextComments.getText().toString());
                    netResult = NetInterface.updateRoom(baseTask.activity, jsonObject);

                } catch (Exception e) {
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (null == result) {
                    Tool.showMessageDialog(getResources().getString(R.string.error_net), StartLiveActivity.this);
                } else {
                    if (result.isOk()) {
                        LogUtil.i("Update Room:", "Success");
                    } else {
                        Tool.showMessageDialog(result.getMessage(), StartLiveActivity.this);
                    }
                }
            }

        });

        taskProfile.execute(new HashMap<String, String>());
    }


    private void requestUserLivePassword() {
        reSetTask();
        baseTask = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }


            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;

                try {
                    JSONObject json = new JSONObject();

                    netResult = NetInterface.getliveUserPassword(StartLiveActivity.this, json);

                } catch (Exception e) {
                }

                return netResult;
            }


            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result) {
                    if (result.isOk()) {
//                        LogUtil.i(App.TAG, (String) result.getData()[0]);

                        App.getApp().putTemPObject("info", (JSONObject) result.getData()[0]);

                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("title", getInput(editTextTitle));
                            jsonObject.put("comments", getInput(editTextComments));


                            jsonObject.put("img", imgUploadUrl);


                        } catch (Exception e) {

                        }

                        App.getApp().putTemPObject("binfo", jsonObject);

//                        Tool.startActivityForResult(StartLiveActivity.this, MyLiveMainActivity.class, SHORT_GO_LIVE);

                        Tool.startActivityForResult(StartLiveActivity.this, LiveActivity.class, SHORT_GO_LIVE);

//                        Tool.startActivityForResult(StartLiveActivity.this, ConfigPrefsActivity.class, SHORT_GO_LIVE);


                    } else {
                        Tool.showMessageDialog(result.getMessage(), StartLiveActivity.this);
                    }
                } else {
                    Tool.showMessageDialog(R.string.error_net, StartLiveActivity.this);
                }

            }


        });

        baseTask.execute(new HashMap<String, String>());
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == SHORT_GO_LIVE && resultCode == RESULT_CANCELED) {
            if (null != data) {
                Tool.showMessageDialog(data.getStringExtra("msg"), StartLiveActivity.this);
            }
            return;
        }


        String mFileName;
        if (requestCode == SELECT_PIC && resultCode == RESULT_OK) {
            if (resultCode == RESULT_OK && data != null) {
                mFileName = PhotoLibUtils.getDataColumn(getApplicationContext(), data.getData(), null, null);
                PhotoLibUtils.copyFile(mFileName, sharedpreferencesUtil.getImageTempNameString2());
                Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
                cropImageUri(sharedpreferencesUtil.getImageTempNameUri(), outoutUri, outputX, outputY, CROP_BIG_PICTURE);
            }
        }
        if (requestCode == SELECT_PIC_KITKAT && resultCode == RESULT_OK) {
            if (resultCode == RESULT_OK && data != null) {
                mFileName = PhotoLibUtils.getPath(getApplicationContext(), data.getData());
                String newPath = sharedpreferencesUtil.getImageTempNameString2();
                PhotoLibUtils.copyFile(mFileName, newPath);
                Uri uri = sharedpreferencesUtil.getImageTempNameUri();
                Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
                cropImageUri(uri, outoutUri, outputX, outputY, CROP_BIG_PICTURE);

            }
        }
        if (requestCode == TAKE_BIG_PICTURE && resultCode == RESULT_OK) {
            Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
            cropImageUri(sharedpreferencesUtil.getImageTempNameUri(), outoutUri, outputX, outputY, CROP_BIG_PICTURE);
        }


        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                String imgURI = (String) result.getUri().getPath();//.getStringExtra("URI");
                onCropImgSuccess(imgURI);
//                ((ImageView) findViewById(R.id.quick_start_cropped_image)).setImageURI(result.getUri());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }

        }

        if (requestCode == CROP_BIG_PICTURE && resultCode == Activity.RESULT_OK) {
            if (sharedpreferencesUtil.getImageTempNameUri() != null) {
                Uri result = sharedpreferencesUtil.getImageTempNameUriCroped();
                App.firstRun();
                onCropImgSuccess(result.getPath());
            }
        }


    }

    private void onCropImgSuccess(String path) {
        LogUtil.i(App.TAG, "crop url:" + path);
        imageViewCover.setTag(path);
        Picasso.with(this).load(new File(path)).config(Bitmap.Config.RGB_565).placeholder(R.drawable.bg_mylive_set_cover).into(imageViewCover);
        uploadSceen(this, path, new OnUploadCallBack() {
            @Override
            public void onUploadEnd(NetResult netResult, Context context) {
                if (null != netResult) {
                    if (netResult.isOk()) {

                        imgUploadUrl = (String) netResult.getData()[0];
                        User user = App.getApp().getUser();

                        if (null != user && null != user.getRoom()) {
                            user.getRoom().setImgUrl(imgUploadUrl);
                            App.getApp().setUser(user);
                        }

                        LogUtil.i(App.TAG, "upload url:" + imgUploadUrl);
                        Tool.ToastShow(StartLiveActivity.this, "upload success!");

                    } else {
                        Tool.showMessageDialog(netResult.getMessage(), StartLiveActivity.this);
                    }
                } else {
                    Tool.showMessageDialog(R.string.error_net, StartLiveActivity.this);
                }
            }
        });

    }

}
