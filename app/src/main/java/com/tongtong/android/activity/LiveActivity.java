package com.tongtong.android.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.common.BaseActivity;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.LogUtil;
import com.common.util.SharePersistent;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.sina.weibo.sdk.api.share.BaseResponse;
import com.sina.weibo.sdk.api.share.IWeiboHandler;
import com.sina.weibo.sdk.api.share.IWeiboShareAPI;
import com.sina.weibo.sdk.api.share.WeiboShareSDK;
import com.sina.weibo.sdk.constant.WBConstants;
import com.squareup.picasso.Picasso;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.mm.sdk.platformtools.Util;
import com.tongtong.android.App;
import com.tongtong.android.R;
import com.tongtong.android.adapter.MessageAdapter;
import com.tongtong.android.adapter.OnlineAdapter;
import com.tongtong.android.domain.LiveType;
import com.tongtong.android.domain.Message;
import com.tongtong.android.domain.Profile;
import com.tongtong.android.domain.Room;
import com.tongtong.android.domain.Subscribe;
import com.tongtong.android.domain.User;
import com.tongtong.android.json.JsonHelper;
import com.tongtong.android.net.NetInterface;
import com.tongtong.android.net.WSRequester;
import com.tongtong.android.utils.NetworkUtil;
import com.tongtong.android.view.BlockDialog;
import com.tongtong.android.view.CircleImageView;
import com.tongtong.android.view.EndLiveDialog;
import com.tongtong.android.view.MyLiveDialog;
import com.tongtong.android.view.ShareDialog;
import com.tongtong.android.view.PopularityView;
import com.wowza.gocoder.sdk.api.WowzaGoCoder;
import com.wowza.gocoder.sdk.api.configuration.WowzaConfig;
import com.wowza.gocoder.sdk.api.devices.WZCamera;
import com.wowza.gocoder.sdk.api.devices.WZCameraView;
import com.wowza.gocoder.sdk.api.errors.WZError;
import com.wowza.gocoder.sdk.api.errors.WZStreamingError;
import com.wowza.gocoder.sdk.api.geometry.WZSize;
import com.wowza.gocoder.sdk.api.status.WZState;
import com.wowza.gocoder.sdk.api.status.WZStatus;
import com.wowza.gocoder.sdk.api.status.WZStatusCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import static com.tongtong.android.activity.BaseTongTongActivity.showShareDialog;


/**
 * Live Activity
 */
public class LiveActivity extends BaseActivity implements WZStatusCallback, IWeiboHandler.Response{

    private WowzaGoCoder goCoder;
    private WZCameraView goCoderCameraView;

    @InjectView(R.id.textViewUserCount)
    TextView textViewUserCount;

    @InjectView(R.id.textViewFirst)
    TextView textViewFirst;

    @InjectView(R.id.textViewSecond)
    TextView textViewSecond;

    @InjectView(R.id.textViewThird)
    TextView textViewThird;

    @InjectView(R.id.rl_reconnect)
    RelativeLayout relativeLayoutreconnect;

    @InjectView(R.id.rl_connecting)
    RelativeLayout reLativeconnecting;


    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private LayoutInflater mInflater;
    private List<String> mTitleList = new ArrayList<>();
    private View chatTabView, onlineUsertabView, liveBroadCastView;
    private List<View> mViewList = new ArrayList<>();

    private MyLiveDialog dialogLiveWait;

    private ListView listViewChat;
    private ListView listViewOnLine;
    private ListView listViewLiveNotify;

    private JSONObject jsonObjectRoomInfo;
    private JSONObject jsonObjectTextInfo;

    private EditText editTextMessage;
    private WSRequester wsRequester;
    private TextView textViewSend;

    private String keyBroad = "broad";
    private String keybbraod = "kbroad";
    private ArrayList<Message> arrayListMesssage;
    private MessageAdapter messageAdapter;
    private OnlineAdapter onlineAdapter;
    private ArrayList<User> arrayListOnlineUsers;

    private final int WZ_STATUS_ERROR = 101;
    private final int NETWORK_FAIL = 102;

    private int count = 0;

    private ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();

    @InjectView(R.id.linearLayoutChatBg)
    LinearLayout linearLayoutChatBg;

    @InjectView(R.id.linearLayoutChatBox)
    LinearLayout linearLayoutChatBox;

    @InjectView(R.id.linearLayoutChatList)
    LinearLayout linearLayoutChatList;

    @InjectView(R.id.imageViewUpDown)
    ImageView imageViewUpDown;

    @InjectView(R.id.textViewUpDown)
    TextView textViewUpDown;

    @InjectView(R.id.imageViewShare)
    ImageButton imageViewShare;


    @InjectView(R.id.cameraViewContainer)
    RelativeLayout cameraViewContainer;


    @InjectView(R.id.viewAncher)
    View viewAncher;

    @InjectView(R.id.likeview_like)
    PopularityView likeview_like;

    @OnClick(R.id.likeview_like)
    public void like() {
        if (wsRequester != null) {
            boolean result = wsRequester.like();
        }
    }

//    @InjectView(R.id.lienarlayoutCameraContainer)
//    LinearLayout lienarlayoutCameraContainer;


    boolean isCloseClick = false;

    WZSize wzSize;

    private IWXAPI api;
    private IWeiboShareAPI weiboApi;
    private Room room;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LogUtil.v("leo", "oncreate");
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_live);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        ButterKnife.inject(this);

        arrayListMesssage = new ArrayList<>();
        messageAdapter = new MessageAdapter(this, arrayListMesssage);

        arrayListOnlineUsers = new ArrayList<>();
        onlineAdapter = new OnlineAdapter(this, arrayListOnlineUsers);

        onlineAdapter.setOnClickSpeechAdapterListener(new OnlineAdapter.onClickSpeechAdapterListener() {
            @Override
            public void onClickSpeech(final User user, OnlineAdapter onlineAdapter, final boolean isInblack) {
                BlockDialog blockDialog = new BlockDialog(LiveActivity.this, user) {
                    @Override
                    protected void onBlock() {
                        blackList(user.getUserId(), isInblack, user.getBlack(), user);
                    }
                };
                if (!isInblack) {
                    blockDialog.show();
                } else {
                    blackList(user.getUserId(), isInblack, user.getBlack(), user);
                }
            }
        });


        jsonObjectRoomInfo = (JSONObject) App.getApp().getTempObject("info");
        jsonObjectTextInfo = (JSONObject) App.getApp().getTempObject("binfo");

        if (null != jsonObjectTextInfo && null != jsonObjectTextInfo) {

            String broadCastObjcetString = jsonObjectRoomInfo.toString();
            String kbroadCastObjcetString = jsonObjectTextInfo.toString();

            SharePersistent.savePreferenceEn(this, keyBroad, broadCastObjcetString);
            SharePersistent.savePreferenceEn(this, keybbraod, kbroadCastObjcetString);

            LogUtil.i(App.TAG, "save success info");

        }

        //==========================================================================================

//        linearLayoutChatBg.setVisibility(View.INVISIBLE);

        setUpCamera();
        initBelowContent();

//        handler.sendEmptyMessage(WZState.RUNNING);

        singleThreadExecutor.execute(new Runnable() {
            @Override
            public void run() {
                Tool.delay(1000);//
                startLiveBroadcast();
            }
        });


        api = WXAPIFactory.createWXAPI(this, getResources().getString(R.string.app_id));
        weiboApi = WeiboShareSDK.createWeiboAPI(this, getResources().getString(R.string.weibo_api));
        weiboApi.registerApp();
    }


    @OnClick(R.id.rl_reconnect)
    public void reconnect() {
        showNotifyTextIn5Seconds("reconnect");
        startLiveBroadcast();
    }

    private void setUpCamera() {
        // Initialize the GoCoder SDK
        //GOSK-AA42-0103-2586-D52E-5DAF
        this.goCoder = WowzaGoCoder.init(getApplicationContext(), "GOSK-B642-0100-1121-5F1B-0F9B");
//        this.goCoder = WowzaGoCoder.init(getApplicationContext(), "GOSK-AA42-0103-2586-D52E-5DAF");

        if (this.goCoder == null) {
            WZError goCoderInitError = WowzaGoCoder.getLastError();
            LogUtil.e(App.TAG, "GoCoder SDK error: " + goCoderInitError.getErrorDescription());
            return;
        }

        // Set the camera view
        this.goCoderCameraView = (WZCameraView) findViewById(R.id.camera_preview);
        this.goCoder.setCameraView(this.goCoderCameraView);
    }



    BaseTask loadImgtask;

    @OnClick(R.id.imageViewShare)
    public void onShareClick() {
        LogUtil.i(App.TAG, "select room share...");
        if (null == room) {
            room = (Room) SharePersistent.getObjectValue(this, "live_room");
        }

        if (null != loadImgtask && loadImgtask.getStatus() == AsyncTask.Status.RUNNING) {
            loadImgtask.cancel(true);
        }

        loadImgtask = new BaseTask(LiveActivity.this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = new NetResult();
                Bitmap bitmap = null;
                String roomImgUrl = room.getImgUrl();
                int THUMB_SIZE = 100;

                if (!StringUtils.isEmpty(roomImgUrl)) {
                    try {
                        Bitmap bmp = BitmapFactory.decodeStream(new URL(roomImgUrl).openStream());
                        bitmap = Bitmap.createScaledBitmap(bmp, THUMB_SIZE, THUMB_SIZE, true);
                        bmp.recycle();
                    } catch (Exception e) {
                    }
                } else {
                    Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.icon_weshre);
                    bitmap = Bitmap.createScaledBitmap(bmp, THUMB_SIZE, THUMB_SIZE, true);
                    bmp.recycle();
                }

                if (null != bitmap) {
                    byte[] data = Util.bmpToByteArray(bitmap, true);
                    netResult.setData(new Object[]{data});
                    return netResult;
                } else {
                    return null;
                }

            }

            @Override
            public void onFinish(final NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                LogUtil.i(App.TAG, "finish--->");
                if (null != result) {
                    LogUtil.i(App.TAG, "result-->");
                    showShareDialog(LiveActivity.this, room, new ShareDialog.OnClickShare2Listener() {
                        @Override
                        public void share2TimeLine() {

                        }

                        @Override
                        public void share2Session() {

                        }

                        @Override
                        public void share2Weibo() {
                            LogUtil.i(App.TAG, "Share2Weibo");

                        }
                    }, api, weiboApi, (byte[]) result.getData()[0]);
                } else {
                    showShareDialog(LiveActivity.this, room, new ShareDialog.OnClickShare2Listener() {
                        @Override
                        public void share2TimeLine() {

                        }

                        @Override
                        public void share2Session() {

                        }

                        @Override
                        public void share2Weibo() {
                            LogUtil.i(App.TAG, "Share2Weibo");
                        }
                    }, api, weiboApi, null);
                }

            }

        });

        loadImgtask.execute(new HashMap<String, String>());


    }


    private void initBelowContent() {

        //==================message adater=========================================

        mViewPager = (ViewPager) findViewById(R.id.my_view_pager);
        mTabLayout = (TabLayout) findViewById(R.id.my_tabs);

        mInflater = LayoutInflater.from(this);
        chatTabView = mInflater.inflate(R.layout.page_chat_view, null);
        listViewChat = (ListView) chatTabView.findViewById(R.id.listView);
        listViewChat.setAdapter(messageAdapter);


        onlineUsertabView = mInflater.inflate(R.layout.page_online, null);
        listViewOnLine = (ListView) onlineUsertabView.findViewById(R.id.listView);
        listViewOnLine.setAdapter(onlineAdapter);

        //======================================

        liveBroadCastView = mInflater.inflate(R.layout.page_blackboard, null);


        editTextMessage = (EditText) chatTabView.findViewById(R.id.editTextMessage);
        textViewSend = (TextView) chatTabView.findViewById(R.id.textViewSend);
        textViewSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LogUtil.i(App.TAG, "send msg:" + editTextMessage.getText().toString());

                String msgContent = editTextMessage.getText().toString();
                msgContent = StringUtils.isEmpty(msgContent) ? "" : msgContent;


                if (null != wsRequester && !StringUtils.isEmpty(msgContent)) {
                    boolean result = wsRequester.like();
                    LogUtil.v("leo", "---------------------------------" + result);
//                    boolean result = wsRequester.message(App.getApp().getUser(), editTextMessage.getText().toString());
//                    if (result) {
////                        {"time":"2016-08-16 08:01:00","message":"\u8bf4\u70b9\u5565","command":"message","userid":1,"nickname":"baozi"}
//
//                        User user = App.getApp().getUser();
//
//                        Message message = new Message();
//
//                        message.setMessage(editTextMessage.getText().toString());
//                        message.setTime(Constant.SDFD4.format(new Date()));
//                        message.setUserid(user.getUserId());
//                        message.setNickname(user.getNickName());
//                        message.setAvatar(user.getAvatar());
//
//                        arrayListMesssage.add(message);
//                        messageAdapter.notifyDataSetChanged();
//
//                        Tool.setListViewHeightBasedOnChildren(listViewChat);
//
//                        editTextMessage.setText("");
//
//                        listViewChat.post(new Runnable() {
//                            @Override
//                            public void run() {
//                                listViewChat.setSelection(messageAdapter.getCount() - 1);
//                            }
//                        });
//
//                    }
                } else {
                    LogUtil.e(App.TAG, "ws lost connect or empty msg");
                }

            }
        });


        //===========================================
        CircleImageView roundImageViewProfile = (CircleImageView) liveBroadCastView.findViewById(R.id.roundImageViewProfile);

        User currentUser = App.getApp().getUser();

        if (null != currentUser && !StringUtils.isEmpty(currentUser.getAvatar())) {
            Picasso.with(App.getApp()).load(currentUser.getAvatar()).resize(150, 150).config(Bitmap.Config.RGB_565).into(roundImageViewProfile);
        }


        TextView textViewBroadcastTitle = (TextView) liveBroadCastView.findViewById(R.id.textViewBroadcastTitle);
        TextView textViewComments = (TextView) liveBroadCastView.findViewById(R.id.textViewComments);

        Profile currentProfile = App.getApp().getProfile();
        String title = "";
        String comments = "";
        try {
            title = jsonObjectTextInfo.getString("title");
            comments = jsonObjectTextInfo.getString("comments");
        } catch (Exception e) {
            LogUtil.e(App.TAG, "error:" + e.getLocalizedMessage());
        }

        if (title.equals("") && comments.equals("")) {
            ((TextView) liveBroadCastView.findViewById(R.id.textViewBroadcastTitle)).setText(currentProfile.getLivTopic());
            ((TextView) liveBroadCastView.findViewById(R.id.textViewComments)).setText(currentProfile.getDescription());
        } else {
            textViewBroadcastTitle.setText(title);
            textViewComments.setText(comments);
        }

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd HH:ss");
            ((TextView) liveBroadCastView.findViewById(R.id.textViewTime)).setText("时间:" + sdf.format(System.currentTimeMillis()));
        } catch (Exception e) {
        }

        ((TextView) liveBroadCastView.findViewById(R.id.textViewLiveType)).setText("类型:" + LiveType.getTypeTextById(currentProfile.getLiveType()));

        if (null != currentUser.getRoom()) {
            ((TextView) liveBroadCastView.findViewById(R.id.textViewRoomAddress)).setText("房间地址:" + currentUser.getRoom().getAppUrl());
        }

        //===================================

        mViewList.add(chatTabView);
        mViewList.add(onlineUsertabView);
        mViewList.add(liveBroadCastView);

        mTitleList.add("聊天");
        mTitleList.add("在线");
        mTitleList.add("直播公告");

        mTabLayout.setTabMode(TabLayout.MODE_FIXED);

        mTabLayout.addTab(mTabLayout.newTab().setText(mTitleList.get(0)));
        mTabLayout.addTab(mTabLayout.newTab().setText(mTitleList.get(1)));
        mTabLayout.addTab(mTabLayout.newTab().setText(mTitleList.get(2)));

        MyPagerAdapter pagerAdapter = new MyPagerAdapter(mViewList);

        mViewPager.setAdapter(pagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int pos = tab.getPosition();
                if (pos == 1) {
                    refreshRooms();
                }
                mViewPager.setCurrentItem(pos);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                int pos = tab.getPosition();
                if (pos == 1) {
                    refreshRooms();
                }
            }
        });


    }


    private void refreshRooms() {

        if (null != wsRequester) {
            wsRequester.roomusers();
            LogUtil.i(App.TAG, "refresh rooms");
        }

    }


    private String trailerId;
    private Handler handler = new Handler() {

        @Override
        public void handleMessage(android.os.Message msg) {
            int what = msg.what;
            switch (what) {
                case WZState.STARTING: {
                    isCloseClick = false;
                    reLativeconnecting.setVisibility(View.VISIBLE);
                }
                break;
                case WZState.RUNNING: {
                    try {
                        relativeLayoutreconnect.setVisibility(View.GONE);
                        reLativeconnecting.setVisibility(View.GONE);
                        requestStartEndLive(JsonHelper.getStringFroJso("title", jsonObjectTextInfo),
                                JsonHelper.getStringFroJso("comments", jsonObjectTextInfo), "1",
                                JsonHelper.getStringFroJso("img", jsonObjectTextInfo));
                        LogUtil.i(App.TAG, "准备开始直播,调用startendlive api");
                    } catch (Exception e) {
                        LogUtil.e(App.TAG, "request start live error:" + e.getLocalizedMessage());
                    }
                }
                break;
                case WZState.STOPPING: {

                    //unshow this onley after close button click
                    if (isCloseClick) {

                        relativeLayoutreconnect.setVisibility(View.INVISIBLE);

                        try {
                            requestStartEndLive(
                                    JsonHelper.getStringFroJso("title", jsonObjectTextInfo),
                                    JsonHelper.getStringFroJso("comments", jsonObjectTextInfo), "0",
                                    JsonHelper.getStringFroJso("img", jsonObjectTextInfo));
                        } catch (Exception e) {
                        }

                    } else {
                        relativeLayoutreconnect.setVisibility(View.VISIBLE);
                    }

                    reLativeconnecting.setVisibility(View.GONE);
                }
                break;
//                case WZ_STATUS_ERROR:
//                    WZStatus wz = (WZStatus) msg.obj;
//                    LogUtil.i(App.TAG, "直播callback错误:" + wz.getLastError().getErrorDescription());
//                    if (!NetworkUtil.isNetWorkEnable(LiveActivity.this)) {
//                        backResultWithMessage("网络中断,请检查网络。");
//                    } else {
//                        backResultWithMessage("直播错误,请重新开始直播。");
//                    }
//                    break;

                case NETWORK_FAIL:
                    showNotifyTextIn5Seconds("Network is invalid");
                    relativeLayoutreconnect.setVisibility(View.VISIBLE);
                    reLativeconnecting.setVisibility(View.GONE);
            }

        }
    };


//    BaseTask baseTask;

    private void requestStartEndLive(final String title, final String contents, final String isactive, final String imgUrl) {
        if (null != baseTask && baseTask.getStatus() == AsyncTask.Status.RUNNING) {
            baseTask.cancel(true);
        }

        baseTask = new BaseTask(this, new NetCallBack() {

            @Override
            public void onCanCell() {
                super.onCanCell();
                LogUtil.i(App.TAG, "------------------------baseTask.cancel");
            }

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {


                NetResult netResult = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("title", title);
                    jsonObject.put("contents", contents);
                    jsonObject.put("active", isactive);
                    jsonObject.put("img_url", imgUrl);
                    jsonObject.put("trailerid", trailerId);
                    netResult = NetInterface.startEndLive(LiveActivity.this, jsonObject);
                } catch (Exception e) {
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                LogUtil.i(App.TAG, "-------------------------finish()!!!!!!");
                baseTask.hideDialogSelf();
                if (null != result) {
                    if (result.isOk()) {
                        LogUtil.i(App.TAG, "request start end live success ");
                        if ("0".equals(isactive)) {

                            if (null != wsRequester) {
                                wsRequester.disconencted();
                            }
                            finish();
                        } else if ("1".equals(isactive)) {
                            try {
                                JSONObject resultData = (JSONObject) result.getData()[0];
                                JSONObject myData =  resultData.getJSONObject("data");
                                room = JsonHelper.parseRoomObject(myData.getJSONObject("user").getJSONObject("room"));
                                trailerId = myData.getString("id");
                                LogUtil.i("TONGTONG", room.getImgUrl());
                                SharePersistent.setObjectValue(LiveActivity.this, "live_room", room);
                            } catch (Exception e) {
                                LogUtil.i(App.TAG, " start live error:" + e.getLocalizedMessage());
                            }

                            boolean issuccess = initWsrequester();
                            if (issuccess) {
                                LogUtil.i(App.TAG, "init send msg ws success");
                            } else {
                                LogUtil.e(App.TAG, "init send msg ws fail");
                            }
                        }
                    } else {

//                      Tool.showMessageDialog(result.getMessage(), LiveActivity.this);
                        LogUtil.i(App.TAG, "request start end live success erroe ");

//                        backResultWithMessage(result.getMessage());

                        if ("1".equals(isactive)) {
                            relativeLayoutreconnect.setVisibility(View.VISIBLE);
                        } else {
                            finish();
                        }


                    }
                } else {
//                    Tool.showMessageDialog(R.string.error_net, LiveActivity.this);
//                    backResultWithMessage(getResources().getString(R.string.error_net));
                    if ("1".equals(isactive)) {
                        relativeLayoutreconnect.setVisibility(View.VISIBLE);
                    } else {
                        finish();
                    }

                }

            }
        });
        baseTask.execute(new HashMap<String, String>());

    }


    public void backResultWithMessage(String msg) {
        Intent intent = new Intent();
        intent.putExtra("msg", msg);
        setResult(RESULT_CANCELED, intent);
        finish();
    }


    private boolean initWsrequester() {
        try {
            wsRequester = WSRequester.getInstance(new WSRequester.ConnectionListener() {

                @Override
                public void onOpen() {

                    textViewSend.setEnabled(true);
                    String roomId = null;
                    try {
                        roomId = jsonObjectRoomInfo.getJSONObject("data").getString("room_id");
                        wsRequester.enter(App.getApp().getUser(), roomId);
                        LogUtil.i(App.TAG, "enter room id:" + roomId);
                        wsRequester.roomusers();

                    } catch (JSONException e) {
                        LogUtil.e(App.TAG, "enter room error");
                    }

                }

                @Override
                public void onLike(JSONObject jsonObject) {
                    LogUtil.v("leo", "like----------------------------" + jsonObject.toString());
                    try {
                        int likeNum = jsonObject.getInt("like");
                        likeview_like.setCountWithAnim(likeNum);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onReceiverMessage(Message message) {
                    LogUtil.v("leo", "message----------------------------" + message.toString());
                    messageAdapter.addMsg(message);
                    Tool.setListViewHeightBasedOnChildren(listViewChat);

                    listViewChat.post(new Runnable() {
                        @Override
                        public void run() {

                            listViewChat.setSelection(messageAdapter.getCount() - 1);
                        }
                    });

                    count++;

                    if (count < 3) {
                        textViewFirst.setText(textViewSecond.getText());
                        textViewSecond.setText(textViewThird.getText());
                        textViewThird.setText(Html.fromHtml("<font color='#4A90E2'>" + message.getNickname() + ":&nbsp;</font>" + "<font color='#ffffff'>" + message.getMessage() + "</font>"));
                    } else {
                        textViewFirst.setText(textViewSecond.getText());
                        textViewSecond.setText(textViewThird.getText());
                        textViewThird.setText(Html.fromHtml("<font color='#4A90E2'>" + message.getNickname() + ":&nbsp;</font>" + "<font color='#ffffff'>" + message.getMessage() + "</font>"));
                        count = 0;
                    }


                }

                @Override
                public void onRoomUsers(ArrayList<User> users) {

                    onlineAdapter.addAllNewUser(users);
                    Tool.setListViewHeightBasedOnChildren(listViewOnLine);
                }

                @Override
                public void onSubScrible(Subscribe subscribe) {
                    if (subscribe == null) {
                        return;
                    }
                    wsRequester.roomusers();
                    textViewUserCount.setText(subscribe.getCount() + getResources().getString(R.string.live_user_counts));
                    likeview_like.setCount(subscribe.getLike());
                    LogUtil.v("leo", "=============like" + subscribe.getLike());
                    count++;

                    if (count < 3) {
                        textViewFirst.setText(textViewSecond.getText());
                        textViewSecond.setText(textViewThird.getText());
                        textViewThird.setText(Html.fromHtml("<font color='#ffffff'>欢迎</font> <font color='#F5A623'>" + subscribe.getUser().getNickName() + "</font><font color='#ffffff'>来到直播间<font>"));
                    } else {
                        textViewFirst.setText(textViewSecond.getText());
                        textViewSecond.setText(textViewThird.getText());
                        textViewThird.setText(Html.fromHtml("<font color='#ffffff'>欢迎</font><font color='#F5A623'>" + subscribe.getUser().getNickName() + "</font><font color='#ffffff'>来到直播间</font>"));
                        count = 0;
                    }

                }
                @Override
                public void onUnSubScrible(Subscribe subscribe){
                    if(subscribe == null){
                        return;
                    }
                    textViewUserCount.setText(subscribe.getCount() + getResources().getString(R.string.live_user_counts));

                }



                @Override
                public void onClose(int code, String reason) {
                    textViewSend.setEnabled(false);
                }

            });

            return true;
        } catch (Exception e) {
            LogUtil.e(App.TAG, "init ws fail:" + e.getLocalizedMessage());
        }

        return false;

    }


    @OnClick(R.id.imageButtonClose)
    public void onCloseClick() {
        isCloseClick = true;

        EndLiveDialog dialog = new EndLiveDialog(LiveActivity.this);
        dialog.setOnConfirmDialogListener(new EndLiveDialog.OnConfirmDialogListener() {
            @Override
            public void OnConfirmClick() {
                stopLiveBroadcast();
            }

            @Override
            public void OncancelClick() {
            }
        });
        dialog.show();

    }

    @OnClick(R.id.linearLayoutChatBg)
    public void onChatBgClick() {
        if (textViewUpDown.getText().toString().equals("收起")) {
            textViewUpDown.setText("展开");
            imageViewUpDown.setImageResource(R.drawable.icon_down_arrow);
            linearLayoutChatList.setVisibility(View.INVISIBLE);
        } else {
            textViewUpDown.setText("收起");
            imageViewUpDown.setImageResource(R.drawable.icon_up_arrow);
            linearLayoutChatList.setVisibility(View.VISIBLE);
        }
    }

    private void stopLiveBroadcast() {
        LogUtil.i(App.TAG, "------------stopLiveBroadcast");
        if (!NetworkUtil.isNetWorkEnable(this)) {
            finish();
            return;
        }
        if (null == goCoder) {
            finish();
            return;
        } else {
            stopLive();
            finish();
        }

    }


    private void stopLive() {
        LogUtil.i(App.TAG, "------------stop");
        WZStreamingError configValidationError = goCoder.getConfig().validateForBroadcast();
        if (configValidationError != null) {
            finish();
//                Toast.makeText(this, configValidationError.getErrorDescription(), Toast.LENGTH_LONG).show();
        } else {
            long start = System.currentTimeMillis();
            if (goCoder.isStreaming()) {
                LiveActivity.this.goCoder.endStreaming(LiveActivity.this);//ANR
                LogUtil.i(App.TAG, "stop live");
            }
            long end = System.currentTimeMillis();
            LogUtil.i(App.TAG, "cost time:" + (end - start));

        }
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    }

    @OnClick(R.id.imageButtonSwitchCamera)
    public void onswitchCamera() {
        if (null != goCoderCameraView) {
            goCoderCameraView.switchCamera();
        }
    }

//    @InjectView(R.id.ib_like4anim)
//    public ImageButton ib_like4anim;
//
//    @InjectView(R.id.ib_like)
//    public ImageButton ib_like;
//
//    @InjectView(R.id.tv_like)
//    public TextView tv_like;
//    private int countLike = 0;
//
//    @OnClick(R.id.ib_like)
//    public void onClickLike() {
//        countLike++;
//        Animation slideInBot = AnimationUtils.loadAnimation(App.getApp().getBaseContext(), R.anim.like);
//        AnimationSet anim = new AnimationSet(false);
//        float x = (float) (Math.random() * 2.0f - 1.0f);
//        Animation tran = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
//                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, -2f);
//        tran.setDuration(600);
//        Animation alpha = new AlphaAnimation(1.0f, 0.1f);
//        alpha.setDuration(600);
//        Animation scale = new ScaleAnimation(1.0f, 2.0f, 1.0f, 2.0f,
//                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//        scale.setDuration(600);
//        anim.addAnimation(tran);
//        anim.addAnimation(alpha);
//        anim.addAnimation(scale);
//        ib_like4anim.startAnimation(anim);
//        anim.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation animation) {
//                ib_like.setClickable(false);
//            }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
//                ib_like.setClickable(true);
//                tv_like.setText(countLike + "");
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//
//            }
//        });
//    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        LogUtil.v("leo", "oncreate");
        ajustLiveView();


    }


    public void ajustLiveView() {
        int value = Tool.getScreenOrientation(this);
        if (value == Configuration.ORIENTATION_LANDSCAPE) {
            screenLandscapeModeUI();
        } else if (value == Configuration.ORIENTATION_PORTRAIT) {
            screenPortraitModeUI();
        }
    }


    private void screenPortraitModeUI() {


//        if (this.goCoderCameraView != null) {
        wzSize = this.goCoderCameraView.setFrameSize(1280, 720);
//        } else {
//            wzSize = new WZSize(640, 480);
//        }

        WZSize[] supportSizes = this.goCoderCameraView.getCamera().getSupportedFrameSizes();

//        LogUtil.i(App.TAG, "support size:" + supportSizes.length); // null pointer

        LogUtil.i(App.TAG, "ac size:" + wzSize.width + "," + wzSize.height);


        Point p = Tool.getDisplayMetrics(this);

        int h = (int) ((p.x * 1.0f / wzSize.width) * wzSize.height);
        int nx = p.x, nh = h;

        int delta = nx - nh + nx / 3;


        LogUtil.i(App.TAG, "nx:" + nx + " ny:" + nh);

        RelativeLayout.LayoutParams rpp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        rpp.topMargin = -delta / 2;
        cameraViewContainer.setLayoutParams(rpp);

        RelativeLayout.LayoutParams lpp = (RelativeLayout.LayoutParams) viewAncher.getLayoutParams();
        lpp.topMargin = -delta / 2;
        viewAncher.setLayoutParams(lpp);


        linearLayoutChatBox.setVisibility(View.INVISIBLE);


    }

    private void screenLandscapeModeUI() {

        wzSize = this.goCoderCameraView.setFrameSize(1280, 768);

        LogUtil.i(App.TAG, "ac size:" + wzSize.width + "," + wzSize.height);


        RelativeLayout.LayoutParams lpp = (RelativeLayout.LayoutParams) viewAncher.getLayoutParams();
        lpp.topMargin = 0;
        viewAncher.setLayoutParams(lpp);

        Point p = Tool.getDisplayMetrics(this);

        RelativeLayout.LayoutParams rpp = new RelativeLayout.LayoutParams(p.x, p.y);
        cameraViewContainer.setLayoutParams(rpp);

        linearLayoutChatBox.setVisibility(View.VISIBLE);

    }


    @OnClick(R.id.imageButtonTogleScreen)
    public void onTogleScrean() {
        if (Tool.getScreenOrientation(this) == Configuration.ORIENTATION_LANDSCAPE) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else if (Tool.getScreenOrientation(this) == Configuration.ORIENTATION_PORTRAIT) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }


    private void initCamerainfo() {

        try {
            // Specify the broadcast configuration parameters
            WowzaConfig broadcastConfig = this.goCoder.getConfig();


            String hostAddress = "52.63.94.182";
            String applicationName = "himate";

//            String hostAddress = "52.63.94.182";
            int portNumber = 1935;
//            String applicationName = "himate";

            JSONObject dataObject = jsonObjectRoomInfo.getJSONObject("data");


            String username = dataObject.getString("username");
            String streamName = username;
            String password = dataObject.getString("password");

            LogUtil.e(App.TAG, "woaza:" + username + ":" + password);


            broadcastConfig.setHostAddress(hostAddress);
            broadcastConfig.setPortNumber(portNumber);
            broadcastConfig.setApplicationName(applicationName);
            broadcastConfig.setStreamName(streamName);
            broadcastConfig.setUsername(username);
            broadcastConfig.setPassword(password);

//
//            broadcastConfig.setVideoFramerate(30);
//            broadcastConfig.setVideoKeyFrameInterval(60);
//            broadcastConfig.setVideoBitRate(1500);
//
//            broadcastConfig.setAudioSampleRate(41000);
//            broadcastConfig.setAudioBitRate(64000);
//
//
//            broadcastConfig.setVideoFramerate(15);
//            broadcastConfig.setAudioSampleRate(22100);
//            broadcastConfig.setAudioBitRate(64000);


//            int SCALE_MODES[] = {
//                    WZBroadcastConfig.FILL_VIEW,
//                    WZBroadcastConfig.RESIZE_TO_ASPECT
//            };

//            broadcastConfig.set(WZMediaConfig.FRAME_SIZE_1920x1080);

//            this.goCoderCameraView.setScaleMode(SCALE_MODES[0]);

//            wzSize = this.goCoderCameraView.setFrameSize(1280, 720);

//            LogUtil.e(App.TAG, "====realsize:" + wzSize.toString());

            this.goCoder.setConfig(broadcastConfig);


        } catch (Exception e) {
            LogUtil.e(App.TAG, "init camera error:" + e.getLocalizedMessage());
        }

        ajustLiveView();

    }


    @Override
    protected void onResume() {
        LogUtil.v("leo", "onresume");
        super.onResume();
        initCamerainfo();

//        handler.sendEmptyMessage(WZState.RUNNING);

        singleThreadExecutor.execute(new Runnable() {
            @Override
            public void run() {
                Tool.delay(1000);//
                startLiveBroadcast();
            }
        });


        api = WXAPIFactory.createWXAPI(this, getResources().getString(R.string.app_id));
        if (null == jsonObjectTextInfo || null == jsonObjectRoomInfo) {
            try {
                jsonObjectTextInfo = new JSONObject(SharePersistent.getPreferenceDe(this, keyBroad));
                jsonObjectRoomInfo = new JSONObject(SharePersistent.getPreferenceDe(this, keybbraod));

                LogUtil.i(App.TAG, "load broadcast into succes");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (this.goCoder != null) {
//            this.goCoder.startCameraPreview();
            goCoderCameraView.startPreview();

            WZCamera activeCamera = this.goCoderCameraView.getCamera();
            if (activeCamera != null && activeCamera.hasCapability(WZCamera.FOCUS_MODE_CONTINUOUS)) {
                activeCamera.setFocusMode(WZCamera.FOCUS_MODE_CONTINUOUS);
            }
        }
    }

    @Override
    protected void onPause() {
        LogUtil.v("leo", "onpause");
        super.onPause();
        if (null != this.goCoder && null != goCoderCameraView) {
            this.goCoderCameraView.stopPreview();
        }

    }

    private void startLiveBroadcast() {
        if (null == goCoder) {
            return;
        }

        WZStreamingError configValidationError = goCoder.getConfig().validateForBroadcast();
        if (configValidationError != null) {
            LogUtil.e(App.TAG, "xx:" + configValidationError.getErrorDescription());
//            Toast.makeText(this, configValidationError.getErrorDescription(), Toast.LENGTH_LONG).show();
        } else {
            if (!goCoder.isStreaming())
                this.goCoder.startStreaming(this);
        }
    }

    @Override
    public void onBackPressed() {
        if (BaseTongTongActivity.hideShareDialog() == true) {

            return;
        }

        EndLiveDialog dialog = new EndLiveDialog(LiveActivity.this);
        dialog.setOnConfirmDialogListener(new EndLiveDialog.OnConfirmDialogListener() {
            @Override
            public void OnConfirmClick() {
                stopLiveBroadcast();
            }

            @Override
            public void OncancelClick() {
            }
        });
        dialog.show();
    }

//    private void back() {
//        if (null != this.goCoder && this.goCoder.isStreaming()) {
//            requestStartEndLive("", "", "0", "");//
//            return;
//        } else {
//            if (null != this.goCoder) {
//                this.goCoder.stopCameraPreview();
//            }
//
//            finish();
//        }
//    }

    @Override
    public void onWZStatus(WZStatus wzStatus) {

        final StringBuffer statusMessage = new StringBuffer("Broadcast status: ");

        int status = goCoder.getStatus().getState();
        switch (status) {

            case WZState.STARTING:
                statusMessage.append("Broadcast initialization");
                handler.sendEmptyMessage(WZState.STARTING);
                break;

            case WZState.READY:
                statusMessage.append("Ready to begin streaming");

                break;

            case WZState.RUNNING:
                statusMessage.append("Streaming is active");
                handler.sendEmptyMessage(WZState.RUNNING);
                break;

            case WZState.STOPPING:

                if (NetworkUtil.isNetWorkEnable(this)) {
                    statusMessage.append("Broadcast shutting down");
                    handler.sendEmptyMessage(WZState.STOPPING);
                } else {
                    handler.sendEmptyMessage(NETWORK_FAIL);

                }
                break;

            case WZState.IDLE:
                statusMessage.append("The broadcast is stopped");
                break;

            default:
                return;
        }

        LogUtil.e(App.TAG, "onwzstatus:" + statusMessage);

    }

    @Override
    public void onWZError(WZStatus wzStatus) {

//        android.os.Message msg = new android.os.Message();
//        msg.obj = wzStatus;
//        msg.what = wzStatus.getState();
//        handler.sendMessage(msg);

        String errorDescription = wzStatus.getLastError().getErrorDescription();
        LogUtil.e(App.TAG, "error desc:" + errorDescription);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LogUtil.v("leo", "ondestroy");
        singleThreadExecutor.shutdown();
    }


    BaseTask blackTask = null;

    public void blackList(final String userid, final boolean isalowSpeech, final String blackId, final User user) {

        if (null != blackTask && blackTask.getStatus() == AsyncTask.Status.RUNNING) {
            blackTask.cancel(true);
        }

        blackTask = new BaseTask(LiveActivity.this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                    NetResult netResult = null;
                    try {
                        if (isalowSpeech == false) {
                            JSONObject jo = new JSONObject();
                            jo.put("user_id", userid);
                            netResult = NetInterface.blackList(LiveActivity.this, jo);
                        } else {
                            netResult = NetInterface.deleteblackList(LiveActivity.this, blackId);
                            LogUtil.i(App.TAG, "delte black:" + blackId);
                        }
                    } catch (Exception e) {
                }

                return netResult;
            }

            @Override
            public void onCanCell(BaseTask baseTask) {
                baseTask.hideDialogSelf();
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();

                if (null == result) {

                    Tool.showMessageDialog(R.string.error_net, LiveActivity.this);

                } else {

                    if (result.isOk()) {

                        if (null != result.getData()) {

                            String id = (String) result.getData()[0];
                            if (!StringUtils.isEmpty(id)) {
                                user.setBlack(id);
                            }
                        }

                        if (isalowSpeech) {
                            onlineAdapter.removeFromBlack(userid);
                        } else {
                            onlineAdapter.add2Black(userid);
                        }

//                        onlineAdapter.blackSet(userid);

                    } else {
                        Tool.showMessageDialog(result.getMessage(), LiveActivity.this);
                    }


                }
            }
        });
        blackTask.execute(new HashMap<String, String>());


    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        // 从当前应用唤起微博并进行分享后，返回到当前应用时，需要在此处调用该函数
        // 来接收微博客户端返回的数据；执行成功，返回 true，并调用
        // {@link IWeiboHandler.Response#onResponse}；失败返回 false，不调用上述回调
        weiboApi.handleWeiboResponse(intent, this);
    }
    @Override
    public void onResponse(BaseResponse baseResp) {
        if(baseResp!= null){
            switch (baseResp.errCode) {
                case WBConstants.ErrorCode.ERR_OK:
                    Toast.makeText(this, "Share Succeed", Toast.LENGTH_LONG).show();
                    break;
                case WBConstants.ErrorCode.ERR_CANCEL:
                    Toast.makeText(this, "Share Cancelled", Toast.LENGTH_LONG).show();
                    break;
                case WBConstants.ErrorCode.ERR_FAIL:
                    Toast.makeText(this,
                            "Error Message: " + baseResp.errMsg,
                            Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }


    //==============================================================================================

    //ViewPager适配器
    class MyPagerAdapter extends PagerAdapter {
        private List<View> mViewList;

        public MyPagerAdapter(List<View> mViewList) {
            this.mViewList = mViewList;
        }

        @Override
        public int getCount() {
            return mViewList.size();//页卡数
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;//官方推荐写法
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            container.addView(mViewList.get(position));//添加页卡
            return mViewList.get(position);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(mViewList.get(position));//删除页卡
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitleList.get(position);//页卡标题
        }

    }
}
