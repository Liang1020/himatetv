package com.tongtong.android.activity;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;

import com.common.util.LogUtil;
import com.tongtong.android.App;
import com.tongtong.android.R;

public class MyBroadCastActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_broad_cast);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        LogUtil.i(App.TAG, "config changed");

    }
}
