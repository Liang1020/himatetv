package com.tongtong.android.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.LogUtil;
import com.common.util.Tool;
import com.tongtong.android.App;
import com.tongtong.android.R;
import com.tongtong.android.net.NetInterface;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class EditProfieActivity extends BaseTongTongActivity {

    @InjectView(R.id.textViewTitle)
    TextView textViewTitle;

    @InjectView(R.id.editTextContent)
    EditText editTextContent;

    private String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profie);
        ButterKnife.inject(this);
        App.getApp().addActivity(this);
        init();
    }


    private void init() {
        Intent intent = getIntent();
        title = intent.getStringExtra("title");
        String content = intent.getStringExtra("content");

        textViewTitle.setText(title);
        editTextContent.setText(content);

    }

    private boolean checkValue() {

        String content = getInput(editTextContent);
        if (content.isEmpty()) {
            Tool.showMessageDialog("Input is Empty!", this);
            return false;
        }

        return true;
    }

    @OnClick(R.id.textViewSave)
    public void onSaveClick() {

        Intent intent = new Intent(EditProfieActivity.this, MainActivity.class);
        switch (getIntent().getIntExtra("request", 0)) {
            case App.INTENT_NICKNAME:
                intent.putExtra("nickname", editTextContent.getText().toString());
                break;
            case App.INTENT_CITY:
                intent.putExtra("city", editTextContent.getText().toString());
                break;
            case App.INTENT_TOPIC:
                intent.putExtra("topic", editTextContent.getText().toString());
                break;
            case App.INTENT_DES:
                intent.putExtra("description", editTextContent.getText().toString());
                break;
            default:
                break;
        }

        setResult(RESULT_OK, intent);
        finish();

    }

    @OnClick(R.id.textViewCancel)
    public void onCancelClick() {
        finish();
    }

    @OnClick(R.id.imageViewClear)
    public void onClearClick() {
        editTextContent.setText("");
    }


    BaseTask taskProfile;

    private void uploadProfile(final String title, final String content) {
        LogUtil.i("PROFILE:", title + " " + content);
        if (null != taskProfile && taskProfile.getStatus() == AsyncTask.Status.RUNNING) {
            taskProfile.cancel(true);
        }

        taskProfile = new BaseTask(EditProfieActivity.this, new NetCallBack() {

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("token", App.getApp().getApiToken());
                    jsonObject.put(title, content);
                    if (title.equals("nickname")) {
                        jsonObject.put("city", App.getApp().getProfile().getCity());
                    } else if (title.equals("city")) {
                        jsonObject.put("nickname", App.getApp().getProfile().getNickname());
                    }

                    netResult = NetInterface.updateProfile(baseTask.activity, jsonObject);

                } catch (Exception e) {
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (null == result) {
                    showNotifyTextIn5Seconds(R.string.error_net);
                } else {
                    if (result.isOk()) {
                        showNotifyTextIn5Seconds("Success");
                        Intent intent = new Intent();
                        intent.putExtra(title, content);
                        setResult(RESULT_OK);

                        finish();
                    } else {
                        showNotifyTextIn5Seconds(result.getMessage());
                    }
                }
            }

        });

        taskProfile.execute(new HashMap<String, String>());
    }

    private void uploadRoom(final String title, final String content) {
        LogUtil.i("ROOM:", title + " " + content);
        if (null != taskProfile && taskProfile.getStatus() == AsyncTask.Status.RUNNING) {
            taskProfile.cancel(true);
        }

        taskProfile = new BaseTask(EditProfieActivity.this, new NetCallBack() {

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("token", App.getApp().getApiToken());
                    jsonObject.put(title, content);

                    netResult = NetInterface.updateRoom(baseTask.activity, jsonObject);

                } catch (Exception e) {
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (null == result) {
                    showNotifyTextIn5Seconds(R.string.error_net);
                } else {
                    if (result.isOk()) {
                        showNotifyTextIn5Seconds("Success");
                        Intent intent = new Intent();
                        intent.putExtra(title, content);
                        setResult(RESULT_OK);

                        finish();
                    } else {
                        showNotifyTextIn5Seconds(result.getMessage());
                    }
                }
            }

        });

        taskProfile.execute(new HashMap<String, String>());
    }

}
