package com.tongtong.android.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Window;
import android.view.WindowManager;

import com.callback.OnUploadCallBack;
import com.common.BaseActivity;
import com.common.net.FormFile;
import com.common.net.NetException;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.BitmapTool;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.sina.weibo.sdk.api.share.IWeiboShareAPI;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.platformtools.Util;
import com.tongtong.android.App;
import com.tongtong.android.domain.Profile;
import com.tongtong.android.domain.Room;
import com.tongtong.android.domain.User;
import com.tongtong.android.net.NetInterface;
import com.tongtong.android.photoutils.SharedpreferencesUtil;
import com.tongtong.android.view.ShareDialog;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;

/**
 * Created by clia on 2016/5/18.
 */
public class BaseTongTongActivity extends BaseActivity {

    private static BaseTask baseTaskLogin;

    public final short SHORT_REQUEST_ADDCATCH = 1000;
    public final short SHORT_REQUEST_SELECTIMAGE = 1001;
    public final short SHORT_ACCESS_LOCATION = 1003;
    public final short SHORT_REQUEST_READ_EXTEAL = 1004;
    public final short SHORT_REQUEST_READ_EXTEAL_CATCH = 1005;
    public final short SHORT_REQUEST_MEDIAS = 1006;
    public final short SHORT_REQUEST_PHONE_STATE = 1007;
    public final short SHORT_REQUEST_CANCEL_VERIFY = 1008;
    public final short SHORT_REQUESET_VERIFY = 1009;
    public final short SHORT_REQUESET_PERMISSION = 1010;

    public short SELECT_PIC_KITKAT = 801;
    public short SELECT_PIC = 802;
    public short TAKE_BIG_PICTURE = 803;
    public short CROP_BIG_PICTURE = 804;
    public int outputX = 512;
    public int outputY = 512;

    public byte TYPE_REQEST_PICTURE_TAKE = 0;
    public byte TYPE_REQEST_PICTURE_SELECT = 1;
    public SharedpreferencesUtil sharedpreferencesUtil = new SharedpreferencesUtil(this);
    public byte type_request_picture = TYPE_REQEST_PICTURE_TAKE;

    public static ShareDialog shareDialog;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState, persistentState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    }


    public void startTakePicture() {
        type_request_picture = TYPE_REQEST_PICTURE_TAKE;
        if (!sharedpreferencesUtil.getImageTempNameUri().equals("")) {
            sharedpreferencesUtil.delectImageTemp();
        }
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, sharedpreferencesUtil.getImageTempNameUri());
        startActivityForResult(intent, TAKE_BIG_PICTURE);
    }

    public void startSelectPicture() {
        type_request_picture = TYPE_REQEST_PICTURE_SELECT;
        if (!sharedpreferencesUtil.getImageTempNameUri().equals("")) {
            sharedpreferencesUtil.delectImageTemp();
        }
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/jpeg");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            startActivityForResult(intent, SELECT_PIC_KITKAT);
        } else {
            startActivityForResult(intent, SELECT_PIC);
        }
    }


    public void onTakePhtoClick() {

        type_request_picture = TYPE_REQEST_PICTURE_TAKE;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                    ) {
                startTakePicture();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, SHORT_REQUEST_READ_EXTEAL);
            }
        } else {
            startTakePicture();
        }
    }

    public void onChosePhtoClick() {
        type_request_picture = TYPE_REQEST_PICTURE_SELECT;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                startSelectPicture();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission_group.CAMERA}, SHORT_REQUEST_READ_EXTEAL);
            }
        } else {
            startSelectPicture();
        }
    }

    public void cropImageUri(Uri uriIn, Uri uriOut, int outputX, int outputY, int requestCode) {


        //======use local api

        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uriIn, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", outputX);
        intent.putExtra("outputY", outputY);
        intent.putExtra("scale", true);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uriOut);
        intent.putExtra("return-data", false);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true); // no face detection
        startActivityForResult(intent, requestCode);


        //===============use crop new api=========
//        String imgUrl = uriIn.getPath();
//
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inJustDecodeBounds = true;
//        BitmapFactory.decodeFile(imgUrl, options);
//
//        int w = options.outWidth;
//        int h = options.outHeight;
//
//        if (w < outputX || h < outputY) {
//            Tool.showMessageDialog("This picture is too small please select biger picture", this);
//            return;
//        }
//
//        CropImage.activity(uriIn)
//                .setGuidelines(CropImageView.Guidelines.ON)
//                .start(this, outputX, outputY, outputX, outputY, CropImageView.CropShape.RECTANGLE, Bitmap.CompressFormat.PNG);


    }


    //Login request
    public static void requestLogin(final String email, final String pwd, final Activity activity, final boolean showDialog, final NetCallBack netCallBack) {

        if (null != baseTaskLogin && baseTaskLogin.getStatus() == AsyncTask.Status.RUNNING) {
            baseTaskLogin.cancel(true);
        }


        baseTaskLogin = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {

                if (showDialog) {
                    baseTask.showDialogForSelf(true);
                }

                if (null != netCallBack) {

                    netCallBack.onPreCall(baseTask);
                }

            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {

                    JSONObject jsonObject = new JSONObject();

                    jsonObject.put("email", email);
                    jsonObject.put("password", pwd);

                    netResult = NetInterface.login(activity, jsonObject);
//                    LogUtil.v("leo", "sleep in login start");
//                    Thread.sleep(5000);
//                    LogUtil.v("leo", "sleep in login end");


                } catch (Exception e) {
                    LogUtil.e(App.TAG, "error:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {

                if (showDialog) {
                    baseTask.hideDialogSelf();
                }

                if (null != result && result.isOk()) {
                    App.getApp().setUser((User) result.getData()[0]);
                    App.getApp().saveApiToken((String) result.getData()[1]);
                    App.getApp().setProfile((Profile) result.getData()[2]);
                }
                if (null != netCallBack) {
                    netCallBack.onFinish(result, baseTask);
                }
            }
        }, activity);

        HashMap<String, String> hmap = new HashMap<String, String>();

        baseTaskLogin.execute(hmap);

    }



    public  static  BaseTask uploadTask;

    public static void uploadSceen(final Context context, final String fileUrl, final OnUploadCallBack onUploadCallBack) {

        if (null != uploadTask && uploadTask.getStatus() == AsyncTask.Status.RUNNING) {
            uploadTask.cancel(true);
        }

        uploadTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {

                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {
                    byte[] b = BitmapTool.readStream(new FileInputStream(fileUrl));

                    FormFile form = new FormFile("avatar", b, "crop_cache_file.png", "application/octet-stream");
                    FormFile[] temp = {form};
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("api_token", App.getApp().getApiToken());
                    netResult = NetInterface.uploadScreen(temp, map);
                } catch (Exception e) {
                    e.printStackTrace();
                    LogUtil.e(App.TAG, "upload error:" + e.getLocalizedMessage());
                    if (e instanceof NetException) {
                        LogUtil.e(App.TAG, "upload error:" + ((NetException) e).getCode());
                    }
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();

                if (null != onUploadCallBack) {
                    onUploadCallBack.onUploadEnd(result, baseTask.activity);
                }

                if (null != result && StringUtils.isEmpty(result.getMessage())) {
                    try {
                        File file = new File(fileUrl);
                        if (null != file && file.exists() && file.isFile()) {
                            file.delete();
                        }
                        LogUtil.e(App.TAG, "deleted file:" + fileUrl);
                    } catch (Exception e) {
                        LogUtil.e(App.TAG, "delete file fail");
                    }

                }

            }
        }, context);
        uploadTask.execute(new HashMap<String, String>());

    }



    @Override
    public void onBackPressed() {
        if (null != shareDialog && shareDialog.isShowing()) {
            shareDialog.dismiss();
            return;
        }

        super.onBackPressed();
    }


    public static boolean hideShareDialog() {
        if (null != shareDialog && shareDialog.isShowing()) {
            shareDialog.dismiss();
            return true;
        }

        return false;
    }

    public String buildTransaction(final String type) {
        return (type == null) ? String.valueOf(System.currentTimeMillis()) : type + System.currentTimeMillis();
    }


    public static void showShareDialog(Activity activity, Room room, ShareDialog.OnClickShare2Listener share2Listener, IWXAPI iwxapi, IWeiboShareAPI iwsapi, byte[] thumbData) {

//        if (null == shareDialog) {
            shareDialog = new ShareDialog(activity, room, iwxapi, iwsapi, thumbData);
//        }

        shareDialog.setOnClickShare2Listener(share2Listener);
        LogUtil.i(App.TAG, "show-->"+ shareDialog.isShowing());
        shareDialog.show();
    }


    public static void getBitmapBytes(final String url, final int drawable, final BaseActivity baseTongTongActivity, final NetCallBack netCallBack) {

        if (!StringUtils.isEmpty(url)) {

            Picasso.with(App.getApp()).load(url).resize(150, 150).config(Bitmap.Config.RGB_565).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    baseTongTongActivity.hideProgressDialog();
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    baseTongTongActivity.hideProgressDialog();
                    getBitmapBytes(null, drawable, baseTongTongActivity, netCallBack);

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    baseTongTongActivity.showProgressDialog(false);

                }
            });

        } else {

            Picasso.with(App.getApp()).load(drawable).resize(150, 150).config(Bitmap.Config.RGB_565).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    baseTongTongActivity.hideProgressDialog();

                    if (null != netCallBack && null != bitmap) {
                        NetResult netResult = new NetResult();
                        netResult.setData(new Object[]{Util.bmpToByteArray(bitmap, false)});
                        netCallBack.onFinish(netResult);
                    }

                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    baseTongTongActivity.hideProgressDialog();
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    baseTongTongActivity.showProgressDialog(false);
                }
            });
        }

    }


}
