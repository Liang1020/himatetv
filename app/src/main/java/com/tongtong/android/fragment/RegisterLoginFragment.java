package com.tongtong.android.fragment;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.common.view.BaseFragment;
import com.tongtong.android.R;

public class RegisterLoginFragment extends BaseFragment implements View.OnClickListener{

    private View rootView;
    private FragmentManager fragmentManager;

    private Button btnSignIn;

    private OnRegisterLoginFragmentInteractionListener mListener;

    public RegisterLoginFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fragmentManager = getFragmentManager();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_reigster_login, container, false);
        initView();

        return rootView;
    }

    private void initView(){
        btnSignIn = (Button) rootView.findViewById(R.id.btnSignIn);
        btnSignIn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSignIn:
                FragmentTransaction transaction = fragmentManager.beginTransaction();

                Fragment targetFragment = FragmentFactory.getInstanceByIndex(100);

                transaction.replace(R.id.content, targetFragment);
                transaction.addToBackStack(null);
                transaction.commit();

                if (mListener != null) {
                    mListener.onRegisterLoginFragmentInteraction(true);
                }
                break;
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (Build.VERSION.SDK_INT < 23) {

            onAttach(getActivity());
        }
        if (context instanceof OnRegisterLoginFragmentInteractionListener) {
            mListener = (OnRegisterLoginFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    public interface OnRegisterLoginFragmentInteractionListener {
        void onRegisterLoginFragmentInteraction(boolean isShow);
    }
}
