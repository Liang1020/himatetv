package com.tongtong.android.fragment;

import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.common.view.BaseFragment;
import com.indicator.view.IndicatorView;
import com.markmao.pulltorefresh.widget.XListView;
import com.squareup.picasso.Picasso;
import com.tongtong.android.App;
import com.tongtong.android.R;
import com.tongtong.android.activity.MainActivity;
import com.tongtong.android.activity.SearchActivity;
import com.tongtong.android.activity.VideoActivity;
import com.tongtong.android.adapter.RoomHomeAdapter;
import com.tongtong.android.domain.Room;
import com.tongtong.android.net.NetInterface;
import com.tongtong.android.view.CircleImageView;
import com.tongtong.android.view.CustomViewPager;

import org.junit.experimental.categories.Categories;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class HomeFragment extends BaseFragment implements CustomViewPager.OnSingleTouchListener, CustomViewPager.OnPageChangeListener, View.OnClickListener {

    private static final String TAG = "HOMEFRAGMENT";
    private int type;

    private CustomViewPager viewPager;
    private ImageView imgViewSearch;

    private int currentItem = 0;
    private int oldPosition = 0;

    private View viewHome;
    private static HomeFragment instance = null;

    private ScheduledExecutorService scheduledExecutorService;
    private ArrayList<Room> arrayListRooms;


    public IndicatorView indicatorView;

    private View viewTopConentListViewFistItem;

    private ArrayList<Room> roomsTop;
    private ArrayList<Room> roomsList;

    private RoomHomeAdapter roomAdapter;

    private int topNumber = 0;

    @InjectView(R.id.xlistView)
    public XListView xlistView;
    private int listViewTotalHeight;


    private TextView textViewTop;
    private ImageView imageViewSearch;

    public static HomeFragment newInstance(int type) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putInt("type", type);
        fragment.setArguments(args);
        LogUtil.v("leo", "Home Fragment newInstance");
        return fragment;
    }


    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            viewPager.setCurrentItem(currentItem);
        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        type = getArguments().getInt("type");
        LogUtil.v("leo", "Home Fragment onCreateView");

        if (null != viewHome) {
            ViewGroup parent = (ViewGroup) viewHome.getParent();
            if (parent != null) {
                parent.removeView(viewHome);
            }
            return viewHome;
        }

        viewHome = inflater.inflate(R.layout.fragment_home, container, false);
        viewTopConentListViewFistItem = inflater.inflate(R.layout.page_home_fragment_header, null);
        viewTopConentListViewFistItem.findViewById(R.id.relativeLayoutMore).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).showMoreFragment();
            }
        });

        textViewTop = (TextView) viewHome.findViewById(R.id.textViewTop);
        imageViewSearch = (ImageView) viewHome.findViewById(R.id.imgViewSearch);
        if(type == 1){
            textViewTop.setText(getResources().getString(R.string.bottom_live));
            imageViewSearch.setVisibility(View.GONE);
        }else{
            textViewTop.setText(getResources().getString(R.string.bottom_home));
            imageViewSearch.setVisibility(View.VISIBLE);
        }

        viewPager = (CustomViewPager) viewTopConentListViewFistItem.findViewById(R.id.viewPager);
        viewPager.setOnSingleTouchListener(new CustomViewPager.OnSingleTouchListener() {
            @Override
            public void onSingleTouch() {


                LogUtil.i(App.TAG, "=======dafa");

            }
        });


        imgViewSearch = (ImageView) viewTopConentListViewFistItem.findViewById(R.id.imgViewSearch);

        indicatorView = (IndicatorView) viewTopConentListViewFistItem.findViewById(R.id.indicatorView);

        ButterKnife.inject(this, viewHome);

        xlistView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                listViewTotalHeight = xlistView.getHeight();
            }
        });


        xlistView.setPullLoadEnable(false);
        xlistView.setPullRefreshEnable(true);

        xlistView.setXListViewListener(new XListView.IXListViewListener() {
            @Override
            public void onRefresh() {
                requestHomeRooms();
            }

            @Override
            public void onLoadMore() {

            }
        });


        return viewHome;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init() {


    }

    public void onClickmore() {
        ((MainActivity) getActivity()).showMoreFragment();
    }


    @Override
    public void onStart() {

        super.onStart();
    }

    @Override
    public void onStop() {
        LogUtil.i("ScheduleService", "------->finish");

        if (null != scheduledExecutorService) {
            scheduledExecutorService.shutdown();
        }
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgViewSearch:
                Tool.startActivity(getActivity(), SearchActivity.class);
                break;
        }
    }


    private class ScrollTask implements Runnable {

        public void run() {
            synchronized (viewPager) {
                currentItem = (currentItem + 1) % topNumber;
                handler.obtainMessage().sendToTarget();
            }
        }

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        currentItem = position;
        oldPosition = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onSingleTouch() {

    }

    BaseTask topTask;
    BaseTask timeLineTask;
    BaseTask addTimelineTask;

    private void requestHomeRooms() {

        if (null != topTask && topTask.getStatus() == AsyncTask.Status.RUNNING) {
            topTask.cancel(true);
        }

        topTask = new BaseTask(getActivity(), new NetCallBack() {
            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {
                    netResult = NetInterface.getHomeRooms3(getActivity());
                } catch (Exception e) {
                    e.printStackTrace();
                    LogUtil.e(App.TAG, "home rooms error:" + e.getLocalizedMessage());
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result) {
                if (null != result && result.isOk()) {
                    ArrayList<Room> list = (ArrayList<Room>) result.getData()[0];
                    ArrayList<Room> listgust = (ArrayList<Room>) result.getData()[1];

//                    ArrayList<Room> rooms = new ArrayList<>();
//
//                    Room room = new Room();
//
//                    room.setUser(App.getApp().getUser());
//
//                    rooms.addAll(list);
//                    rooms.addAll(listgust);
//                    rooms.add(room);

//                    buildHomeAdater(new ArrayList<Room>(), list);

                    addTop(list);
                }
                xlistView.stopRefresh();
            }

            @Override
            public void onCanCell(BaseTask baseTask) {
                xlistView.stopRefresh();
            }
        });

        timeLineTask = new BaseTask(getActivity(), new NetCallBack() {
            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
//                NetResult rootResult = new NetResult();
                LogUtil.v("leo", "getTrailerAll start");
                NetResult netResult1 = null;
                try {
                    netResult1 = new NetResult();
                    netResult1 = NetInterface.getTrailerAll(getActivity(), "0");
                } catch (Exception e) {
                    e.printStackTrace();
                    LogUtil.e(App.TAG, "Trailer error:" + e.getLocalizedMessage());
                }
                if (netResult1 == null) {
                    netResult1 = new NetResult();
                    netResult1.setCode(NetResult.CODE_ERROR);
                } else if (netResult1.isOk()) {
                    netResult1.setCode(NetResult.CODE_OK);
                } else {
                    netResult1.setCode(NetResult.CODE_ERROR);
                }
                LogUtil.v("leo", "getTrailerAll end");
                return netResult1;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (null != result && result.isOk()) {
//                    NetResult netResult1 = (NetResult) result.getData()[0];
//                    NetResult netResult2 = (NetResult) result.getData()[1];
                    NetResult netResult1 = result;

                    ArrayList<Room> timeLine = new ArrayList<>();
                    for (int i = 0; i < netResult1.getData().length; i++) {
                        timeLine.add((Room) netResult1.getData()[i]);
                    }
                    buildHomeAdapter(new ArrayList<Room>());
                    addTimeline(timeLine);
                    LogUtil.v("leo", "home fragment is inflated");
                }
                xlistView.stopRefresh();
            }

            @Override
            public void onCanCell(BaseTask baseTask) {
                xlistView.stopRefresh();
            }
        });


        topTask.execute(new HashMap<String, String>());
        timeLineTask.execute(new HashMap<String, String>());

    }


    @Override
    public void onResume() {
        super.onResume();
        if (ListUtiles.isEmpty(roomsList) || ListUtiles.isEmpty(roomsTop)) {
            requestHomeRooms();
        }
        LogUtil.i("ScheduleService", "------->start");
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleAtFixedRate(new ScrollTask(), 1, 2, TimeUnit.SECONDS);
    }

    private void buildHomeAdapter(ArrayList<Room> timelineRooms) {

        roomsList = timelineRooms;
        Point p = Tool.getDisplayMetrics(getActivity());
        roomAdapter = new RoomHomeAdapter(getActivity(), roomsList, p.x, p.y / 5, listViewTotalHeight, viewTopConentListViewFistItem, type);
        xlistView.setAdapter(roomAdapter);
        //===============================
        imgViewSearch = (ImageView) viewHome.findViewById(R.id.imgViewSearch);
        imgViewSearch.setOnClickListener(this);
    }

    private void addTop(ArrayList<Room> topRooms) {
        topNumber = topRooms.size();
        roomsTop = topRooms;
        indicatorView.setIndicatorSize(topRooms.size(), R.drawable.icon_dot_stroke, R.drawable.icon_dot_full);
        viewPager.setAdapter(new CustomAdapter(topRooms, xlistView.getHeight() / 2));

        viewPager.addOnPageChangeListener(this);
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                indicatorView.checkIndex(position);
            }
        });
    }

    private void addTimeline(ArrayList<Room> timelineRooms) {
        roomAdapter.addTimeline(timelineRooms);
        roomAdapter.printList();
    }


    private class CustomAdapter extends PagerAdapter {


        private ArrayList<Room> rooms;

        private ArrayList<View> views;

        private LayoutInflater layoutInfater;

        private Point p;

        private int vh;

        public CustomAdapter(ArrayList<Room> rooms, int vh) {

            this.rooms = rooms;
            this.views = new ArrayList<>();
            this.layoutInfater = LayoutInflater.from(getActivity());
            this.p = Tool.getDisplayMetrics(getActivity());
            if(rooms.size() == 0 || rooms == null){
                View v = layoutInfater.inflate(R.layout.item_home_viewpager, null);
                ImageView imgView = (ImageView) v.findViewById(R.id.imageView);
                LinearLayout viewCoverClick = (LinearLayout) v.findViewById(R.id.viewCoverClick);
                viewCoverClick.setVisibility(View.GONE);
                Picasso.with(App.getApp()).load(R.drawable.bg_no_anchor).resize(p.x, vh).centerCrop().into(imgView);
                views.add(v);
            }

            for (final Room r : rooms) {

                View v = layoutInfater.inflate(R.layout.item_home_viewpager, null);

                LinearLayout viewCoverClick = (LinearLayout) v.findViewById(R.id.viewCoverClick);
                viewCoverClick.setVisibility(View.VISIBLE);

                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        App.getApp().putTemPObject("room", r);
                        Tool.startActivity(getContext(), VideoActivity.class);
                    }
                });

//                v.findViewById(R.id.viewCoverClick).setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        LogUtil.e(App.TAG,"=====");
//
//                    }
//                });

                ImageView imgView = (ImageView) v.findViewById(R.id.imageView);
                TextView textViewlabel = (TextView) v.findViewById(R.id.textViewTitle);
                textViewlabel.setText(r.getSubject());

                TextView textViewAncher = (TextView) v.findViewById(R.id.textViewAncher);
                if (r.getUser() != null)
                    textViewAncher.setText(r.getUser().getNickName().toString());


                CircleImageView circleImageView = (CircleImageView) v.findViewById(R.id.circleImageView);

                if (null != r.getUser() && !StringUtils.isEmpty(r.getUser().getAvatar())) {

                    Picasso.with(App.getApp()).load(r.getUser().getAvatar()).
                            resize(50, 50).placeholder(R.drawable.bg_default_header).
                            centerCrop().into(circleImageView);
                }


                TextView textViewOnLineDesc = (TextView) v.findViewById(R.id.textViewOnLineDesc);
                String perons = String.valueOf(r.getTotalPeople());
                textViewOnLineDesc.setText(perons);


                String url = r.getImgUrl();
                if (!StringUtils.isEmpty(url)) {
                    Picasso.with(App.getApp()).load(url).resize(p.x, vh).placeholder(R.drawable.bg_default_broad2).centerCrop().into(imgView);
                } else {
                    Picasso.with(App.getApp()).load(R.drawable.bg_default_broad2).resize(p.x, vh).centerCrop().into(imgView);
                }


//                imgView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        LogUtil.e(App.TAG, "=================");
//                    }
//                });


//                v.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        App.getApp().putTemPObject("room", r);
////                      room = (Room) App.getApp().getTempObject("room");
//                        Tool.startActivity(getContext(), VideoActivity.class);
//
//                    }
//                });
                views.add(v);

            }

        }

        @Override
        public int getCount() {
            if(rooms.size() == 0 || rooms == null){
                return 1;
            }
            return rooms.size();
        }

        @Override
        public Object instantiateItem(ViewGroup arg0, int arg1) {

            View v = views.get(arg1);


            arg0.addView(v);
            return views.get(arg1);
        }

        @Override
        public void destroyItem(ViewGroup arg0, int arg1, Object arg2) {
            arg0.removeView((View) arg2);
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {

        }

        @Override
        public Parcelable saveState() {
            return null;
        }

    }

}
