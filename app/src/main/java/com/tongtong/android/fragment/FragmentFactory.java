package com.tongtong.android.fragment;

import android.support.v4.app.Fragment;

import com.tongtong.android.R;


/**
 * Created by clia on 2016/5/18.
 */
public class FragmentFactory {
    private final static int ANCHHOR = 100;

    private static HomeFragment homeFragment;
    private static HomeFragment liveFragment;
    private static CategoryFragment categoryFragment;
    private static RegisterLoginFragment registerLoginFragment;
    private static ProfileFragment anchorProfileFragment;

    public static Fragment getInstanceByIndex(int index) {

        Fragment fragment = null;

        if(index==R.id.rb_home){
            fragment = getHomeFragment();
        }else if(index==R.id.rb_live){
            fragment = getLiveFragment();
        }else if(index==R.id.rb_register_login){
            fragment = getProfileFragment();
        }else if(index==R.id.rb_channel){
            fragment = getCategoryFragment();
        }else if(index==ANCHHOR){
            fragment = getProfileFragment();
        }

//        switch (index) {
//            case R.id.rb_home:
//                fragment = getHomeFragment();
//                break;
//            case R.id.rb_live:
//                fragment = getLiveFragment();
//                break;
//            case R.id.rb_register_login:
//                fragment = getProfileFragment();
//                break;
//            case R.id.rb_channel:
//                fragment = getCategoryFragment();
//                break;
//            case ANCHHOR:
//                fragment = getProfileFragment();
//                break;
//        }
        return fragment;
    }


    private static HomeFragment getHomeFragment() {
        if (homeFragment == null) {
            homeFragment = HomeFragment.newInstance(0);
        }
        return homeFragment;
    }

    private static HomeFragment getLiveFragment() {
        if (liveFragment == null) {
            liveFragment = HomeFragment.newInstance(1);
        }
        return liveFragment;
    }

    private static RegisterLoginFragment getRegisterLoginFragment() {
        if (registerLoginFragment == null) {
            registerLoginFragment = new RegisterLoginFragment();
        }
        return registerLoginFragment;
    }

    private static ProfileFragment getProfileFragment() {
        if (anchorProfileFragment == null) {
            anchorProfileFragment = new ProfileFragment();
        }
        return anchorProfileFragment;
    }

    private static CategoryFragment getCategoryFragment() {
        if (categoryFragment == null) {
            categoryFragment = new CategoryFragment();
        }
        return categoryFragment;
    }
}
