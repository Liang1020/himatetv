package com.tongtong.android.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.common.view.BaseFragment;
import com.tongtong.android.R;
import com.tongtong.android.adapter.LivePageAdapter;
import com.tongtong.android.domain.LiveType;
import com.tongtong.android.view.SlidingTabLayout;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class LiveFragment extends BaseFragment {

    private SlidingTabLayout mSlidingTabLayout;


    @InjectView(R.id.viewpager)
    ViewPager mViewPager;

    private ArrayList<LiveType> liveTypes;

    private View view;

    private int totalheight;

    LivePageAdapter livePageAdapter;

    public LiveFragment() {


    }

    public static LiveFragment newInstance(String param1, String param2) {
        LiveFragment fragment = new LiveFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
            return view;
        }
        view = inflater.inflate(R.layout.fragment_live, container, false);

        ButterKnife.inject(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        mViewPager.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                totalheight = mViewPager.getHeight();
            }
        });


    }


    @Override
    public void onResume() {
        super.onResume();


        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        buildTypes();

                        livePageAdapter.onShowIndex(0);
                    }
                });

            }
        }, 500);


    }

    private boolean isbuildType = false;

    private void buildTypes() {

        if (isbuildType == true) {
            return;
        }
        isbuildType = true;

        mViewPager = (ViewPager) view.findViewById(R.id.viewpager);


        ArrayList<LiveType> liveTypes = new ArrayList<>();

        LiveType liveType1 = new LiveType();
        liveType1.setType("热门");
        liveType1.setId("hot");

        liveTypes.add(liveType1);


        LiveType liveType2 = new LiveType();
        liveType2.setType("推荐");
        liveType2.setId("suggest");

        liveTypes.add(liveType2);


        LiveType liveType3 = new LiveType();
        liveType3.setType("人气");
        liveType3.setId("popularity");

        liveTypes.add(liveType3);

        LiveType liveType4 = new LiveType();
        liveType4.setType("新人");
        liveType4.setId("new");

        liveTypes.add(liveType4);


        livePageAdapter = new LivePageAdapter(getActivity(), liveTypes, totalheight / 4, true);
        mViewPager.setAdapter(livePageAdapter);

        mSlidingTabLayout = (SlidingTabLayout) view.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setMotionEventSplittingEnabled(true);


        mSlidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.red);
            }

            @Override
            public int getDividerColor(int position) {
                return 0;
            }
        });

        mSlidingTabLayout.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                livePageAdapter.onShowIndex(position);

            }
        });

        mSlidingTabLayout.setViewPager(mViewPager);


    }


}
