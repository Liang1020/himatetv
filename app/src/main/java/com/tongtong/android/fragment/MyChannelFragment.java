package com.tongtong.android.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.common.view.BaseFragment;
import com.tongtong.android.R;

import java.util.ArrayList;
import java.util.List;

import com.tongtong.android.activity.StartLiveActivity;

public class MyChannelFragment extends BaseFragment
        implements View.OnClickListener {

    private View root;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private View viewInfo, viewParticipants;

    private List<View> viewList;
    private List<String> titleList;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    protected Button mBtnBroadcast = null;

    public MyChannelFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyChannelFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyChannelFragment newInstance(String param1, String param2) {
        MyChannelFragment fragment = new MyChannelFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_my_channel, container, false);
        initView();
        return root;
    }

    private void initView() {
        tabLayout = (TabLayout) root.findViewById(R.id.tabLayout);
        viewPager = (ViewPager) root.findViewById(R.id.vpChannel);

        viewInfo = root.inflate(getContext(),R.layout.page_live_info,null);
        viewParticipants = root.inflate(getContext(),R.layout.page_participants,null);

        viewList = new ArrayList<>();
        viewList.add(viewInfo);
        viewList.add(viewParticipants);

        titleList = new ArrayList<>();
        titleList.add("直播信息");
        titleList.add("参与者");

        ChannelPagerAdapter pagerAdapter = new ChannelPagerAdapter(viewList, titleList);
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        mBtnBroadcast = (Button) root.findViewById(R.id.broadcast_btn);
        mBtnBroadcast.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
//        Intent intent = new Intent(getContext(), MyLiveFullScreenActivity.class);
//        getContext().startActivity(intent);

        Intent intent = new Intent(getContext(), StartLiveActivity.class);
        getContext().startActivity(intent);
    }

    private class ChannelPagerAdapter extends PagerAdapter {

        private List<View> viewList;
        private List<String> titleList;

        public ChannelPagerAdapter(List<View> viewList, List<String> titleList) {
            this.viewList = viewList;
            this.titleList = titleList;
        }

        @Override
        public int getCount() {
            return viewList.size();
        }


        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {

            return arg0 == arg1;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            container.addView(viewList.get(position));
            return viewList.get(position);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {

            container.removeView(viewList.get(position));

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titleList.get(position);
        }
    }
}


