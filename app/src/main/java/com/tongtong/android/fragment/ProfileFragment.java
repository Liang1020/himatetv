package com.tongtong.android.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.LogUtil;
import com.common.util.MyAnimationUtils;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.common.view.BaseFragment;
import com.squareup.picasso.Picasso;
import com.tongtong.android.App;
import com.tongtong.android.R;
import com.tongtong.android.activity.BaseTongTongActivity;
import com.tongtong.android.activity.EditProfieActivity;
import com.tongtong.android.activity.MainActivity;
import com.tongtong.android.activity.RegisterActivity;
import com.tongtong.android.activity.StartLiveActivity;
import com.tongtong.android.domain.LiveType;
import com.tongtong.android.domain.Profile;
import com.tongtong.android.domain.User;
import com.tongtong.android.net.NetInterface;
import com.tongtong.android.photoutils.PhotoSelectDialog;
import com.tongtong.android.view.CategoryDialog;
import com.tongtong.android.view.CircleImageView;
import com.tongtong.android.view.ForgetPasswordDialog;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import static com.tongtong.android.App.INTENT_CITY;
import static com.tongtong.android.App.INTENT_DES;
import static com.tongtong.android.App.INTENT_NICKNAME;
import static com.tongtong.android.App.INTENT_TOPIC;
import static com.tongtong.android.App.getApp;

/**
 * user or ancher profile
 */
public class ProfileFragment extends BaseFragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private View root;

    private BaseTask taskProfile;
    @InjectView(R.id.textViewStartLiving)
    TextView textViewStartLiving;

    @InjectView(R.id.roundImageViewProfile)
    CircleImageView roundImageViewProfile;

    @InjectView(R.id.linearLayoutTop)
    View linearLayoutTop;

    @InjectView(R.id.linearLayoutPageLoginRoot)
    LinearLayout linearLayoutPageLoginRoot;//login view

    @InjectView(R.id.relativeLayoutContent)
    RelativeLayout relativeLayoutContent;//profile view

    @InjectView(R.id.editTextEmail)
    EditText editTextEmail;

    @InjectView(R.id.editTextpwd)
    EditText editTextpwd;

    @InjectView(R.id.textViewNickname)
    TextView textViewNickName;

    @InjectView(R.id.textViewEmail)
    TextView textViewEmail;

    @InjectView(R.id.textViewCity)
    TextView textViewCity;

    @InjectView(R.id.textViewCategory)
    TextView textViewCategory;

    @InjectView(R.id.textViewItem)
    TextView textViewItem;

    @InjectView(R.id.textViewAddr)
    TextView textViewAddr;

    @InjectView(R.id.textViewDesc)
    TextView textViewDescription;

//    @InjectView(R.id.linearLayoutLiveType)
//    LinearLayout linearLayoutLiveType;

    @InjectView(R.id.linearLayoutLiveTopic)
    LinearLayout linearLayoutLiveTopic;

//    @InjectView(R.id.linearLayoutLiveAddr)
//    LinearLayout linearLayoutLiveAddr;

    @InjectView(R.id.linearLayoutDesc)
    LinearLayout linearLayoutDesc;

//    @InjectView(R.id.imageViewType)
//    ImageView imageViewType;

    @InjectView(R.id.imageViewTopic)
    ImageView imageViewTopic;

//    @InjectView(R.id.imageViewAddr)
//    ImageView imageViewAddr;

    @InjectView(R.id.imageViewDesc)
    ImageView imageViewDesc;

    Animation animationHide, animationShow;

    private ArrayList<LiveType> liveTypes;
    BaseTask baseTask;

    private Profile profile;
    private boolean startUpdate = false;


    PhotoSelectDialog dialogPhotoSelect = null;
    ForgetPasswordDialog dialogForgetPassword = null;
    CategoryDialog dialogCategoery = null;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        animationHide = AnimationUtils.loadAnimation(getContext(), R.anim.slide_bottom_out);
        animationShow = AnimationUtils.loadAnimation(getContext(), R.anim.slide_bottom_in);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LogUtil.i("OnActivityCreated", "------");
//        requestLiveTypes();

        LogUtil.i("OnActivityCreated", "------");
        init();

    }


    private void init() {

        User user = getApp().getUser();
        if (null != user && !StringUtils.isEmpty(user.getAvatar())) {
            updateUserHeader(user.getAvatar());

        }


        liveTypes = getApp().getLiveTypes();
        profile = getApp().getProfile();

        if (null != profile) {
            textViewNickName.setText(profile.getNickname());
            textViewEmail.setText(profile.getEmail());
            textViewCity.setText(profile.getCity());

            if (!isAnchor()) {

                textViewStartLiving.setVisibility(View.INVISIBLE);
//                linearLayoutLiveType.setVisibility(View.INVISIBLE);
                linearLayoutLiveTopic.setVisibility(View.INVISIBLE);
//                linearLayoutLiveAddr.setVisibility(View.INVISIBLE);
                linearLayoutDesc.setVisibility(View.INVISIBLE);
//                imageViewType.setVisibility(View.INVISIBLE);
                imageViewTopic.setVisibility(View.INVISIBLE);
//                imageViewAddr.setVisibility(View.INVISIBLE);
                imageViewDesc.setVisibility(View.INVISIBLE);


            } else {
                textViewCategory.setText(convertIdtoString(profile.getLiveType()));
                textViewItem.setText(profile.getLivTopic());


                if (null != user.getRoom()) {
                    textViewAddr.setText(user.getRoom().getAppUrl());
                }

//                textViewAddr.setText(profile.getLiveAddr());


                textViewDescription.setText(profile.getDescription());

                textViewStartLiving.setVisibility(View.VISIBLE);
//                linearLayoutLiveType.setVisibility(View.VISIBLE);
                linearLayoutLiveTopic.setVisibility(View.VISIBLE);
//                linearLayoutLiveAddr.setVisibility(View.VISIBLE);
                linearLayoutDesc.setVisibility(View.VISIBLE);
//                imageViewType.setVisibility(View.VISIBLE);
                imageViewDesc.setVisibility(View.VISIBLE);
//                imageViewAddr.setVisibility(View.VISIBLE);
                imageViewDesc.setVisibility(View.VISIBLE);

            }

        } else {
            if (null != getApp().getUser()) {
                relativeLayoutContent.setVisibility(View.VISIBLE);
                linearLayoutPageLoginRoot.setVisibility(View.GONE);
                textViewNickName.setText(user.getNickName());
                textViewEmail.setText(user.getEmail());
                textViewCity.setText(user.getCity());

                textViewStartLiving.setVisibility(View.INVISIBLE);
//                linearLayoutLiveType.setVisibility(View.INVISIBLE);
                linearLayoutLiveTopic.setVisibility(View.INVISIBLE);
//                linearLayoutLiveAddr.setVisibility(View.INVISIBLE);
                linearLayoutDesc.setVisibility(View.INVISIBLE);
//                imageViewType.setVisibility(View.INVISIBLE);
                imageViewTopic.setVisibility(View.INVISIBLE);
//                imageViewAddr.setVisibility(View.INVISIBLE);
                imageViewDesc.setVisibility(View.INVISIBLE);
            } else {
                relativeLayoutContent.setVisibility(View.GONE);
                linearLayoutPageLoginRoot.setVisibility(View.VISIBLE);
            }

        }

        roundImageViewProfile.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int height = roundImageViewProfile.getHeight();
                RelativeLayout.LayoutParams lpp = (RelativeLayout.LayoutParams) roundImageViewProfile.getLayoutParams();
                lpp.topMargin = -height / 2;
                roundImageViewProfile.setLayoutParams(lpp);
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LogUtil.i("OnActivityView", "------");
        root = inflater.inflate(R.layout.fragment_anchor_profile, container, false);
        ButterKnife.inject(this, root);
        init();
        return root;
    }

    private boolean isAnchor() {
        User user = getApp().getUser();
        if (null != user && user.getRole().equals("anchor")) {
            return true;
        }
        return false;
    }

    public void updateUserHeader(String path) {

        if (StringUtils.isEmpty(path)) {
            Picasso.with(getApp()).load(R.drawable.bg_default_header).resize(200, 200).config(Bitmap.Config.RGB_565).into(roundImageViewProfile);
        } else {
            Picasso.with(getApp()).load(path).resize(200, 200).placeholder(R.drawable.bg_default_header).config(Bitmap.Config.RGB_565).into(roundImageViewProfile);
        }

    }

    @OnClick(R.id.roundImageViewProfile)
    public void onClickEditPhoto() {

        ((MainActivity) getActivity()).outputX = 512;
        ((MainActivity) getActivity()).outputY = 512;

        dialogPhotoSelect = new PhotoSelectDialog(getActivity());
        dialogPhotoSelect.setOnAddCatchListener(new PhotoSelectDialog.OnPhotoSelectionActionListener() {
            @Override
            public void onTakePhotoClick() {

                ((MainActivity) getActivity()).onTakePhtoClick();
            }

            @Override
            public void onChoosePhotoClick() {
                ((MainActivity) getActivity()).onChosePhtoClick();
            }

            @Override
            public void onDismis() {

            }
        });
        dialogPhotoSelect.getRootView().clearAnimation();
        dialogPhotoSelect.show();


    }

    @Override
    public boolean onBackPressed() {
        if (null != dialogPhotoSelect && dialogPhotoSelect.isShowing()) {
            dialogPhotoSelect.dismiss();
            return true;
        }

        if (null != dialogForgetPassword && dialogForgetPassword.isShowing()) {
            dialogForgetPassword.dismiss();
            return true;
        }

        if (null != dialogCategoery && dialogCategoery.isShowing()) {
            dialogCategoery.dismiss();
            return true;
        }

        return false;
    }


    @OnClick(R.id.textViewStartLiving)
    public void onStartClick() {
        Tool.startActivity(getActivity(), StartLiveActivity.class);
    }

    @OnClick(R.id.buttonSignIn)
    public void onSignInClick() {

        String email = editTextEmail.getText().toString();
        String pwd = editTextpwd.getText().toString();


        BaseTongTongActivity.requestLogin(email, pwd, getActivity(), true, new NetCallBack() {
            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (!getActivity().isFinishing()) {
                    if (null != result) {
                        if (result.isOk()) {
                            init();
                            MyAnimationUtils.animationHideview(linearLayoutPageLoginRoot, animationHide);
                            MyAnimationUtils.animationShowView(relativeLayoutContent, animationShow);
                        } else {
                            Tool.showMessageDialog(result.getMessage(), getActivity());
                        }
                    } else {
                        Tool.showMessageDialog("Net error", getActivity());
                    }
                }

            }
        });
    }

    @OnClick(R.id.buttonSignUp)
    public void onSignUpClick() {
        Tool.startActivity(getContext(), RegisterActivity.class);
        getActivity().finish();
    }


    private void switchLogOutView() {

        MyAnimationUtils.animationHideview(relativeLayoutContent, animationHide);
        MyAnimationUtils.animationShowView(linearLayoutPageLoginRoot, animationShow);

    }

    @OnClick(R.id.textViewForgetpwd)
    public void onForgetClick() {
        dialogForgetPassword = new ForgetPasswordDialog(getActivity(), editTextEmail.getText().toString());
        dialogForgetPassword.setOnConfirmDialogListener(new ForgetPasswordDialog.OnConfirmDialogListener() {
            @Override
            public void OnConfirmClick(String email) {
                if (!StringUtils.isEmpty(email)) {
                    forgetPwd(email);
                }
            }

            @Override
            public void OncancelClick() {

            }
        });
        dialogForgetPassword.show();
    }

    @OnClick(R.id.textViewLogOut)
    public void onClickLogOut() {

        getApp().setUser(null);
        switchLogOutView();

    }

    @OnClick(R.id.textViewNickname)
    public void onClickNickname() {
        Intent intent = new Intent(getActivity(), EditProfieActivity.class);

        intent.putExtra("request", App.INTENT_NICKNAME);
        intent.putExtra("title", getResources().getString(R.string.nickname).toString());
        intent.putExtra("content", textViewNickName.getText().toString());
        getActivity().startActivityForResult(intent, INTENT_NICKNAME);
    }

    @OnClick(R.id.textViewCity)
    public void onClickCity() {
        Intent intent = new Intent(getContext(), EditProfieActivity.class);

        intent.putExtra("request", App.INTENT_CITY);
        intent.putExtra("title", getResources().getString(R.string.city).toString());
        intent.putExtra("content", textViewCity.getText().toString());
        getActivity().startActivityForResult(intent, App.INTENT_CITY);
    }

    @OnClick(R.id.textViewCategory)
    public void onClickCategory() {


        dialogCategoery = new CategoryDialog(getContext(), liveTypes);
        dialogCategoery.setOnClickDialogListener(new CategoryDialog.OnClickDialogListener() {
            @Override
            public void OnOkClick(String category) {
                if (null != profile) {
                    profile.setLiveType(String.valueOf(convertStringtoId(category)));
                }
                textViewCategory.setText(category);
                updateRoom();
            }

            @Override
            public void OnCancelClick() {

            }
        });

        dialogCategoery.show();

    }

    @OnClick(R.id.textViewItem)
    public void onClickItem() {
        Intent intent = new Intent(getContext(), EditProfieActivity.class);

        intent.putExtra("request", App.INTENT_TOPIC);
        intent.putExtra("title", getResources().getString(R.string.item).toString());
        intent.putExtra("content", textViewItem.getText().toString());
        getActivity().startActivityForResult(intent, App.INTENT_TOPIC);
    }

    @OnClick(R.id.textViewDesc)
    public void onClickDesc() {
        Intent intent = new Intent(getContext(), EditProfieActivity.class);

        intent.putExtra("request", App.INTENT_DES);
        intent.putExtra("title", getResources().getString(R.string.more).toString());
        intent.putExtra("content", textViewDescription.getText().toString());
        getActivity().startActivityForResult(intent, App.INTENT_DES);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtil.i(App.TAG, "OnActiviytResultFragment---------->enter");
        LogUtil.i(App.TAG, "requestCode:" + requestCode + " resultCode:" + resultCode);

        profile = getApp().getProfile();
        if (null == profile) return;

        if (resultCode == getActivity().RESULT_OK) {
            switch (requestCode) {
                case INTENT_NICKNAME:
                    profile.setNickname(data.getStringExtra("nickname"));
                    break;
                case INTENT_CITY:
                    profile.setCity(data.getStringExtra("city"));
                    break;
                case INTENT_TOPIC:
                    profile.setLivTopic(data.getStringExtra("topic"));
                    break;
                case INTENT_DES:
                    profile.setDescription(data.getStringExtra("description"));
                    break;
            }
            startUpdate = true;
        }
        getApp().setProfile(profile);

    }

    @Override
    public void onResume() {
        super.onResume();
        LogUtil.i("ProfileFragmentOnResume---------->", "enter");
        if (null != getApp().getUser()) {
            relativeLayoutContent.setVisibility(View.VISIBLE);
            linearLayoutPageLoginRoot.setVisibility(View.GONE);
        } else {
            relativeLayoutContent.setVisibility(View.GONE);
            linearLayoutPageLoginRoot.setVisibility(View.VISIBLE);
        }

//        if (startUpdate) {
        init();

        if (isAnchor()) {
            updateRoom();
        } else {
            if (null != getApp().getUser()) {
                updateProfile();
            }
        }
//        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private String convertIdtoString(String id) {
        if (null == liveTypes || liveTypes.isEmpty()) return "";
        String type = "";
        for (int i = 0; i < liveTypes.size(); i++) {
            if (liveTypes.get(i).getId().equals(id)) {
                type = liveTypes.get(i).getType();
                break;
            }
        }
        return type;
    }

    private int convertStringtoId(String type) {
        if (null == liveTypes || liveTypes.isEmpty()) return 1;
        int typeId = 1;
        for (int i = 0; i < liveTypes.size(); i++) {
            if (liveTypes.get(i).getType().equals(type)) {
                typeId = Integer.valueOf(liveTypes.get(i).getId());
                break;
            }
        }
        return typeId;
    }


    private void forgetPwd(final String email) {
        if (null != baseTask && baseTask.getStatus() == AsyncTask.Status.RUNNING) {
            baseTask.cancel(true);
        }

        baseTask = new BaseTask(getActivity(), new NetCallBack() {
            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("token", getApp().getApiToken());
                    jsonObject.put("email", email);
                    netResult = NetInterface.forgetPassword(baseTask.activity, jsonObject);

                } catch (Exception e) {
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null == result) {
                    Tool.showMessageDialog(getResources().getString(R.string.error_net), getActivity());
                } else {
                    if (result.isOk()) {
                        Tool.showMessageDialog(getResources().getString(R.string.reset_password_successfully), getActivity());
                    } else {
                        Tool.showMessageDialog(result.getMessage(), getActivity());
                    }
                }
            }

        });

        baseTask.execute(new HashMap<String, String>());


    }


    private void requestLiveTypes() {

        if (null != baseTask && baseTask.getStatus() == AsyncTask.Status.RUNNING) {
            baseTask.cancel(true);
        }

        baseTask = new BaseTask(getActivity(), new NetCallBack() {


            @Override
            public void onPreCall(BaseTask baseTask) {

            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;

                try {
                    netResult = NetInterface.getLiveType(getActivity(), new JSONObject());
                } catch (Exception e) {
                    LogUtil.i(App.TAG, "request live type error");
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {

                if (null != result && result.isOk()) {
                    liveTypes = ((ArrayList<LiveType>) result.getData()[0]);
                    getApp().setLiveTypes(liveTypes);
                    init();
                }
            }
        });

        baseTask.execute(new HashMap<String, String>());
    }


    private void updateProfile() {

        if (null != taskProfile && taskProfile.getStatus() == AsyncTask.Status.RUNNING) {
            taskProfile.cancel(true);
        }

        taskProfile = new BaseTask(getActivity(), new NetCallBack() {

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("token", getApp().getApiToken());
                    jsonObject.put("nickname", textViewNickName.getText().toString());
                    jsonObject.put("city", textViewCity.getText().toString());

                    netResult = NetInterface.updateProfile(baseTask.activity, jsonObject);

                } catch (Exception e) {
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (null == result) {
                    Tool.showMessageDialog(getResources().getString(R.string.error_net), getActivity());
                } else {
                    if (result.isOk()) {
                        LogUtil.i("Update Profile:", "Success");
                    } else {
                        Tool.showMessageDialog(result.getMessage(), getActivity());
                    }
                }
            }

        });

        taskProfile.execute(new HashMap<String, String>());
    }

    private void updateRoom() {

        if (null != taskProfile && taskProfile.getStatus() == AsyncTask.Status.RUNNING) {
            taskProfile.cancel(true);
        }

        taskProfile = new BaseTask(getActivity(), new NetCallBack() {

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("token", getApp().getApiToken());
                    jsonObject.put("nickname", textViewNickName.getText().toString());
                    jsonObject.put("city", textViewCity.getText().toString());
                    jsonObject.put("subject", textViewItem.getText().toString());
                    jsonObject.put("contents", textViewDescription.getText().toString());
                    jsonObject.put("type_id", convertStringtoId(textViewCategory.getText().toString()));

                    netResult = NetInterface.updateRoom(baseTask.activity, jsonObject);

                } catch (Exception e) {
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (null == result) {
                    Tool.showMessageDialog(getResources().getString(R.string.error_net), getActivity());
                } else {
                    if (result.isOk()) {
                        LogUtil.i("Update Room:", "Success");
                    } else {
                        App.getApp().showError(result, getActivity());
                    }
                }
            }

        });

        taskProfile.execute(new HashMap<String, String>());
    }

}
