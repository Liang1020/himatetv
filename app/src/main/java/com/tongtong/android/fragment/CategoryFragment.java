package com.tongtong.android.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.view.BaseFragment;
import com.tongtong.android.App;
import com.tongtong.android.R;
import com.tongtong.android.adapter.LivePageAdapter;
import com.tongtong.android.domain.LiveType;
import com.tongtong.android.net.NetInterface;
import com.tongtong.android.view.SlidingTabLayout;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class CategoryFragment extends BaseFragment {

    private SlidingTabLayout mSlidingTabLayout;

    private ViewPager mViewPager;

    private ArrayList<LiveType> liveTypes;
    private View view;
    private int listHeight;


    public static LiveFragment newInstance(String param1, String param2) {
        LiveFragment fragment = new LiveFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
            return view;
        }
        view = inflater.inflate(R.layout.fragment_category, container, false);
        mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
        mViewPager.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                listHeight = mViewPager.getHeight();
            }
        });



        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ListUtiles.isEmpty(liveTypes)) {
            requestLiveTypes();
        }
    }


    private void buildTypes(ArrayList<LiveType> types) {

        this.liveTypes = types;

        if (!ListUtiles.isEmpty(types)) {

            int itemheight = listHeight / 4;
            final LivePageAdapter livePageAdapter = new LivePageAdapter(getActivity(), types, itemheight, false);
            mViewPager.setAdapter(livePageAdapter);

            mSlidingTabLayout = (SlidingTabLayout) view.findViewById(R.id.sliding_tabs);

            mSlidingTabLayout.setMotionEventSplittingEnabled(true);
            mSlidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

                @Override
                public int getIndicatorColor(int position) {
                    return getResources().getColor(R.color.red);
                }

                @Override
                public int getDividerColor(int position) {
                    return 0;
                }
            });

            mSlidingTabLayout.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    livePageAdapter.onShowIndex(position);
                }
            });

            mSlidingTabLayout.setViewPager(mViewPager);
            livePageAdapter.onShowIndex(mViewPager.getCurrentItem());
        }

    }

    BaseTask baseTask;

    private void requestLiveTypes() {

        if (null != baseTask && baseTask.getStatus() == AsyncTask.Status.RUNNING) {
            baseTask.cancel(true);
        }

        baseTask = new BaseTask(getActivity(), new NetCallBack() {


            @Override
            public void onPreCall(BaseTask baseTask) {

            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;

                try {
                    netResult = NetInterface.getLiveType(baseTask.activity, new JSONObject());
                } catch (Exception e) {
                    LogUtil.i(App.TAG, "request live type error");
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {

                if (null != result && result.isOk()) {
                    buildTypes((ArrayList<LiveType>) result.getData()[0]);
                }
            }
        });

        baseTask.execute(new HashMap<String, String>());


    }

}
