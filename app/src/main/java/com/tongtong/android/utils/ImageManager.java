package com.tongtong.android.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.tongtong.android.App;

/**
 * Created by leocx on 2016/9/6.
 */
public class ImageManager {
    public static Bitmap getBitmap(int id) {
        return BitmapFactory.decodeStream(App.getApp().getBaseContext().getResources().openRawResource(id));
    }

}

