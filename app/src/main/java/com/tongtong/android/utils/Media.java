package com.tongtong.android.utils;

/**
 * Created by clia on 2016/7/12.
 */
public class Media {

    private  String mName = "";
    private String mPath = "";

    public String getmPath() {
        return mPath;
    }

    public void setmPath(String mPath) {
        this.mPath = mPath;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }
}
