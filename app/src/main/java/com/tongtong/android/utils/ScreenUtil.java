package com.tongtong.android.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import java.util.HashMap;

/**
 * Created by clia on 2016/7/12.
 */
public class ScreenUtil {
    /**
     * dp to px
     */
    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * px to dp
     */
    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    /**
     * screen
     */
    public static int getWidth(Context context){
        WindowManager wm=(WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(outMetrics);
        return outMetrics.widthPixels;
    }

    /**
     * screen
     * */
    public static int getHeight(Context context){
        WindowManager wm=(WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(outMetrics);
        return outMetrics.heightPixels;
    }

    /**
     * physical screen
     * */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static double getScreenSizeOfDevice2(Context cxt) {
        Point point = new Point();
        WindowManager wm = (WindowManager) cxt.getSystemService(Context.WINDOW_SERVICE);
        //getRealSize()要求minapi 17
        wm.getDefaultDisplay().getRealSize(point);
        DisplayMetrics dm = cxt.getResources().getDisplayMetrics();
        double x = Math.pow(point.x / dm.xdpi, 2);
        double y = Math.pow(point.y / dm.ydpi, 2);
        double screenInches = Math.sqrt(x + y);
        return screenInches;
    }

    /**
     * dpi
     * */
    public static HashMap<String, String> getDensity(Context cxt) {
        DisplayMetrics displayMetrics = cxt.getResources().getDisplayMetrics();
        StringBuilder builder = new StringBuilder();
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Density", displayMetrics.density + "");
        map.put("densityDpi", displayMetrics.densityDpi + "");
        map.put("heightPixels", displayMetrics.heightPixels + "");
        map.put("widthPixels", displayMetrics.widthPixels + "");
        return map;
    }
}
