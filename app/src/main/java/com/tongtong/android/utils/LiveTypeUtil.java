package com.tongtong.android.utils;

import com.tongtong.android.App;
import com.tongtong.android.domain.LiveType;

import java.util.ArrayList;

/**
 * Created by clia on 2016/8/19.
 */
public class LiveTypeUtil {

    public static String convertIdtoString(ArrayList<LiveType> liveTypes, String id){
        if(null == liveTypes || liveTypes.isEmpty()) return "";
        String type = "";
        for(int i = 0; i < liveTypes.size(); i++){
            if(liveTypes.get(i).getId().equals(id)){
                type = liveTypes.get(i).getType();
                break;
            }
        }
        return type;
    }

    public static int convertStringtoId(ArrayList<LiveType> liveTypes, String type){
        if(null == liveTypes || liveTypes.isEmpty()) return 1;
        int typeId = 1;
        for(int i = 0; i < liveTypes.size(); i++){
            if(liveTypes.get(i).getType().equals(type)){
                typeId = Integer.valueOf(liveTypes.get(i).getId());
                break;
            }
        }
        return typeId;
    }
}
